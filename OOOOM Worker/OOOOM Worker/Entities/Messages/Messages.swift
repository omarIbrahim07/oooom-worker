//
//  Messages.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 4/25/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import Foundation
import ObjectMapper

class Message : Mappable {
    
    var id : Int?
    var postId : String?
    var parentId : String?
    var toUserId : String?
    var fromUserId : String?
    var message : String?
    var fromName : String?
    var createdAt : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        postId <- map["post_id"]
        parentId <- map["parent_id"]
        toUserId <- map["to_user_id"]
        fromUserId <- map["from_user_id"]
        message <- map["message"]
        fromName <- map["from_name"]
        createdAt <- map["created_at"]
    }
    
}

class ReturnedMessage : Mappable {
    
    var Message : String?
    var ConversationID : String?
    var MessageID : Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        Message <- map["Message"]
        ConversationID <- map["ConversationID"]
        MessageID <- map["MessageID"]
    }
    
}
