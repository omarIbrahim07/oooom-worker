//
//  ff.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/19/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

struct ChatMessage {
    var fromUSerID: String?
    var toUSerID: String?
    var message: String?
    var messageType: String?
    var timeStamp: String?
}
