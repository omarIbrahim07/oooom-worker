//
//  Prelogin Info.swift
//  OOOOM Worker
//
//  Created by Omar Ibrahim on 1/9/20.
//  Copyright © 2020 Egy Designer. All rights reserved.
//

import Foundation
import ObjectMapper

class PreloginInfo: Mappable {

    var success : Bool?
    var userInfo: Info?
    
    required init?(map: Map) {
        
    }

    func mapping(map: Map) {
        success <- map["success"]
        userInfo <- map["user"]
    }

}

class Info: Mappable {

    var id : Int?
    var isVerified: Int?
    var approved: Int?
    var balanceLimit: Int?
    var active: Int?
    
    required init?(map: Map) {
        
    }

    func mapping(map: Map) {
        id <- map["id"]
        isVerified <- map["is_verified"]
        approved <- map["approved"]
        balanceLimit <- map["balancelimit"]
        active <- map["active"]
    }

}

