//
//  Technician.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/12/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class Technician : Mappable {
    
    var userId : Int?
    var name : String?
    var longitude : Float?
    var latitude : Float?
    var rating : Float?
    var distance: Float?
    var areaID: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        userId <- map["user_id"]
        name <- map["name"]
        longitude <- map["longitude"]
        latitude <- map["latitude"]
        rating <- map["rating"]
        distance <- map["distance"]
        areaID <- map["area_id"]
        
    }
    
}
