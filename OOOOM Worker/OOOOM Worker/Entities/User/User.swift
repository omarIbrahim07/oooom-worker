//
//  User.swift
//  Blabber
//
//  Created by Hassan on 12/22/17.
//  Copyright © 2017 Hassan. All rights reserved.
//

import UIKit
import ObjectMapper

class User: NSObject, Mappable, NSCoding {
    
    var id : Int?
    var firstName : String?
    var lastName : String?
    var email : String?
    var phone : String?
    var address: String?
    var image : String?
    var isVerified: Int?
    var rating: Double?
    var areaID: Int?
    var area: String?
    var areaEn: String?
    var notificationsCount: Int?
    var balanceLimit: Int?
    var active: Int?
    var age: Int?
    var price: Int?
    var idImageFront: String?
    var serviceID: Int?
    var balance: Int?
    var serviceName: String?
    var serviceNameEn: String?
    var from: String?
    var fromEN: String?
    var ordersCompletedCount: Int?
    var newOrdersCount: Int?
    var newBidsCount: Int?
    
    required override init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        firstName <- map["first_name"]
        lastName <- map["last_name"]
        email <- map["email"]
        phone <- map["phone"]
        address <- map["address"]
        isVerified <- map["is_verified"]
        rating <- map["rating"]
        image <- map["image"]
        areaID <- map["area_id"]
        area <- map["area_name"]
        areaEn <- map["area_nameEN"]
        notificationsCount <- map["NotificationsCount"]
        balanceLimit <- map["balancelimit"]
        active <- map["active"]
        age <- map["age"]
        price <- map["price"]
        idImageFront <- map["id_image_front"]
        serviceID <- map["service_id"]
        balance <- map["balance"]
        serviceName <- map["service_name"]
        serviceNameEn <- map["service_nameEN"]
        from <- map["from"]
        fromEN <- map["fromEN"]
        ordersCompletedCount <- map["orders_completed"]
        newOrdersCount <- map["new_orders"]
        newBidsCount <- map["new_bids"]
    }
    
    //MARK: - NSCoding -
    required init(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: "id") as? Int
        self.firstName = aDecoder.decodeObject(forKey: "first_name") as? String
        self.lastName = aDecoder.decodeObject(forKey: "last_name") as? String
        self.email = aDecoder.decodeObject(forKey: "email") as? String
        self.phone = aDecoder.decodeObject(forKey: "phone") as? String
        self.address = aDecoder.decodeObject(forKey: "address") as? String
        self.isVerified = aDecoder.decodeObject(forKey: "is_verified") as? Int
        self.rating = aDecoder.decodeObject(forKey: "rating") as? Double
        self.image = aDecoder.decodeObject(forKey: "image") as? String
        self.areaID = aDecoder.decodeObject(forKey: "area_id") as? Int
        self.area = aDecoder.decodeObject(forKey: "area_name") as? String
        self.areaEn = aDecoder.decodeObject(forKey: "area_nameEN") as? String
        self.notificationsCount = aDecoder.decodeObject(forKey: "NotificationsCount") as? Int
        self.balanceLimit = aDecoder.decodeObject(forKey: "balancelimit") as? Int
        self.active = aDecoder.decodeObject(forKey: "active") as? Int
        self.age = aDecoder.decodeObject(forKey: "age") as? Int
        self.price = aDecoder.decodeObject(forKey: "price") as? Int
        self.idImageFront = aDecoder.decodeObject(forKey: "id_image_front") as? String
        self.serviceID = aDecoder.decodeObject(forKey: "service_id") as? Int
        self.balance = aDecoder.decodeObject(forKey: "balance") as? Int
        self.serviceName = aDecoder.decodeObject(forKey: "service_name") as? String
        self.serviceNameEn = aDecoder.decodeObject(forKey: "service_nameEN") as? String
        self.from = aDecoder.decodeObject(forKey: "from") as? String
        self.fromEN = aDecoder.decodeObject(forKey: "fromEN") as? String
        self.ordersCompletedCount = aDecoder.decodeObject(forKey: "orders_completed") as? Int
        self.newOrdersCount = aDecoder.decodeObject(forKey: "new_orders") as? Int
        self.newBidsCount = aDecoder.decodeObject(forKey: "new_bids") as? Int
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(firstName, forKey: "first_name")
        aCoder.encode(lastName, forKey: "last_name")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(phone, forKey: "phone")
        aCoder.encode(address, forKey: "address")
        aCoder.encode(isVerified, forKey: "is_verified")
        aCoder.encode(rating, forKey: "rating")
        aCoder.encode(image, forKey: "image")
        aCoder.encode(areaID, forKey: "area_id")
        aCoder.encode(areaEn, forKey: "area_nameEN")
        aCoder.encode(area, forKey: "area_name")
        aCoder.encode(from, forKey: "from")
        aCoder.encode(fromEN, forKey: "fromEN")
        aCoder.encode(notificationsCount, forKey: "NotificationsCount")
        aCoder.encode(balanceLimit, forKey: "balancelimit")
        aCoder.encode(active, forKey: "active")
        aCoder.encode(age, forKey: "age")
        aCoder.encode(price, forKey: "price")
        aCoder.encode(idImageFront, forKey: "id_image_front")
        aCoder.encode(serviceID, forKey: "service_id")
        aCoder.encode(balance, forKey: "balance")
        aCoder.encode(serviceName, forKey: "service_name")
        aCoder.encode(serviceNameEn, forKey: "service_nameEN")
        aCoder.encode(ordersCompletedCount, forKey: "orders_completed")
        aCoder.encode(newOrdersCount, forKey: "new_orders")
        aCoder.encode(newBidsCount, forKey: "new_bids")
    }
    
}
