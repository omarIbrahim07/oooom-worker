//
//  Work.swift
//  OOOOM Worker
//
//  Created by Omar Ibrahim on 1/14/20.
//  Copyright © 2020 Egy Designer. All rights reserved.
//

import Foundation
import ObjectMapper

class Work : Mappable {
    
    var image: String?
    var id: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        image <- map["image"]
        id <- map["id"]
    }
    
}
