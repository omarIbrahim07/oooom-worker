//
//  BidDetails.swift
//  OOOOM Worker
//
//  Created by Omar Ibrahim on 1/19/20.
//  Copyright © 2020 Egy Designer. All rights reserved.
//

import Foundation
import ObjectMapper

class BidDetails: Mappable {
    
    var id: Int?
    var message: String?
    var record: String?
    var image: String?
    var image2: String?
    var image3: String?
    var longitude: String?
    var latitude: String?
    var bidOfferPrice: Int?
    var name: String?
    var clientID: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        message <- map["message"]
        record <- map["record"]
        image <- map["image"]
        image2 <- map["image2"]
        image3 <- map["image3"]
        longitude <- map["longitude"]
        latitude <- map["latitude"]
        bidOfferPrice <- map["bidofferprice"]
        name <- map["user_name"]
        clientID <- map["user_id"]
    }
}
