//
//  BlockList.swift
//  OOOOM Service Provider
//
//  Created by Omar Ibrahim on 5/4/20.
//  Copyright © 2020 Egy Designer. All rights reserved.
//

import Foundation
import ObjectMapper

class BlockList: Mappable {
        
    var toId: Int?
    var updatedAt: String?
    var name: String?
        
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        toId <- map["to_id"]
        updatedAt <- map["updated_at"]
        name <- map["name"]
    }
    
}
