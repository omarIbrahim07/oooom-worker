//
//  UserSigned.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/3/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class UserSignedObj: Mappable {

    var success : Bool?
    var userSigned: UserSigned?
    
    required init?(map: Map) {
        
    }

    func mapping(map: Map) {
        success <- map["success"]
        userSigned <- map["data"]
    }

}

class UserSigned: Mappable {

    var id : Int?
    var firstName : String?
    var lastName : String?
    var email : String?
    var phone : String?
    var isVerified: Int?
    var areaID: Int?

    
    required init?(map: Map) {
        
    }

    func mapping(map: Map) {
        id <- map["id"]
        firstName <- map["first_name"]
        lastName <- map["last_name"]
        email <- map["email"]
        phone <- map["phone"]
        isVerified <- map["is_verified"]
        areaID <- map["area_id"]
    }

}
