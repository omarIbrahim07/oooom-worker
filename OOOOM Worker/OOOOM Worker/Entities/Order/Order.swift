//
//  Order.swift
//  OOOOM Worker
//
//  Created by Omar Ibrahim on 1/16/20.
//  Copyright © 2020 Egy Designer. All rights reserved.
//

import Foundation
import ObjectMapper

class Order : Mappable {
    
    var id: Int?
    var service: String?
    var serviceEn: String?
    var createdAt: String?
    var clientName: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        service <- map["service"]
        serviceEn <- map["serviceEN"]
        createdAt <- map["created_at"]
        clientName <- map["client_name"]
    }
    
}
