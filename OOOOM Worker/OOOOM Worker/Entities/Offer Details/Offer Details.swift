//
//  OfferDetails.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 2/16/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class OfferDetails: Mappable {
    
    var id: Int?
    var title: String?
    var price: Int?
    var description: String?
    var expireDate: String?
    var createdAt: String?
    var workerId: Int?
    var workerName: String?
    var serviceId: Int?
    var firstImage: String?
    var isSpecial: Int?
    var images: [String]?
    var specialDate: String?
    var areaID: Int?
    var technicianDetails: Technician?
            
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        price <- map["price"]
        description <- map["description"]
        expireDate <- map["expire_date"]
        createdAt <- map["created_at"]
        workerId <- map["worker_id"]
        serviceId <- map["service_id"]
        firstImage <- map["first_image"]
        isSpecial <- map["is_special"]
        images <- map["images"]
        specialDate <- map["special_date"]
        areaID <- map["area_id"]
        workerName <- map["worker_name"]
        technicianDetails <- map["worker"]
    }
    
}
