//
//  Package.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/22/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import Foundation
import ObjectMapper

    class DeliveryMethod: Mappable {
        
        var id : Int!
        var name : String!
        var termsOfContract : String!
        
        required init?(map: Map) {
            
        }
        
        func mapping(map: Map) {
            
            id <- map["id"]
            name <- map["name"]
            termsOfContract <- map["termsofcontract"]
        }
        
    }

    class AdditionalService: Mappable {
        
        var id : Int!
        var name : String!
        var price : String!
        
        required init?(map: Map) {
            
        }
        
        func mapping(map: Map) {
            
            id <- map["id"]
            name <- map["name"]
            price <- map["price"]
        }
    }


    
    class Location: Mappable {
        
        var id : Int!
        var name : String!
        var latitude : String!
        var longitude : String!
        
        required init?(map: Map) {
            
        }
        
        func mapping(map: Map) {
            
            id <- map["id"]
            name <- map["name"]
            latitude <- map["latitude"]
            longitude <- map["Longitude"]

        }
    }

class FeedbackItem: Mappable {
    
    var id : Int!
    var name : String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        
    }
}

class Category: Mappable {
    
    var id : Int!
    var name : String!
    var price : String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        price <- map["price"]
    }
    
}

class ShippingLocation: Mappable {
    
    var id : Int!
    var name : String!
    var latitude : String!
    var longitude : String!

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        latitude <- map["latitude"]
        longitude <- map["Longitude"]
    }
}

class Package: Mappable {
    
    var id : Int?
    var length : String!
    var width : String!
    var height: String!
    var dimensionUnit: String!
    var weight: String!
    var weightUnit: String!
    var breakable: String!
    var packaging: String!
    var storage: String!
    var price: String!
    var createdAt: String!
    var packageType: String!
    var fromLocation: String!
    var toLocation: String!
    var deliveryMethod: String!
    var orderStatus: String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        length <- map["length"]
        width <- map["width"]
        height <- map["height"]
        dimensionUnit <- map["dim_unit"]
        weight <- map["weight"]
        weightUnit <- map["weight_unit"]
        breakable <- map["breakable"]
        packaging <- map["packaging"]
        storage <- map["storage"]
        price <- map["price"]
        createdAt <- map["created_at"]
        packageType <- map["category"]
        fromLocation <- map["loc_from"]
        toLocation <- map["loc_to"]
        deliveryMethod <- map["DeliveryMethod"]
        orderStatus <- map["OrderStatus"]
    }
}

class OrderId: Mappable {
    
    var id : Int!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
    }
}

//class OrderInfo: Mappable {
//
//    var orderPrice : String!
//
//    required init?(map: Map) {
//
//    }
//
//    func mapping(map: Map) {
//
//        orderPrice <- map["order_price"]
//    }
//}
