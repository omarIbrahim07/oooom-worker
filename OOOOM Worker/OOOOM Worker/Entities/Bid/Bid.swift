//
//  Bid.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/18/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class Bid: Mappable {
    
    var id: Int?
    var createdAt: String?
    var service: BidService?
    var name: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        service <- map["service"]
        createdAt <- map["created_at"]
        name <- map["user_name"]
    }
    
}

class BidService: Mappable {
    
    var serviceName: String?
    var serviceNameEn: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        serviceName <- map["name"]
        serviceNameEn <- map["nameEN"]
    }
    
}

