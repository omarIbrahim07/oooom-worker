//
//  Offer.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 2/13/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class Offer: Mappable {
        
    var id: Int?
    var title: String?
    var price: Int?
    var description: String?
    var expireDate: String?
    var createdAt: String?
    var workerId: Int?
    var serviceId: Int?
    var firstImage: String?
    var isSpecial: Int?
    var approved: Int?
    var active: Int?
    var hide: Int?

        
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        price <- map["price"]
        description <- map["description"]
        expireDate <- map["expire_date"]
        createdAt <- map["created_at"]
        workerId <- map["worker_id"]
        serviceId <- map["service_id"]
        firstImage <- map["first_image"]
        isSpecial <- map["is_special"]
        approved <- map["approved"]
        active <- map["active"]
        hide <- map["hide"]
    }
    
}
