//
//  OrderDetails.swift
//  OOOOM Worker
//
//  Created by Omar Ibrahim on 2/23/20.
//  Copyright © 2020 Egy Designer. All rights reserved.
//

import Foundation
import ObjectMapper

class OrderDetails : Mappable {
    
    var id: Int?
    var service: String?
    var serviceEn: String?
    var createdAt: String?
    var clientName: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        service <- map["service"]
        serviceEn <- map["serviceEN"]
        createdAt <- map["created_at"]
        clientName <- map["client_name"]
    }
    
}
