//
//  Nationality.swift
//  OOOOM Worker
//
//  Created by Omar Ibrahim on 1/8/20.
//  Copyright © 2020 Egy Designer. All rights reserved.
//

import Foundation
import ObjectMapper

class Nationality: Mappable {
    
    var id: Int?
    var name: String?
    var nameEn: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        nameEn <- map["nameEN"]
    }
    
}
