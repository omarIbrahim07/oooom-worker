//
//  String+Extensions.swift
//  GameOn
//
//  Created by Hassan on 12/16/17.
//  Copyright © 2017 Hassan. All rights reserved.
//

import Foundation
let MaxTextField = 50
let MaxAge = 2
let MaxPhoneNumber = 15
let MinPassword = 6
let MaxPassword = 25
let MinUsername = 6
let MinName = 6
let MaxUsername = 40
extension String {
    
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }

    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return substring(from: fromIndex)
    }

    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return substring(to: toIndex)
    }

    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return substring(with: startIndex..<endIndex)
    }
    
    func isValidEmailAddress() -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = self as NSString
            let results = regex.matches(in: self, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    var localizeStringNumbers : String {
        get{
            let formatter = NumberFormatter()
            var localizedString = self
            formatter.locale = NSLocale(localeIdentifier: "ar") as Locale
            for i in 0..<10 {
                
                let num = i
                
                guard let formattedNumber = formatter.string(from: NSNumber.init(value: num)) else {
                    continue
                }
                
                localizedString = localizedString.replacingOccurrences(of: "\(num)", with: formattedNumber)
            }
            
            return localizedString
        }
    }

        var html2AttributedString: NSAttributedString? {
            return Data(utf8).html2AttributedString
        }
        var html2String: String {
            return html2AttributedString?.string ?? ""
        }
    
    var isValidUsername: Bool {
        let usernameRegex = "^[A-Za-z][A-Za-z0-9_.]{\(MinUsername - 1),}$"
        let usernameTest = NSPredicate(format: "SELF MATCHES %@", usernameRegex)
        return usernameTest.evaluate(with: self)
    }
    
    var trimmed: String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    var containsAtLeastOneLetter: Bool {
        let alphaNumericRegex = ".*[a-zA-Z]+.*"
        let alphaNumericTest = NSPredicate(format: "SELF MATCHES %@", alphaNumericRegex)
        return alphaNumericTest.evaluate(with: self)
    }
    
    var isValidName: Bool {
        let trimmedString = self.withoutWhitespaces
        if (trimmedString.count > MinName) {
            let stringSet = CharacterSet(charactersIn: trimmedString)
            return CharacterSet.letters.isSuperset(of: stringSet)
        }
        return false
    }
    
    var isAlphaNumberic: Bool {
        let alphaNumericRegex = "^[a-zA-Z0-9_]*$"
        let alphaNumericTest = NSPredicate(format: "SELF MATCHES %@", alphaNumericRegex)
        return alphaNumericTest.evaluate(with: self)
    }
    
    var isValidPassword: Bool {
        let passwordRegex = "^[A-Za-z0-9#?!@$%^&*-_.]{\(MinPassword),}$"
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", passwordRegex)
        return passwordTest.evaluate(with: self)
    }
    
    var withoutNumbers: String {
        let words = self.components(separatedBy: CharacterSet.decimalDigits)
        let trimmedString = words.joined()
        return trimmedString
    }
    
    var isValidMobileNumber: Bool {
        if self.count == 11 {
            let mobileNumberRegex = "^(\\+201|00201|01|)(0|1|2|5)([0-9]{8})$"
            let mobileNumberTest = NSPredicate(format: "SELF MATCHES %@", mobileNumberRegex)
            return mobileNumberTest.evaluate(with: self.withWesternNumbers)
        }
        else {
            return false
        }
    }
    
    var withoutWhitespaces: String {
        let words = self.components(separatedBy: CharacterSet.whitespacesAndNewlines)
        let trimmedString = words.joined()
        return trimmedString
    }
    
    var withoutSpecialCharacters: String {
        var alphaNumericCharacters = CharacterSet.alphanumerics
        alphaNumericCharacters.formUnion(CharacterSet.whitespacesAndNewlines)
        let symbolCharacters = alphaNumericCharacters.inverted
        let words = self.components(separatedBy: symbolCharacters)
        let trimmedString = words.joined()
        return trimmedString
    }
    
    var isNumeric: Bool {
        let stringSet = CharacterSet(charactersIn: self.withWesternNumbers)
        return CharacterSet.decimalDigits.isSuperset(of: stringSet)
    }
    
    var isValidAge: Bool {
        return self.isNumeric && (Int(self) != nil) && (Int(self)! > 0)
    }
    
    var withWesternNumbers: String {
        var string = self
        let arabic = "٠١٢٣٤٥٦٧٨٩"
        let western = "0123456789"
        for i in 0..<arabic.count {
            let arabicCharacter = arabic[arabic.index(arabic.startIndex, offsetBy: i)]
            let westernCharacter = western[western.index(western.startIndex, offsetBy: i)]
            string = string.replacingOccurrences(of: "\(arabicCharacter)",
                with: "\(westernCharacter)")
        }
        return string
    }
    
    var withArabicNumbers: String {
        var string = self
        let arabic = "٠١٢٣٤٥٦٧٨٩"
        let western = "0123456789"
        for i in 0..<arabic.count {
            let arabicCharacter = arabic[arabic.index(arabic.startIndex, offsetBy: i)]
            let westernCharacter = western[western.index(western.startIndex, offsetBy: i)]
            string = string.replacingOccurrences(of: "\(westernCharacter)",
                with: "\(arabicCharacter)")
        }
        return string
    }
    
    func removingOccurrences(of stringToRemove: String) -> String {
        return self.replacingOccurrences(of: stringToRemove, with: "")
    }
    
    var isAlphaNumeric: Bool {
        return CharacterSet.alphanumerics.isSuperset(of: CharacterSet(charactersIn: self))
    }
    
    var hasAtLeastOneLetter: Bool {
        return !self.components(separatedBy: CharacterSet.letters.inverted).joined().isEmpty
    }
    
    func hasNumberOfLetters(_ count: Int) -> Bool {
        return self.components(separatedBy: CharacterSet.letters.inverted).count == count
    }
    
    var detectPhoneNumber: String? {
        return (try? NSDataDetector(types: NSTextCheckingAllTypes))?.firstMatch(in: self, range: NSRange(0..<self.count))?.phoneNumber
    }

    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
