//
//  UIViewController+Extensions.swift
//  GameOn
//
//  Created by Hassan on 3/21/18.
//  Copyright © 2018 Hassan. All rights reserved.
//

import UIKit

extension UIViewController {

    var isModal: Bool {
        if let count = self.navigationController?.viewControllers.count, count > 0 {
            if self.navigationController?.viewControllers[0] != self {
                return false
            }
        }
        let presentingIsModal = presentingViewController != nil
        let presentingIsNavigation = navigationController?.presentingViewController?.presentedViewController == navigationController
        let presentingIsTabBar = tabBarController?.presentingViewController is UITabBarController
        
        return presentingIsModal || presentingIsNavigation || presentingIsTabBar
    }

    func configureNavigationBarWithCloseButton() {
        self.navigationController?.navigationBar.adjustDefaultNavigationBar()
        if isModal {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "CloseIcon"), style: .plain, target: self, action: #selector(exitAction))
        }
    }
    
    @objc fileprivate func exitAction() {
        self.dismiss(animated: true, completion: nil)
    }
}
