//
//  OrderAPIManager.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/24/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class OrderAPIManager: BaseAPIManager {
    
    func getWorkerWorks(basicDictionary params:APIParams , onSuccess: @escaping ([Work])->Void, onFailure: @escaping  (APIError)->Void) {

    let engagementRouter = BaseRouter(method: .get, path: GET_USER_URL, parameters: params)

    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let user: [String : Any] = response["user"] as? [String : Any], let jsonArray: [[String : Any]] = user["works"] as? [[String : Any]] {
            let wrapper = Mapper<Work>().mapArray(JSONArray: jsonArray)
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getNewOrders(basicDictionary params:APIParams , onSuccess: @escaping ([Order])->Void, onFailure: @escaping  (APIError)->Void) {
    
    let engagementRouter = BaseRouter(method: .get, path: GET_NEW_ORDERS_URL, parameters: params)
    
    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
            let wrapper = Mapper<Order>().mapArray(JSONArray: jsonArray)
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getBids(basicDictionary params:APIParams , onSuccess: @escaping ([Bid])->Void, onFailure: @escaping  (APIError)->Void) {
    
    let engagementRouter = BaseRouter(method: .get, path: GET_BIDS_URL, parameters: params)
    
    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
            let wrapper = Mapper<Bid>().mapArray(JSONArray: jsonArray)
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getBidDetails(basicDictionary params:APIParams , onSuccess: @escaping (BidDetails)->Void, onFailure: @escaping  (APIError)->Void) {

        let engagementRouter = BaseRouter(method: .get, path: GET_BID_DETAILS_URL, parameters: params)

        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let json: [String : Any] = response["data"] as? [String : Any], let wrapper = Mapper<BidDetails>().map(JSON: json) {
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }

    func sendBidPrice(basicDictionary params:APIParams, onSuccess: @escaping (Bool) -> Void, onFailure: @escaping (APIError) -> Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: POST_BID_OFFER_PRICE_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let sent = response["saved"] as? Bool {
                
                onSuccess(sent)
            }
            
        }) { (apiError) in
            if apiError.responseStatusCode == 401 || apiError.responseStatusCode == 422 {
                apiError.message = "لم يتم الارسال"
            }
            onFailure(apiError)
        }
    }
    
    func sendReport(basicDictionary params:APIParams, onSuccess: @escaping (Bool) -> Void, onFailure: @escaping (APIError) -> Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: SEND_REPORT_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let saved = response["saved"] as? Bool {
                
                onSuccess(saved)
            }
            
        }) { (apiError) in
            if apiError.responseStatusCode == 500 || apiError.responseStatusCode == 422 {
                if "Lang".localized == "ar" {
                    apiError.message = "عذرًا لقد تم ارسال الشكوى من قبل"
                } else if "Lang".localized == "en" {
                    apiError.message = "Sorry you report this before"
                }
            }
            onFailure(apiError)
        }
    }

    func getOrderDetails(basicDictionary params:APIParams , onSuccess: @escaping (OrderSent) -> Void, onFailure: @escaping  (APIError) -> Void) {

        let engagementRouter = BaseRouter(method: .get, path: GET_ORDER_DETAILS_URL, parameters: params)

        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let json: [String : Any] = response["data"] as? [String : Any], let wrapper = Mapper<OrderSent>().map(JSON: json) {
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func submitActionOnOrder(basicDictionary params:APIParams , onSuccess: @escaping (Int)->Void, onFailure: @escaping  (APIError)->Void) {

        let engagementRouter = BaseRouter(method: .post, path: SUBMIT_ACTION_ON_ORDER_URL, parameters: params)

        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let wrapper: Int = response["order_id"] as? Int {
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func SendOrder(basicDictionary params:APIParams , onSuccess: @escaping (Int)->Void, onFailure: @escaping  (APIError)->Void) {

        let engagementRouter = BaseRouter(method: .post, path: SEND_ORDER_URL, parameters: params)

        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
           
        if let response: [String : Any] = responseObject as? [String : Any], let orderID: Int = response["order_id"] as? Int {
        onSuccess(orderID)
        }
           
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
       
        }) { (apiError) in
            onFailure(apiError)
        }
     }
    
    func rateOrder(basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void) {

       let engagementRouter = BaseRouter(method: .post, path: RATE_ORDER_URL, parameters: params)

       super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
          
       if let response: [String : Any] = responseObject as? [String : Any], let saved: Bool = response["saved"] as? Bool {
       onSuccess(saved)
       }
          
       else {
           let apiError = APIError()
           onFailure(apiError)
       }
      
       }) { (apiError) in
           onFailure(apiError)
       }
    }
    
    func sendBidWithRecord(recordData: Data?, recordName: String, basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void) {

        let engagementRouter = BaseRouter(method: .post, path: SEND_BID_URL, parameters: params)

        super.performAudioUploadNetworkRequest(songName: recordName, songData: recordData, forRouter: engagementRouter, onSuccess: { (responseObject) in

         if let response: [String : Any] = responseObject as? [String : Any], let saved: Bool = response["saved"] as? Bool {
         onSuccess(saved)
         }
            
         else {
             let apiError = APIError()
             onFailure(apiError)
         }
        
         }) { (apiError) in
             onFailure(apiError)
         }
    }
    
    func sendBid(recordData: Data?, recordName: String, imgData : Data?, imgData2 : Data?, imgData3 : Data?, basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void) {

        let engagementRouter = BaseRouter(method: .post, path: SEND_BID_URL, parameters: params)

        super.performUploadThreeImagesWithRecordNetworkRequest(songName: recordName, songData: recordData, firstImageData: imgData, secondImageData: imgData2, thirdImageData: imgData3, forRouter: engagementRouter, onSuccess: { (responseObject) in

         if let response: [String : Any] = responseObject as? [String : Any], let saved: Bool = response["saved"] as? Bool {
         onSuccess(saved)
         }
            
         else {
             let apiError = APIError()
             onFailure(apiError)
         }
        
         }) { (apiError) in
             onFailure(apiError)
         }
    }

    
    func sendBidThreeImages(imgData : Data?, imgData2 : Data?, imgData3 : Data? ,basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void) {

        let engagementRouter = BaseRouter(method: .post, path: SEND_BID_URL, parameters: params)

        super.performUploadThreeImagesNetworkRequest(firstImageData: imgData, secondImageData: imgData2, thirdImageData: imgData3, forRouter: engagementRouter, onSuccess: { (responseObject) in

            if let response: [String : Any] = responseObject as? [String : Any], let message: Bool = response["saved"] as? Bool {
//                UserDefaultManager.shared.currentUser = userWrapper

                onSuccess(message)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }

        }) { (apiError) in
            onFailure(apiError)
        }
    }

    
}
