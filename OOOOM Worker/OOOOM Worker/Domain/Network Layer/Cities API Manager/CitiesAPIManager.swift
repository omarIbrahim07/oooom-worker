//
//  CitiesAPIManager.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/27/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class CitiesAPIManager: BaseAPIManager {

    func getCities(basicDictionary params:APIParams , onSuccess: @escaping ([City])->Void, onFailure: @escaping  (APIError)->Void) {

    let engagementRouter = BaseRouter(method: .get, path: GET_CITIES_URL, parameters: params)

    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
            let wrapper = Mapper<City>().mapArray(JSONArray: jsonArray)
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getNationalities(basicDictionary params:APIParams , onSuccess: @escaping ([Nationality])->Void, onFailure: @escaping  (APIError)->Void) {

    let engagementRouter = BaseRouter(method: .get, path: GET_NATIONALITIES_URL, parameters: params)

    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
            let wrapper = Mapper<Nationality>().mapArray(JSONArray: jsonArray)
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getServices(basicDictionary params:APIParams , onSuccess: @escaping ([Service])->Void, onFailure: @escaping  (APIError)->Void) {

    let engagementRouter = BaseRouter(method: .get, path: GET_SERVICES_URL, parameters: params)

    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
            let wrapper = Mapper<Service>().mapArray(JSONArray: jsonArray)
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }

}
