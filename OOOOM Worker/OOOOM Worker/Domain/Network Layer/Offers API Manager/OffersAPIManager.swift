//
//  OffersAPIManager.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/28/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class OffersAPIManager: BaseAPIManager {

    func sendPromoCode(basicDictionary params:APIParams , onSuccess: @escaping (PromocodeResponse)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: SEND_PROMOCODE_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let wrapper = Mapper<PromocodeResponse>().map(JSON: response) {
                   
            onSuccess(wrapper)

            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getPromoCode(basicDictionary params:APIParams , onSuccess: @escaping (PromocodeObj)->Void, onFailure: @escaping  (APIError)->Void) {
           
       let engagementRouter = BaseRouter(method: .get, path: GET_PROMOCODE_URL, parameters: params)
       
       super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
           
        if let response: [String : Any] = responseObject as? [String : Any], let wrapper = Mapper<PromocodeObj>().map(JSON: response) {

            onSuccess(wrapper)
           }
           else {
               let apiError = APIError()
               onFailure(apiError)
           }
           
       }) { (apiError) in
           onFailure(apiError)
       }
   }
    
    func getOffers(basicDictionary params:APIParams , onSuccess: @escaping ([Offer])->Void, onFailure: @escaping  (APIError)->Void) {

    let engagementRouter = BaseRouter(method: .get, path: GET_OFFERS_URL, parameters: params)

    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
            let wrapper = Mapper<Offer>().mapArray(JSONArray: jsonArray)
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getOfferDetails(basicDictionary params:APIParams , onSuccess: @escaping (OfferDetails)->Void, onFailure: @escaping  (APIError)->Void) {
    
    let engagementRouter = BaseRouter(method: .get, path: GET_OFFER_DETAILS_URL, parameters: params)
    
    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let json: [String : Any] = response["data"] as? [String : Any], let wrapper = Mapper<OfferDetails>().map(JSON: json) {
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func activeOffer(basicDictionary params:APIParams, onSuccess: @escaping (Int) -> Void, onFailure: @escaping (APIError) -> Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: ACTIVATE_OFFER_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let sent = response["active"] as? Int {
                
                onSuccess(sent)
            }
            
        }) { (apiError) in
            if apiError.responseStatusCode == 401 || apiError.responseStatusCode == 422 {
                apiError.message = "لم يتم الارسال"
            }
            onFailure(apiError)
        }
    }

    func makeOfferSpecial(basicDictionary params:APIParams, onSuccess: @escaping (String) -> Void, onFailure: @escaping (APIError) -> Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: MAKE_ORDER_SPECIAL_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let sent = response["data"] as? String {
                
                onSuccess(sent)
            }
            
        }) { (apiError) in
            if apiError.responseStatusCode == 401 || apiError.responseStatusCode == 422 {
                apiError.message = "لم يتم الارسال"
            }
            onFailure(apiError)
        }
    }

    func deleteOffer(basicDictionary params:APIParams, onSuccess: @escaping (String) -> Void, onFailure: @escaping (APIError) -> Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: DELETE_OFFER_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let sent = response["data"] as? String {
                
                onSuccess(sent)
            }
            
        }) { (apiError) in
            if apiError.responseStatusCode == 401 || apiError.responseStatusCode == 422 {
                apiError.message = "لم يتم الارسال"
            }
            onFailure(apiError)
        }
    }

    func getSpecialOfferPrice(basicDictionary params:APIParams, onSuccess: @escaping (String) -> Void, onFailure: @escaping (APIError) -> Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_SPECIAL_OFFER_PRICE_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let setting: [String : Any] = response["setting"] as? [String : Any], let value: String = setting["value"] as? String {
                onSuccess(value)
            }
            
        }) { (apiError) in
            if apiError.responseStatusCode == 401 || apiError.responseStatusCode == 422 {
                apiError.message = "لم يتم الارسال"
            }
            onFailure(apiError)
        }
    }

    func addOfferWithMainImage(imageData imgData : Data? ,basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void) {

        let engagementRouter = BaseRouter(method: .post, path: ADD_OFFER_URL, parameters: params)

        super.performUploadNetworkRequestMainOfferImage(withName: "first_image" ,imageData: imgData,forRouter: engagementRouter, onSuccess: { (responseObject) in

            if let response: [String : Any] = responseObject as? [String : Any], let message: Bool = response["saved"] as? Bool {
//                UserDefaultManager.shared.currentUser = userWrapper

                onSuccess(message)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }

        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func addOfferWithExtraImages(imageData imgData : Data?, imageDataArray: [Data], basicDictionary params:APIParams , onSuccess: @escaping ()->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: ADD_OFFER_URL, parameters: params)

        super.performUploadNetworkRequestArrayOfImagesWithImage(withName: "first_image", imageData: imgData, nameOfArrayImages: "images[]", imageDataArray: imageDataArray, forRouter: engagementRouter, onSuccess: { (response) in
            onSuccess()
        }) { (error) in
            onFailure(error)
        }
    }


    
}
