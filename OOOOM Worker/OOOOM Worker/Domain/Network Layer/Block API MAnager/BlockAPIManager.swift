//
//  BlockAPIManager.swift
//  OOOOM Service Provider
//
//  Created by Omar Ibrahim on 5/4/20.
//  Copyright © 2020 Egy Designer. All rights reserved.
//

import Foundation
import ObjectMapper

class BlockAPIManager: BaseAPIManager {
        
    func getBlockList(basicDictionary params:APIParams , onSuccess: @escaping ([BlockList])->Void, onFailure: @escaping  (APIError)->Void) {
    
    let engagementRouter = BaseRouter(method: .get, path: BLOCK_LIST_URL, parameters: params)
    
    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
            let wrapper = Mapper<BlockList>().mapArray(JSONArray: jsonArray)
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }
            
    func removeBlock(basicDictionary params:APIParams, onSuccess: @escaping (Bool) -> Void, onFailure: @escaping (APIError) -> Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: UNBLOCK_USER_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let saved = response["saved"] as? Bool {
                
                onSuccess(saved)
            }
            
        }) { (apiError) in
            if apiError.responseStatusCode == 500 || apiError.responseStatusCode == 422 {
//                if "Lang".localized == "ar" {
//                    apiError.message = "عذرًا لقد تم ارسال الشكوى من قبل"
//                } else if "Lang".localized == "en" {
//                    apiError.message = "User unblocked sucessfully"
//                }
            }
            onFailure(apiError)
        }
    }
    
    func blockUser(basicDictionary params:APIParams, onSuccess: @escaping (Bool) -> Void, onFailure: @escaping (APIError) -> Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: BLOCK_USER_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let saved = response["saved"] as? Bool {
                
                onSuccess(saved)
            }
            
        }) { (apiError) in
            if apiError.responseStatusCode == 500 || apiError.responseStatusCode == 422 {
//                if "Lang".localized == "ar" {
//                    apiError.message = "عذرًا لقد تم ارسال الشكوى من قبل"
//                } else if "Lang".localized == "en" {
//                    apiError.message = "User unblocked sucessfully"
//                }
            }
            onFailure(apiError)
        }
    }

    
}
