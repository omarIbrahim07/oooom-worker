//
//  BaseRouter.swift
//  GameOn
//
//  Created by Hassan on 12/16/17.
//  Copyright © 2017 Hassan. All rights reserved.
//

import Foundation
import Alamofire

public typealias JSONDictionary = [String: AnyObject]
typealias APIParams = [String : AnyObject]?

protocol APIConfiguration
{
    var method: Alamofire.HTTPMethod { get set }
    var encoding: Alamofire.ParameterEncoding? { get }
    var path: String { get set }
    var parameters: APIParams { get set }
    var baseURLString: String { get }
    var requestHeaders : [String : Any] { get set }
}

class BaseRouter : URLRequestConvertible, APIConfiguration
{
    var path: String
    
    var parameters: APIParams
    
    var method: HTTPMethod
    
    var requestHeaders: [String : Any]
    
    init(method: HTTPMethod, path: String, parameters: APIParams) {
        self.method = method
        self.path = path
        self.parameters = parameters
        self.requestHeaders = [:]
        if let authorization = UserDefaultManager.shared.authorization {
            self.requestHeaders.updateValue(authorization, forKey: "Authorization")
        }
    }
    
    var encoding: ParameterEncoding?{
        switch method {
        case .get:
            return URLEncoding.default
        case .post:
            return JSONEncoding.default
        default:
            return URLEncoding.default
        }
    }
    
    var baseURLString : String {
        return MainURL
    }
    
    /// Returns a URL request or throws if an `Error` was encountered.
    ///
    /// - throws: An `Error` if the underlying `URLRequest` is `nil`.
    ///
    /// - returns: A URL request.
    func asURLRequest() throws -> URLRequest {
        
        let url = URL(string: baseURLString)!
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        for (key,value) in requestHeaders
        {
            urlRequest.setValue(value as? String, forHTTPHeaderField: key)
        }
        
        return try encoding!.encode(urlRequest, with: parameters)
    }
    
}
