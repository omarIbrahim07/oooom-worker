//
//  FeedBacks API Manager.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/27/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import Foundation
import ObjectMapper

class FeedbacksAPIManager: BaseAPIManager {
    
    func giveFeedback(basicDictionary params:APIParams, onSuccess: @escaping (Bool) -> Void, onFailure: @escaping (APIError) -> Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: GIVE_FEEDBACK, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let sent = response["saved"] as? Bool {
                
                onSuccess(sent)
            }
            
        }) { (apiError) in
            if apiError.responseStatusCode == 401 || apiError.responseStatusCode == 422 {
                apiError.message = "لم يتم الارسال"
            }
            onFailure(apiError)
        }
    }
    
}
