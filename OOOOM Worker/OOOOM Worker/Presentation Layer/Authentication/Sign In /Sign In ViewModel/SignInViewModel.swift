//
//  SignInViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/1/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

class SignInViewModel {
    
    var error: APIError?

    var reloadCitiesClosure: (()->())?
    var chooseCityClosure: (()->())?
    var reloadNationalitiesClosure: (()->())?
    var chooseNationalityClosure: (()->())?
    var reloadServicesClosure: (()->())?
    var chooseServiceClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var updateGender: (()->())?
    var updateUser: (()->())?
    var updateID: (()->())?
    
    // callback for interfaces
    var user: UserSigned? {
        didSet {
            updateID?()
        }
    }
    
    var id: Int? {
        didSet {
//            updateUser?()
            updateID?()
        }
    }
    
    // callback for interfaces
    var cities: [City]? {
        didSet {
            self.reloadCitiesClosure?()
        }
    }
    
    var nationalities: [Nationality]? {
        didSet {
            self.reloadNationalitiesClosure?()
        }
    }
    
    var services: [Service]? {
        didSet {
            self.reloadServicesClosure?()
        }
    }
        
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    // callback for interfaces
    var choosedCity: City? {
        didSet {
            self.chooseCityClosure?()
        }
    }
    
    var choosedNationality: Nationality? {
        didSet {
            self.chooseNationalityClosure?()
        }
    }
    
    var choosedService: Service? {
        didSet {
            self.chooseServiceClosure?()
        }
    }


    func initFetch(locationID: Int) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "location_id" : locationID as AnyObject,
        ]
        
        CitiesAPIManager().getCities(basicDictionary: params, onSuccess: { (cities) in

        self.cities = cities // Cache
        self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func fetchNationalities() {
        state = .loading
        
        let params: [String : AnyObject] = [:]
        
        CitiesAPIManager().getNationalities(basicDictionary: params, onSuccess: { (nationalities) in

        self.nationalities = nationalities // Cache
        self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func fetchServices() {
        state = .loading
        
        let params: [String : AnyObject] = [:]
        
        CitiesAPIManager().getServices(basicDictionary: params, onSuccess: { (services) in

        self.services = services // Cache
        self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }

    
//    func signUp(imgData: Data?, imgData2: Data?, firstName: String, lastName: String, email: String, password: String, phone: String, areaID: Int, age: Int, serviceID: Int, price: Int, idNumber: String, fcmToken: String, nationalityID: Int, latitude: String, longitude: String) {
    func signUp(imgData: Data?, imgData2: Data?, firstName: String, lastName: String, email: String, password: String, phone: String, areaID: Int, serviceID: Int, price: Int, fcmToken: String, latitude: String, longitude: String) {

        
        let params: [String : AnyObject] = [
            "first_name" : firstName as AnyObject,
            "last_name" : lastName as AnyObject,
            "email" : email as AnyObject,
            "password" : password as AnyObject,
            "phone" : phone as AnyObject,
            "area_id" : areaID as AnyObject,
            "role_id" : 5 as AnyObject,
//            "age" : age as AnyObject,
            "service_id" : serviceID as AnyObject,
            "price" : price as AnyObject,
//            "id_number" : idNumber as AnyObject,
            "fcm_token" : fcmToken as AnyObject,
            "app_id" : 5 as AnyObject,
//            "nationality_id" : nationalityID as AnyObject,
            "lat" : latitude as AnyObject,
            "long" : longitude as AnyObject
        ]
        
        self.state = .loading
        
        AuthenticationAPIManager().registerUser(profileImage: imgData, idImage: imgData2, basicDictionary: params, onSuccess: { (id) in
            
            self.id = id
            self.state = .populated

        }) { (error) in
            self.state = .populated
            self.makeError(message: "email or phone is used before")
        }
    }
    
    func makeError(message: String) {
        let apiError = APIError()
        apiError.message = message
        self.error = apiError
        self.state = .error
    }

    func getUser() -> UserSigned {
        return user!
    }
    
    func getID() -> Int {
        return id!
    }
    
    func getCities() -> [City] {
        return cities!
    }
    
    func getNationalities() -> [Nationality] {
        return nationalities!
    }
    
    func getServices() -> [Service] {
        return services!
    }
            
    func getError() -> APIError {
        return self.error!
    }

    func getCity(city: City) {
        self.choosedCity = city
    }
    
    func getNationality(nationality: Nationality) {
        self.choosedNationality = nationality
    }
    
    func getService(service: Service) {
        self.choosedService = service
    }
    
}
