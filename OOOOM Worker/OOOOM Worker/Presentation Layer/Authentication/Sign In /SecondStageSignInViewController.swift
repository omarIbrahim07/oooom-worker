//
//  SecondStageSignInViewController.swift
//  OOOOM Worker
//
//  Created by Omar Ibrahim on 1/7/20.
//  Copyright © 2020 Egy Designer. All rights reserved.
//

import UIKit
import AssetsPickerViewController
import Toast_Swift
import PKHUD
import Photos

class SecondStageSignInViewController: BaseViewController {
    
    lazy var viewModel: SignInViewModel = {
        return SignInViewModel()
    }()

    var firstName: String?
    var lastName: String?
    var email: String?
    var password: String?
    var phone: String?
    var areaID: Int?
    var age: Int?
    var serviceID: Int?
    var inspectionExpenses: Int?
    var idNumber: String?
    var fcmToken: String?
    var nationalityID: Int?
    var latitude: String?
    var longitude: String?
    
    var error: APIError?
    var id: Int?
    
    var myFirstImage: UIImage? = UIImage()
    var mySecondImage: UIImage? = UIImage()
    
    var choosedImage: Int?
    var firstImageData: Data?
    var secondImageData: Data?
    
    @IBOutlet weak var logoView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var idImageView: UIImageView!
    @IBOutlet weak var signUpButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        showImagesAlert()
        
        initVM()
    }
    
    // MARK:- Configuration
    func configureView() {
        signUpButton.setTitle("sign up".localized, for: .normal)
        self.navigationItem.title = "sign up".localized
        logoView.roundCorners([.bottomLeft, .bottomRight], radius: 30.0)
        bottomView.roundCorners([.topLeft, .topRight], radius: 30.0)
        signUpButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func showImagesAlert() {
        let alertController = UIAlertController(title: "images alert title".localized, message: "images alert message".localized, preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let cancelAction = UIAlertAction(title: "cancel images alert".localized, style: .cancel)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }

    // MARK:- Init View Model
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.showError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
        
        viewModel.updateID = { [weak self] () in
            DispatchQueue.main.async { [weak self] () in
                self?.id = self!.viewModel.getID()
                if let id: Int = self?.id {
                    self!.goToVerifyPhone(id: id)
                }
            }
        }

    }

    
    func imagePressed() {
        
        let image = UIImagePickerController()
        
        image.delegate = self
        
        image.sourceType = .photoLibrary
        
        self.present(image, animated: true) {
            
        }
    }
    
    func signUp() {
        
        if myFirstImage?.jpegData(compressionQuality: 0.3) == nil || mySecondImage?.jpegData(compressionQuality: 0.3) == nil {
            self.showImagesAlert()
        }
        
        if let imgData = myFirstImage {
            firstImageData = imgData.jpegData(compressionQuality: 0.3)
        }
        
        if let imgData = mySecondImage {
            secondImageData = imgData.jpegData(compressionQuality: 0.3)
        }

//        viewModel.signUp(imgData: firstImageData, imgData2: secondImageData, firstName: firstName!, lastName: lastName!, email: email!, password: password!, phone: phone!, areaID: areaID!, age: age!, serviceID: serviceID!, price: inspectionExpenses!, idNumber: idNumber ?? "", fcmToken: fcmToken!, nationalityID: nationalityID ?? 0, latitude: latitude!, longitude: longitude!)
        viewModel.signUp(imgData: firstImageData, imgData2: secondImageData, firstName: firstName!, lastName: lastName!, email: email!, password: password!, phone: phone!, areaID: areaID!, serviceID: serviceID!, price: inspectionExpenses!, fcmToken: fcmToken!, latitude: latitude!, longitude: longitude!)
    }
    
    
    
    // MARK:- Actions
    @IBAction func profileImageButtonIsPressed(_ sender: Any) {
        print("Profile pic button pressed")
        self.choosedImage = 1
        imagePressed()
    }
    
    @IBAction func idImageButtonIsPressed(_ sender: Any) {
        print("id pic button pressed")
        self.choosedImage = 2
        imagePressed()
    }
    
    @IBAction func signUpButtonIsPressed(_ sender: Any) {
        signUp()
    }
    
    // MARK:- Navigation
    func goToVerifyPhone(id: Int) {
        if let verifyPhoneNavVC = UIStoryboard(name: "Authentication", bundle: nil).instantiateViewController(withIdentifier: "VerifyPhoneNavigationVC") as? UINavigationController, let rootViewContoller = verifyPhoneNavVC.viewControllers[0] as? VerifyPhoneViewController {
                rootViewContoller.id = id
            self.present(verifyPhoneNavVC, animated: true, completion: nil)
        }
    }
    
}

// MARK:- Image Picker delegate
extension SecondStageSignInViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            if choosedImage == 1 {
                profileImageView.image = image
                profileImageView.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
                myFirstImage = image
            } else if choosedImage == 2 {
                idImageView.image = image
                idImageView.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
                mySecondImage = image
            }
            self.dismiss(animated: true, completion: nil)
        }
    }
}
