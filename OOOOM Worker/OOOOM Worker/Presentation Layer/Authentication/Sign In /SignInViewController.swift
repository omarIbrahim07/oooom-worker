//
//  SignInViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 8/26/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import CoreLocation

class SignInViewController: BaseViewController {
    
    lazy var viewModel: SignInViewModel = {
        return SignInViewModel()
    }()
    
    var checked: Bool = false
    var error: APIError?
    var user: UserSigned?
    
    var cities: [City] = [City]()
    var choosedCity: City?
    var nationalities: [Nationality] = [Nationality]()
    var choosedNAtionality: Nationality?
    var services: [Service] = [Service]()
    var choosedService: Service?
    
    var longitude: String?
    var latitude: String?
    
    let locationManager = CLLocationManager()
    
    @IBOutlet weak var logoView: UIView!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var idNumberTextfield: UITextField!
    @IBOutlet weak var inspectionExpensesTextfield: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var serviceLabel: UILabel!
    @IBOutlet weak var nationalityLabel: UILabel!
    @IBOutlet weak var agreeOnTermsLabel: UILabel!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var havingAccountButton: UIButton!
    
    override func viewDidLoad() {
                
        super.viewDidLoad()
        configureView()
        closeKeypad()
        setIcon()
        
        locationManager.requestWhenInUseAuthorization()
        checkLocationManager()

        // Do any additional setup after loading the view.
        initVM()
        showOptionalFieldsPopUp()
    }
    
    func checkLocationManager() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    // MARK:- Init View Model
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.showError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
        
        viewModel.reloadCitiesClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] () in
                self?.cities = self!.viewModel.getCities()
            }
        }
        
        viewModel.reloadNationalitiesClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] () in
                self?.nationalities = self!.viewModel.getNationalities()
            }
        }
        
        viewModel.reloadServicesClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] () in
                self?.services = self!.viewModel.getServices()
            }
        }
        
        viewModel.updateUser = { [weak self] () in
            DispatchQueue.main.async { [weak self] () in
                self?.user = self!.viewModel.getUser()
                if let user: UserSigned = self?.user {
                    if let userId: Int = user.id {
                        self!.goToVerifyPhone(id: userId)
                    }
                }
            }
            
        }
            
        viewModel.chooseCityClosure = { [weak self] () in
            
            DispatchQueue.main.async { [weak self] in
                self?.choosedCity = self?.viewModel.choosedCity
                if let city = self?.choosedCity {
                    self?.bindCity(city: city)
                }
            }
        }
        
        viewModel.chooseServiceClosure = { [weak self] () in
            
            DispatchQueue.main.async { [weak self] in
                self?.choosedService = self?.viewModel.choosedService
                if let service = self?.choosedService {
                    self?.bindService(service: service)
                }
            }
        }

        viewModel.chooseNationalityClosure = { [weak self] () in
            
            DispatchQueue.main.async { [weak self] in
                self?.choosedNAtionality = self?.viewModel.choosedNationality
                if let nationality = self?.choosedNAtionality {
//                    self?.bindNationality(nationality: nationality)
                }
            }
        }
                
        viewModel.initFetch(locationID: 1)
        viewModel.fetchNationalities()
        viewModel.fetchServices()
    }
    
    func showOptionalFieldsPopUp() {
        let alertController = UIAlertController(title: "optional fields alert title".localized, message: "optional fields alert message".localized, preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let cancelAction = UIAlertAction(title: "optional fields alert".localized, style: .cancel)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func bindCity(city: City) {
        if "Lang".localized == "ar" {
            self.cityLabel.text = city.name
        } else if "Lang".localized == "en" {
            self.cityLabel.text = city.nameEn
        }
    }
    
    func bindService(service: Service) {
        if "Lang".localized == "ar" {
            self.serviceLabel.text = service.name
        } else if "Lang".localized == "en" {
            self.serviceLabel.text = service.nameEN
        }
    }

//    func bindNationality(nationality: Nationality) {
//        if "Lang".localized == "ar" {
//            self.nationalityLabel.text = nationality.name
//        } else if "Lang".localized == "en" {
//            self.nationalityLabel.text = nationality.nameEn
//        }
//    }

    // MARK:- Show NAtionalities Options Picker
    func setupNationalitiesSheet() {
        
        let sheet = UIAlertController(title: "nationality alert title".localized, message: "nationality alert message".localized, preferredStyle: .actionSheet)
        for nationality in nationalities {
            if "Lang".localized == "ar" {
                sheet.addAction(UIAlertAction(title: nationality.name, style: .default, handler: {_ in
                    self.viewModel.getNationality(nationality: nationality)
                }))
            } else if "Lang".localized == "en" {
                sheet.addAction(UIAlertAction(title: nationality.nameEn, style: .default, handler: {_ in
                    self.viewModel.getNationality(nationality: nationality)
                }))
            }
        }
        sheet.addAction(UIAlertAction(title: "cancel nationality".localized, style: .cancel, handler: nil))
        
//        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)

    }
    
    // MARK:- Show Service Options Picker
    func setupServicesSheet() {
        
        let sheet = UIAlertController(title: "service alert title".localized, message: "service alert message".localized, preferredStyle: .actionSheet)
        for service in services {
            if "Lang".localized == "ar" {
                sheet.addAction(UIAlertAction(title: service.name, style: .default, handler: {_ in
                    self.viewModel.getService(service: service)
                }))
            } else if "Lang".localized == "en" {
                sheet.addAction(UIAlertAction(title: service.nameEN, style: .default, handler: {_ in
                    self.viewModel.getService(service: service)
                }))
            }
        }
        sheet.addAction(UIAlertAction(title: "cancel service".localized, style: .cancel, handler: nil))
        
//        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)

    }
    
    // MARK:- Show Cities Options Picker
    func setupCitiesSheet() {
        
        let sheet = UIAlertController(title: "city alert title".localized, message: "city alert message".localized, preferredStyle: .actionSheet)
        for city in cities {
            if "Lang".localized == "ar" {
                sheet.addAction(UIAlertAction(title: city.name, style: .default, handler: {_ in
                    self.viewModel.getCity(city: city)
                }))
            } else if "Lang".localized == "en" {
                sheet.addAction(UIAlertAction(title: city.nameEn, style: .default, handler: {_ in
                    self.viewModel.getCity(city: city)
                }))
            }
        }
        sheet.addAction(UIAlertAction(title: "cancel city".localized, style: .cancel, handler: nil))
        
//        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)

    }
    
    // MARK:- Configuration
    func configureView() {
        
        firstNameTextField.placeholder = "first name".localized
        lastNameTextField.placeholder = "last name".localized
        phoneTextField.placeholder = "phone".localized
        emailTextField.placeholder = "email".localized
        passwordTextField.placeholder = "password".localized
        confirmPasswordTextField.placeholder = "confirm".localized
        inspectionExpensesTextfield.placeholder = "inspection expenses".localized
//        idNumberTextfield.placeholder = "id number".localized
//        ageTextField.placeholder = "age".localized
        
        cityLabel.text = "city".localized
        serviceLabel.text = "service".localized
//        nationalityLabel.text = "nationality".localized
        agreeOnTermsLabel.text = "terms agree".localized
        signUpButton.setTitle("next stage".localized, for: .normal)
        havingAccountButton.setTitle("having account".localized, for: .normal)
        
        
        self.navigationItem.title = "sign up".localized
        logoView.roundCorners([.bottomLeft, .bottomRight], radius: 30.0)
        bottomView.roundCorners([.topLeft, .topRight], radius: 30.0)
        signUpButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        firstNameTextField.endEditing(true)
        lastNameTextField.endEditing(true)
        phoneTextField.endEditing(true)
        emailTextField.endEditing(true)
        passwordTextField.endEditing(true)
        confirmPasswordTextField.endEditing(true)
        inspectionExpensesTextfield.endEditing(true)
//        ageTextField.endEditing(true)
//        idNumberTextfield.endEditing(true)
    }
    
    func setIcon() {
        phoneTextField.setIcon(UIImage(named: "iphone-reverse")!, imageState: "Home")
        emailTextField.setIcon(UIImage(named: "mail_1")!, imageState: "Home")
        firstNameTextField.setIcon(UIImage(named: "userPhoto")!, imageState: "Home")
        lastNameTextField.setIcon(UIImage(named: "userPhoto")!, imageState: "Home")
        passwordTextField.setIcon(UIImage(named: "unlocked")!, imageState: "Home")
        confirmPasswordTextField.setIcon(UIImage(named: "unlocked")!, imageState: "Home")
//        inspectionExpensesTextfield.setIcon(UIImage(named: "unlocked")!, imageState: "Home")
        
//        ageTextField.setIcon(UIImage(named: "unlocked")!, imageState: "Home")
//        idNumberTextfield.setIcon(UIImage(named: "unlocked")!, imageState: "Home")
    }
    
    func saveLocation(longitude: Double, latitude: Double) {
        UserDefaults.standard.set(longitude, forKey: "longitude") //Bool
        UserDefaults.standard.set(latitude, forKey: "latitude") //Bool
    }
        
    //MARK:- Navigation
    func presentHomeScreen(isPushNotification: Bool = false) {
        //        if let _ = UserDefaultManager.shared.currentUser, let _ = UserDefaultManager.shared.authorization {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeNavigationVC")
        appDelegate.window!.rootViewController = viewController
        appDelegate.window!.makeKeyAndVisible()
        //        }
    }
    
    func goToTermsAndConditions() {
        if let termsAndConditions = UIStoryboard(name: "Authentication", bundle: nil).instantiateViewController(withIdentifier: "AgreeOnTermsViewController") as? AgreeOnTermsViewController {
            //                rootViewContoller.test = "test String"
            navigationController?.pushViewController(termsAndConditions, animated: true)
        }
    }
    
//    func goToSecondStageSignUpScreen(firstName: String, lastName: String, email: String, password: String, phone: String, areaID: Int, age: Int, serviceID: Int, inspectionExpenses: Int, idNumber: String, fcmToken: String, nationalityID: Int, latitude: String, longitude: String) {
    func goToSecondStageSignUpScreen(firstName: String, lastName: String, email: String, password: String, phone: String, areaID: Int, serviceID: Int, inspectionExpenses: Int, fcmToken: String, latitude: String, longitude: String) {
        let storyboard = UIStoryboard.init(name: "Authentication", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SecondStageSignInViewController") as! SecondStageSignInViewController
        
        viewController.firstName = firstName
        viewController.lastName = lastName
        viewController.email = email
        viewController.password = password
        viewController.phone = phone
        viewController.areaID = areaID
//        viewController.age = age
        viewController.inspectionExpenses = inspectionExpenses
//        viewController.idNumber = idNumber
        viewController.fcmToken = fcmToken
//        viewController.nationalityID = nationalityID
        viewController.latitude = latitude
        viewController.longitude = longitude
        viewController.serviceID = serviceID
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func signUp() {
        
        guard let firstName: String = firstNameTextField.text, firstName.count > 0 else {
              let apiError = APIError()
              apiError.message = "برجاء إدخال الإسم الأول"
              self.viewModel.error = apiError
              self.viewModel.state = .error
              return
          }
        
        if firstName.count < 16 {
        } else {
            let apiError = APIError()
            apiError.message = "الإسم الأول يزيد عن ١٥ حرف"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let lastName: String = lastNameTextField.text, lastName.count > 0 else {
              let apiError = APIError()
              apiError.message = "برجاء إدخال الإسم الثاني"
              self.viewModel.error = apiError
              self.viewModel.state = .error
              return
          }
        
        if lastName.count < 16 {
        } else {
            let apiError = APIError()
            apiError.message = "الإسم الثاني يزيد عن ١٥ حرف"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let phone: String = phoneTextField.text, phone.count == 10 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال رقم الجوال بشكل صحيح"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let email: String = emailTextField.text, email.count > 0 else {
              let apiError = APIError()
              apiError.message = "برجاء إدخال البريد الإلكتروني"
              self.viewModel.error = apiError
              self.viewModel.state = .error
              return
        }
        
//        guard let age: String = ageTextField.text, age.count > 0 else {
//              let apiError = APIError()
//              apiError.message = "برجاء إدخال عمرك"
//              self.viewModel.error = apiError
//              self.viewModel.state = .error
//              return
//        }
        
//        guard let workerAge: Int = Int(age), workerAge > 0 && workerAge < 100 else {
//              let apiError = APIError()
//              apiError.message = "برجاء إدخال عمرك بشكل صحيح"
//              self.viewModel.error = apiError
//              self.viewModel.state = .error
//              return
//        }
                
//        guard let idNumber: String = idNumberTextfield.text, idNumber.count > 0 else {
//              let apiError = APIError()
//              apiError.message = "برجاء إدخال رقم الهوية"
//              self.viewModel.error = apiError
//              self.viewModel.state = .error
//              return
//        }
        
        guard let inspectionExpenses: String = inspectionExpensesTextfield.text, inspectionExpenses.count > 0 else {
              let apiError = APIError()
              apiError.message = "برجاء إدخال مصاريف المعاينة"
              self.viewModel.error = apiError
              self.viewModel.state = .error
              return
        }
        
        guard let workerInspectionExpenses: Int = Int(inspectionExpenses), workerInspectionExpenses > 0 && workerInspectionExpenses < 100 else {
              let apiError = APIError()
              apiError.message = "برجاء إدخال مصاريف المعاينة بشكل صحيح"
              self.viewModel.error = apiError
              self.viewModel.state = .error
              return
        }
                
        guard let password: String = passwordTextField.text, password.count > 5 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال كلمة المرور أكثر من ٦ حروف"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let confirmPassword: String = confirmPasswordTextField.text, confirmPassword == password else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال تأكيد كلمة المرور بشكل صحيح"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        
        guard let cityID: Int = choosedCity?.id, cityID > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال المدينة"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let serviceID: Int = choosedService?.id, serviceID > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال الخدمة المقدمة"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

//        guard let nationalityID: Int = choosedNAtionality?.id, nationalityID > 0 else {
//            let apiError = APIError()
//            apiError.message = "برجاء إدخال الجنسية"
//            self.viewModel.error = apiError
//            self.viewModel.state = .error
//            return
//        }
                
        guard let checked: Bool = self.checked, checked == true else {
            let apiError = APIError()
            apiError.message = "برجاء مراجعة الشروط والأحكام"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let longitude: String = self.longitude, longitude.count > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال العنوان"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        guard let latitude: String = self.latitude, latitude.count > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال العنوان"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
                        
//        self.viewModel.signUp(firstName: firstName, lastName: lastName, email: email, password: password, phone: phone, areaID: cityID, age: workerAge, serviceID: serviceID, price: workerInspectionExpenses, idNumber: workerIDNumber, fcmToken: "FES", nationalityID: nationalityID, latitude: 12, longitude: 3)
//        self.goToSecondStageSignUpScreen(firstName: firstName, lastName: lastName, email: email, password: password, phone: phone, areaID: cityID, age: workerAge, serviceID: serviceID, inspectionExpenses: workerInspectionExpenses, idNumber: idNumberTextfield.text ?? "", fcmToken: FirebaseToken, nationalityID: choosedNAtionality?.id ?? 0, latitude: latitude, longitude: longitude)
        self.goToSecondStageSignUpScreen(firstName: firstName, lastName: lastName, email: email, password: password, phone: phone, areaID: cityID, serviceID: serviceID, inspectionExpenses: workerInspectionExpenses, fcmToken: FirebaseToken, latitude: latitude, longitude: longitude)
    }
    
    //MARK:- Navigation
    func goToVerifyPhone(id: Int) {
//        if let verifyPhoneVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VerifyPhoneNavigationVC") as? VerifyPhoneViewController {
//               verifyPhoneVC.id = id
//            self.present(verifyPhoneVC, animated: true, completion: nil)
//        }
        if let verifyPhoneNavVC = UIStoryboard(name: "Authentication", bundle: nil).instantiateViewController(withIdentifier: "VerifyPhoneNavigationVC") as? UINavigationController, let rootViewContoller = verifyPhoneNavVC.viewControllers[0] as? VerifyPhoneViewController {
                rootViewContoller.id = id
            self.present(verifyPhoneNavVC, animated: true, completion: nil)
        }
    }
    
    // MARK:- Actions
    @IBAction func termsAndConditionsButtonIsPressed(_ sender: Any) {
        print("Terms Button is pressed")
    }
    
    @IBAction func registrationIsPressed(_ sender: Any) {
        print("Registration is pressed")
//        presentHomeScreen()
        self.signUp()
    }
    
    @IBAction func checkIsPressed(_ sender: Any) {
        if checked == false {
            checkImage.image = UIImage(named: "check-box (1)")
            self.goToTermsAndConditions()
            checked = true
        } else if checked == true {
            checkImage.image = UIImage(named: "check-box")
            checked = false
        }
        
    }
    
    @IBAction func cityButtonIsPressed(_ sender: Any) {
        print("City Button Pressed")
        setupCitiesSheet()
    }
    
    @IBAction func serviceButtonIsPressed(_ sender: Any) {
        print("Service is pressed")
        setupServicesSheet()
    }
    
    @IBAction func nationalityButtonIsPressed(_ sender: Any) {
        print("Nationality is pressed")
        setupNationalitiesSheet()
    }
}

// MARK:- Location Manager Delegate
extension SignInViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        UserDefaults.standard.set(, forKey: "ID") //Bool
        if let location = locations.first {
            print(location.coordinate)
            saveLocation(longitude: location.coordinate.longitude, latitude: location.coordinate.latitude)
            self.longitude = String(location.coordinate.longitude)
            self.latitude = String(location.coordinate.latitude)
        }
    }
}
