//
//  VerifyPhoneViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/30/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import KWVerificationCodeView

class VerifyPhoneViewController: BaseViewController {
    
    lazy var viewModel: VerifyPhoneViewModel = {
        return VerifyPhoneViewModel()
    }()
    
    var id: Int?
    var verificationCode: String?
    var error: APIError?
    var status: Bool?

    @IBOutlet weak var logoView: UIView!
    @IBOutlet weak var verifyCodeView: KWVerificationCodeView!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var sendMessageButton: UIButton!
    @IBOutlet weak var messageIsSentLabel: UILabel!
    @IBOutlet weak var resendLabel: UILabel!
    @IBOutlet weak var secondLAbel: UILabel!
    @IBOutlet weak var bottomView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        configureView()
        verifyCodeView.delegate = self
        
        initVM()
        if let id: Int = self.id {
            viewModel.initFetch(id: id)
        }
    }
    
    // MARK:- Init View Model
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.showError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
        
        viewModel.updateStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
               
                self.status = self.viewModel.getStatus()
                if let status = self.status {
                    if status == false {
                        self.stopLoadingWithSuccess()
                        let apiError = APIError()
                        apiError.message = "فشل تأكيد الجوال"
                        self.viewModel.error = apiError
                        self.viewModel.state = .error
                    } else if status == true {
                        self.goToLogIn()
                    }
                }
            }
        }
        
    }
    
    func configureView() {
         
        navigationItem.title = "Verify phone"
        
        bottomView.roundCorners([.topLeft, .topRight], radius: 30.0)
        confirmButton.isEnabled = false
        self.navigationItem.title = "verify phone".localized
        logoView.roundCorners([.bottomLeft, .bottomRight], radius: 30.0)
        sendMessageButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        confirmButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        closeKeypad()
    }
    
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        verifyCodeView.endEditing(true)
    }


    @IBAction func resendMessageIsPressed(_ sender: Any) {
        guard let id: Int = self.id, id > 0 else {
            let apiError = APIError()
            apiError.message = "لا يوجد مستخدم"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        self.viewModel.initFetch(id: id)
    }
    
    @IBAction func confirmButtonIsPressed(_ sender: Any) {
        if verifyCodeView.hasValidCode() {
            
           guard let id: Int = self.id, id > 0 else {
               let apiError = APIError()
               apiError.message = "لا يوجد مستخدم"
               self.viewModel.error = apiError
               self.viewModel.state = .error
               return
           }
            
            guard let verificationCodee: String = self.verificationCode, verificationCodee.count > 0 else {
               let apiError = APIError()
               apiError.message = "لا يوجد مستخدم"
               self.viewModel.error = apiError
               self.viewModel.state = .error
               return
           }
            
//          let alertController = UIAlertController(title: "Success", message: "Code is \(verifyCodeView.getVerificationCode())", preferredStyle: .alert)
//          let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
//          alertController.addAction(okAction)
//          present(alertController, animated: true, completion: nil)
            
            self.viewModel.checkVerificationCode(id: id, verificationCode: Int(verificationCodee)!)
        }
    }

    // MARK: - Navigation
       func goToLogIn() {
           let storyboard = UIStoryboard.init(name: "Authentication", bundle: nil)
           let viewController = storyboard.instantiateViewController(withIdentifier: "LoginNavigationVC")
           UIApplication.shared.keyWindow?.rootViewController = viewController
       }
    
}

// MARK: - KWVerificationCodeViewDelegate
extension VerifyPhoneViewController: KWVerificationCodeViewDelegate {
  func didChangeVerificationCode() {
    confirmButton.isEnabled = verifyCodeView.hasValidCode()
    self.verificationCode = verifyCodeView.getVerificationCode()
  }
}

