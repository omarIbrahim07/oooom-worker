//
//  LoginViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 8/26/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {

    @IBOutlet weak var logoView: UIView!
    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var registrationButton: UIButton!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var forgetPasswordButton: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        configureView()
        // Do any additional setup after loading the view.
        closeKeypad()
    }
    
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        phoneTextField.endEditing(true)
        passwordTextField.endEditing(true)
    }
    
    func configureView() {
        
        logInButton.setTitle("log in".localized, for: .normal)
        forgetPasswordButton.setTitle("forget password".localized, for: .normal)
        phoneTextField.placeholder = "phone".localized
        passwordTextField.placeholder = "password".localized
        registrationButton.setTitle("sign up".localized, for: .normal)

        self.navigationItem.title = "log in".localized
        logInButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        registrationButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        logoView.roundCorners([.bottomLeft, .bottomRight], radius: 30.0)
        bottomView.roundCorners([.topLeft, .topRight], radius: 30.0)
    }
    
    func changeTextFieldPlaceHolderColor(textField: UITextField, placeHolderString: String,fontSize: CGFloat) {
        textField.attributedPlaceholder = NSAttributedString(string: placeHolderString, attributes: [.foregroundColor: #colorLiteral(red: 0.2901960784, green: 0.6745098039, blue: 0.9137254902, alpha: 1), .font: UIFont.boldSystemFont(ofSize: fontSize)])
    }
    
    func presentRegistration() {
        if let SignUpNavigationController = UIStoryboard(name: "Authentication", bundle: nil).instantiateViewController(withIdentifier: "SignInNavigationVC") as? UINavigationController, let rootViewContoller = SignUpNavigationController.viewControllers[0] as? SignInViewController {
            //                rootViewContoller.test = "test String"
            self.present(SignUpNavigationController, animated: true, completion: nil)
        }
    }
    
//    func logIn() {
//
//        guard let phone = phoneTextField.text, phone.isEmpty == false else {
//            let apiError = APIError()
//            apiError.message = "Please enter your phone"
//            showError(error: apiError)
//            return
//        }
//
//        guard let password = passwordTextField.text, password.isEmpty == false else {
//            let apiError = APIError()
//            apiError.message = "Please enter the password"
//            showError(error: apiError)
//            return
//        }
//
//        let params: [String : AnyObject] = [
//            "phone" : phone as AnyObject,
//            "password" : password as AnyObject,
//            "app_id" : 4 as AnyObject,
//            "fcm_token" : FirebaseToken as AnyObject
//        ]
//
//        startLoading()
//
//        AuthenticationAPIManager().loginUser(basicDictionary: params, onSuccess: { (token) in
//
//            self.stopLoadingWithSuccess()
//            print(UserDefaultManager.shared.currentUser)
//            self.presentHomeScreen()
//
//        }) { (error) in
//            self.stopLoadingWithError(error: error)
//        }
//
//
//    }
    
    func logIn() {
        
        guard let phone = phoneTextField.text, phone.isEmpty == false else {
            let apiError = APIError()
            apiError.message = "Please enter your phone"
            showError(error: apiError)
            return
        }
        
        guard let password = passwordTextField.text, password.isEmpty == false else {
            let apiError = APIError()
            apiError.message = "Please enter the password"
            showError(error: apiError)
            return
        }
        
        let params: [String : AnyObject] = [
            "phone" : phone as AnyObject,
            "password" : password as AnyObject,
            "app_id" : 5 as AnyObject,
            "fcm_token" : FirebaseToken as AnyObject
//            "fcm_token" : "FES" as AnyObject
        ]
        
        startLoading()
        
        AuthenticationAPIManager().loginUser(basicDictionary: params, onSuccess: { (userInfo) in
                        
            if userInfo.success == true {
                
                let userInfo = userInfo.userInfo
                
                if userInfo?.isVerified == 1 && userInfo?.approved == 1 && userInfo?.active == 1 && userInfo?.balanceLimit == 0 {
                    self.getUserProfile()
                }
                
                if userInfo?.isVerified == 0 {
                    if let userID: Int = userInfo?.id {
                        self.goToVerifyPhone(id: userID)
                    }
                }
                
                if userInfo?.approved == 0 {
                    self.stopLoadingWithSuccess()
                    let apiError = APIError()
                    apiError.message = "برجاء انتظار موافقة الإدارة"
                    self.showError(error: apiError)
                }
                
                if userInfo?.active == 0 {
                    self.stopLoadingWithSuccess()
                    let apiError = APIError()
                    apiError.message = "تم حظر الحساب برجاء مراجعة الإدارة"
                    self.showError(error: apiError)
                }
                
                if userInfo?.balanceLimit == 1 {
                    self.stopLoadingWithSuccess()
                    let apiError = APIError()
                    apiError.message = "برجاء مراجعة المكتب لسداد المديونية"
                    self.showError(error: apiError)
                }
            }

        }) { (error) in
            self.stopLoadingWithError(error: error)
        }
        
    }
    
    func getUserProfile() {
        weak var weakSelf = self
        
        AuthenticationAPIManager().getUserProfile(onSuccess: { (_) in
            
            weakSelf?.stopLoadingWithSuccess()
            weakSelf?.presentHomeScreen()
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
    }


    
    //MARK:- Navigation
    func presentHomeScreen() {
        if let _ = UserDefaultManager.shared.currentUser, let _ = UserDefaultManager.shared.authorization {
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "HomeNavigationVC")
            UIApplication.shared.keyWindow?.rootViewController = viewController
//            appDelegate.window!.rootViewController = viewController
        }
    }
    
    func goToVerifyPhone(id: Int) {
        if let verifyPhoneNavVC = UIStoryboard(name: "Authentication", bundle: nil).instantiateViewController(withIdentifier: "VerifyPhoneNavigationVC") as? UINavigationController, let rootViewContoller = verifyPhoneNavVC.viewControllers[0] as? VerifyPhoneViewController {
                rootViewContoller.id = id
            self.present(verifyPhoneNavVC, animated: true, completion: nil)
        }
    }

    
    func goToForgetPassword() {
        if let forgetPasswordVC = UIStoryboard(name: "Authentication", bundle: nil).instantiateViewController(withIdentifier: "ForgetPasswordViewController") as? ForgetPasswordViewController {
            //                rootViewContoller.test = "test String"
            navigationController?.pushViewController(forgetPasswordVC, animated: true)
//            (forgetPasswordVC, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func signUpButtonPressed(_ sender: Any) {
        print("Sign Up")
        presentRegistration()
    }
    
    @IBAction func forgetPasswordButtonPressed(_ sender: Any) {
        goToForgetPassword()
    }
    
    
    @IBAction func logInButtonPressed(_ sender: Any) {
        print("Home")
        logIn()
//        presentHomeScreen()
    }
    

}
