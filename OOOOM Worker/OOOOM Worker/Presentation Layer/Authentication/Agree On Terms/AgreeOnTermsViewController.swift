//
//  AgreeOnTermsViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/1/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class AgreeOnTermsViewController: BaseViewController {

    lazy var viewModel: AgreeOnTermsViewModel = {
        return AgreeOnTermsViewModel()
    }()
    
    @IBOutlet weak var logoView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
//                    self.stopLoadingWithError(error: <#T##APIError#>)
                    self.stopLoadingWithSuccess()
                case .empty:
                    print("")
                }
            }
        }
                                                
        viewModel.reloadStaticPage = { [weak self] () in
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
            }
        }
        
        self.viewModel.initSafteyProcedures(lang: "Lang".localized)
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "TermsAndConditionTableViewCell", bundle: nil), forCellReuseIdentifier: "TermsAndConditionTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
    }

    func configureView() {
        logoView.roundCorners([.bottomLeft, .bottomRight], radius: 30.0)
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AgreeOnTermsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: TermsAndConditionTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TermsAndConditionTableViewCell") as? TermsAndConditionTableViewCell {
            
            if let title: String = self.viewModel.termsAndConditions?.content {
                cell.setTermsAndConditions(title: title)
                return cell
            }
        }
        return UITableViewCell()
    }
}
