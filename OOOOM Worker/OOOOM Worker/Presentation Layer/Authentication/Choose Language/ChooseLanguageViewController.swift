//
//  ChooseLanguageViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/1/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import MOLH

class ChooseLanguageViewController: BaseViewController {
    
    @IBOutlet weak var logoView: UIView!
    @IBOutlet weak var englishButton: UIButton!
    @IBOutlet weak var arabicButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configure()
        checkLanguage()
    }
        

    
    func checkLanguage() {
        if UserDefaults.standard.bool(forKey: "First Launch") == true {
            self.goToLogIn()
        } else {
            print("Lesa")
        }
    }
    
    func configure() {
        logoView.roundCorners([.bottomLeft, .bottomRight], radius: 30.0)
        englishButton.addCornerRadius(raduis: 7.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 1.0)
        arabicButton.addCornerRadius(raduis: 7.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 1.0)
    }
    
    //MARK:- Actions
    @IBAction func englishButtonIsPressed(_ sender: Any) {
        print("English")
        UserDefaults.standard.set(true, forKey: "First Launch")
        MOLH.setLanguageTo("en")
        englishButton.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        englishButton.titleLabel?.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        print("Lang".localized)
        MOLH.reset()
        resetFirstTime()
    }
    
    func resetFirstTime() {
        let storyboard = UIStoryboard.init(name: "Authentication", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "LoginNavigationVC")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    @IBAction func arabicButtonIsPressed(_ sender: Any) {
        print("Arabic")
        UserDefaults.standard.set(true, forKey: "First Launch")
        MOLH.setLanguageTo("ar")
        arabicButton.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        arabicButton.titleLabel?.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        print("Lang".localized)
        MOLH.reset()
        resetFirstTime()
    }
    
    

    // MARK:- Navigation
    func goToLogIn() {
        print("Log in")
        let storyboard = UIStoryboard.init(name: "Authentication", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "LoginNavigationVC")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }

}
