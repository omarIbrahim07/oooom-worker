//
//  ForgetPasswordViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/4/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

class ForgetPasswordViewModel {
    
    var error: APIError?

    var updateLoadingStatus: (()->())?
    var updateStatus: (()->())?
    var updateVerificationCode: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    // callback for interfaces
    var verificationCode: Int? {
        didSet {
            self.updateVerificationCode?()
        }
    }
    
    // callback for interfaces
    var status: Bool? {
        didSet {
            self.updateStatus?()
        }
    }

    func forgetPassword(phone: String) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "phone" : phone as AnyObject,
        ]
        
        AuthenticationAPIManager().forgetPassword(basicDictionary: params, onSuccess: { (forgetPassObj) in
            
            if forgetPassObj.status == true {
                self.verificationCode = forgetPassObj.verifyCode
                self.state = .populated
            } else {
                self.makeError(message: "هذا الجوال غير موجود")
            }

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func confirmVerificationCode(phone: String, verificationCode: Int) {
        
        if verificationCode == self.verificationCode {
            self.status = true
        } else {
            self.makeError(message: "هذا الكود غير صحيح")
        }

    }
    
    func getVerificationCode() -> Int {
        return verificationCode!
    }
    
    func makeError(message: String) {
        let apiError = APIError()
        apiError.message = message
        self.error = apiError
        self.state = .error
    }
        
    func getError() -> APIError {
        return self.error!
    }
    
    func getStatus() -> Bool {
        return self.status!
    }

}
