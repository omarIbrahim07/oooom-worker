//
//  ForgetPasswordViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/1/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import KWVerificationCodeView

class ForgetPasswordViewController: BaseViewController {
    
    lazy var viewModel: ForgetPasswordViewModel = {
        return ForgetPasswordViewModel()
    }()
    
    var error: APIError?
    var status: Bool?
    var phone: String?
    var verificationCode: Int?

    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var verifyCodeView: KWVerificationCodeView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var sendMessageButton: UIButton!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        submitButton.isEnabled = false
        verifyCodeView.delegate = self

        configureView()
        initVM()
    }
    
    func closeKeyPad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        phoneTextField.endEditing(true)
        verifyCodeView.endEditing(true)
    }
    
    func configureView() {
        
        phoneTextField.placeholder = "phone".localized
        submitButton.setTitle("submit button".localized, for: .normal)
        sendMessageButton.setTitle("send message".localized, for: .normal)
        
        self.navigationItem.title = "forget pass".localized
        submitButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        sendMessageButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        closeKeyPad()
    }
    
    // MARK:- Init View Model
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.showError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
        
        viewModel.updateStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
               
                self.status = self.viewModel.getStatus()
                if let status = self.status {
                    if status == true {
                        if let phone = self.phone {
                            self.goToEditPassword(phone: phone)
                        }
                    }
                }
            }
        }
        
        viewModel.updateVerificationCode = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                
                self.verificationCode = self.viewModel.getVerificationCode()
            }
        }
    }

    // MARK:- Actions
    @IBAction func sendMessageIsPressed(_ sender: Any) {
        guard let phone: String = phoneTextField.text, phone.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال رقم الجوال بشكل صحيح"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        self.phone = phone
        viewModel.forgetPassword(phone: phone)
    }
    
    @IBAction func submitIsPressed(_ sender: Any) {
        if verifyCodeView.hasValidCode() {
            
//            guard let phone: String = self.phone, phone.count == 10 else {
//                let apiError = APIError()
//                apiError.message = "برجاء إدخال رقم الجوال بشكل صحيح"
//                self.viewModel.error = apiError
//                self.viewModel.state = .error
//                return
//            }
            guard let phone: String = phoneTextField.text, phone.count > 0 else {
                let apiError = APIError()
                apiError.message = "برجاء إدخال رقم الجوال بشكل صحيح"
                self.viewModel.error = apiError
                self.viewModel.state = .error
                return
            }
            
            guard let verificationCode: String = self.verifyCodeView.getVerificationCode(), verificationCode.count == 4 else {
                let apiError = APIError()
                apiError.message = "برجاء إدخال الكود بشكل صحيح"
                self.viewModel.error = apiError
                self.viewModel.state = .error
                return
            }


//          let alertController = UIAlertController(title: "Success", message: "Code is \(verifyCodeView.getVerificationCode())", preferredStyle: .alert)
//          let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
//          alertController.addAction(okAction)
//          present(alertController, animated: true, completion: nil)
            let verifyCode: Int = Int(verificationCode)!
            self.viewModel.confirmVerificationCode(phone: phone, verificationCode: verifyCode)
        }

    }
    



    // MARK: - Navigation
    func goToEditPassword(phone: String) {
        if let editPasswordVC = UIStoryboard(name: "Authentication", bundle: nil).instantiateViewController(withIdentifier: "EditPasswordViewController") as? EditPasswordViewController {
            editPasswordVC.phone = phone
            navigationController?.pushViewController(editPasswordVC, animated: true)
        }
    }
    

}

// MARK: - KWVerificationCodeViewDelegate
extension ForgetPasswordViewController: KWVerificationCodeViewDelegate {
  func didChangeVerificationCode() {
    submitButton.isEnabled = verifyCodeView.hasValidCode()
  }
}
