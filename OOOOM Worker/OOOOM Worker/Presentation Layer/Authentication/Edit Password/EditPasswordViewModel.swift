//
//  EditPasswordViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/8/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

class EditPasswordViewModel {
    
    var error: APIError?

    var updateLoadingStatus: (()->())?
    var updateStatus: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
        
    // callback for interfaces
    var status: Bool? {
        didSet {
            self.updateStatus?()
        }
    }

    
    func changePassword(phone: String, newPassword: String) {
    state = .loading
    
    let params: [String : AnyObject] = [
        "phone" : phone as AnyObject,
        "new_password" : newPassword as AnyObject,
    ]
    
    AuthenticationAPIManager().changePasswordFromLogIn(basicDictionary: params, onSuccess: { (status) in
        
        self.status = status
        if status {
            self.status = true
            self.state = .populated
        } else {
            self.makeError(message: "كلمة المرور غير مقبولة")
        }


        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func makeError(message: String) {
        let apiError = APIError()
        apiError.message = message
        self.error = apiError
        self.state = .error
    }

    func getError() -> APIError {
        return self.error!
    }

    func getStatus() -> Bool {
        return self.status!
    }
}
