//
//  EditPasswordViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/1/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class EditPasswordViewController: BaseViewController {
        
    lazy var viewModel: EditPasswordViewModel = {
        return EditPasswordViewModel()
    }()
    
    var error: APIError?
    var phone: String?
    var status: Bool?

    @IBOutlet weak var logoView: UIView!
    @IBOutlet weak var newPasswordTextfield: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var updatePasswordButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        initVM()
    }
    
    func configureView() {
        updatePasswordButton.setTitle("update password button".localized, for: .normal)
        newPasswordTextfield.placeholder = "new password".localized
        confirmPasswordTextField.placeholder = "confirm new password".localized
       
        self.navigationItem.title = "change password button".localized
        updatePasswordButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        logoView.roundCorners([.bottomLeft, .bottomRight], radius: 30.0)
        bottomView.roundCorners([.topLeft, .topRight], radius: 30.0)
    }
    
    // MARK:- Init View Model
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.showError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
        
        viewModel.updateStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
               
                self.status = self.viewModel.getStatus()
                if let status = self.status {
                    if status == true {
                        self.goToLogIn()
                    }
                }
            }
        }
    }
    
    // MARK:- Actions
    @IBAction func updatePasswordIsPressed(_ sender: Any) {
        guard let password: String = newPasswordTextfield.text, password.count > 0, password.count > 5 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال كلمة مرور لا تقل عن ٦ حروف"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let confirmPassword: String = confirmPasswordTextField.text, password == confirmPassword else {
            let apiError = APIError()
            apiError.message = "برجاء التأكد من كلمة المرور"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        if let phone: String = self.phone {
            viewModel.changePassword(phone: phone, newPassword: password)
        }
    }
    
    

    
    // MARK: - Navigation
    func goToLogIn() {
        let storyboard = UIStoryboard.init(name: "Authentication", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "LoginNavigationVC")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }

}
