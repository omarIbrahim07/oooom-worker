//
//  ChangePasswordViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/28/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

class ChangePasswordViewModel {
    
    public enum State {
        case loading
        case error
        case populated
        case empty
    }
    
    var updateLoadingStatus: (()->())?
    
    var error: APIError?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    func changePassword(oldPass: String, newPass: String,confirmPass: String) {
        
        let params: [String : AnyObject] = [
            "oldpassword" : oldPass as AnyObject,
            "newpassword" : confirmPass as AnyObject
        ]
        
        self.state = .loading
        
        AuthenticationAPIManager().changePassword(basicDictionary: params, onSuccess: { (message) in
            if message == "password changed successfully" {
            print("Success")
            self.state = .populated
            } else if message == "old password is incorrect" {
            self.state = .error
            }
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func getError() -> APIError {
        return self.error!
    }
    
}
