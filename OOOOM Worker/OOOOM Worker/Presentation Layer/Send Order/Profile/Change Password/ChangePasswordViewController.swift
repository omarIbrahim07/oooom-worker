//
//  ChangePasswordViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/22/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class ChangePasswordViewController: BaseViewController {
    
    lazy var viewModel: ChangePasswordViewModel = {
        return ChangePasswordViewModel()
    }()
    
    var error: APIError?
    
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var updatePasswordButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        navigationItem.title = "edit password title".localized
        configure()
        closeKeypad()
        
        initVM()
    }
    
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        oldPasswordTextField.endEditing(true)
        newPasswordTextField.endEditing(true)
        confirmPasswordTextField.endEditing(true)
    }

    
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.showError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
        
    }
    
    func configure() {
        oldPasswordTextField.placeholder = "last password".localized
        newPasswordTextField.placeholder = "new password".localized
        confirmPasswordTextField.placeholder = "confirm new password".localized
        updatePasswordButton.setTitle("update password button".localized, for: .normal)
    }
    
    func configureView() {
        bottomView.roundCorners([.topLeft, .topRight], radius: 30.0)
        updatePasswordButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    
    
    @IBAction func updatePasswordButtonPressed(_ sender: Any) {
        print("Update Password Pressed")
        
        guard let oldPass: String = oldPasswordTextField.text, oldPass.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال كلمة المرور القديمة"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let newPass: String = newPasswordTextField.text, newPass.count > 0 && newPass.count >= 6 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال كلمة المرور الجديدة"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let confirmPass: String = confirmPasswordTextField.text, confirmPass.count > 0, confirmPass == newPass else {
            let apiError = APIError()
            apiError.message = "برجاء التأكد من كلمة المرور الجديدة"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        self.viewModel.changePassword(oldPass: oldPass, newPass: newPass, confirmPass: confirmPass)
    }
    
}
