//
//  CellView.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/16/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class CellView: UITableViewHeaderFooterView {
        
    static let reuseIdentifier = "CellView"
    
    var didPressOnButton: ( () -> Void )?
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var headerImage: UIImageView!
    
    @IBOutlet weak var uploadImage: UIImageView!
    @IBOutlet weak var uploadButton: UIButton!
    
    
    @IBAction func uploadButtonIsPressed(_ sender: Any) {
        didPressOnButton?()
    }

}
