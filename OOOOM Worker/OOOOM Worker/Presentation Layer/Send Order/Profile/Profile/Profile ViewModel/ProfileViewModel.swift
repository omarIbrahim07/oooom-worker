//
//  TechnicianDetailsViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/14/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

class ProfileViewModel {
        
    private var workerWorks: [Work] = [Work]()
    private var error: APIError?
    
    private var check: Bool?
    
    var saved: Bool? {
        didSet {
            self.updateUploadingImageCheck?()
        }
    }
                
    var pwcellViewModels: [PreviousWorkCollectionCellViewModel] = [PreviousWorkCollectionCellViewModel]() {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
    
    var cellViewModels: TechnicianDetailsCellViewModel? {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
                
    var reloadTableViewClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var updateUploadingImageCheck: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
        
    var numberOfCells: Int {
        if self.check == true {
            return 1
        }
        return 0
     }

    func initFetch() {
        state = .loading
        
        let params: [String : AnyObject] = [:]
        
        OrderAPIManager().getWorkerWorks(basicDictionary: params, onSuccess: { (works) in
                        
            if let user = UserDefaultManager.shared.currentUser {
                self.check = true
                self.cellViewModels = self.processTechnicianDetails(user: user)
            }
            
            self.workerWorks = works
                                    
//            self.processFetchedPhoto(technicians: technicians)
            print(works)
            self.processPreviousWorks(works: self.workerWorks)
                        
            self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func uploadImage(previousWorkImage: Data?) {
        state = .loading
        
        let params: [String : AnyObject] = [:]
        
        AuthenticationAPIManager().uploadPreviousWork(profileImage: previousWorkImage, basicDictionary: params, onSuccess: { (saved) in
                        
            if saved == true {
                self.saved = saved
                self.state = .populated
            } else {
                self.state = .error
                self.makeError(message: "عفوا لفد تم الوصول للحد الاقصى لرفع الصور")
            }
                                                    
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func getError() -> APIError {
        return self.error!
    }

    func makeError(message: String) {
        let apiError = APIError()
        apiError.message = message
        self.error = apiError
        self.state = .error
    }

        
    func processPreviousWorks( works: [Work] ) {
        print("FEEEEEEES")
    //        if let workerOrders = self.technicianDetails?.workerOrders {
    //            self.workerOrders = workerOrders // Cache
    //        }
        var vms = [PreviousWorkCollectionCellViewModel]()
        for work in works {
            vms.append(createPreviousCellViewModel(work: work) )
        }
        self.pwcellViewModels = vms
    }
    
    func createPreviousCellViewModel( work: Work ) -> PreviousWorkCollectionCellViewModel {
        //Wrap a description
        return PreviousWorkCollectionCellViewModel(image: work.image!, id: work.id!)
    }
    
    var numbOfCells: Int {
        return pwcellViewModels.count
     }
        
    func processTechnicianDetails(user: User) -> TechnicianDetailsCellViewModel {
        return TechnicianDetailsCellViewModel(workJop: user.serviceName!, workJopEn: user.serviceNameEn!, name: user.firstName! + " " + user.lastName!, image: user.image!, age: user.age!, workFrom: user.from!, workFromEn: user.fromEN!, workAt: user.area!, workAtEn: user.areaEn!, completedOrders: user.ordersCompletedCount!, inspectionExpenses: user.price!, rate: Double(user.rating ?? 0), phone: user.phone!)
    }

    func getCellViewModel( at indexPath: IndexPath ) -> TechnicianDetailsCellViewModel {
        return cellViewModels!
    }
    
    func getImageUploadingCheck() -> Bool {
        return saved!
    }
                                
}

