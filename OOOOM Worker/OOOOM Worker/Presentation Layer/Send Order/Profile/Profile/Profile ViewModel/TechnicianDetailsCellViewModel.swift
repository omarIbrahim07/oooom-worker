//
//  TechnicianDetailsCellViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/14/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

struct TechnicianDetailsCellViewModel {
    let workJop: String
    let workJopEn: String
    let name: String
    let image: String
    let age: Int
    let workFrom: String
    let workFromEn: String
    let workAt: String
    let workAtEn: String
    let completedOrders: Int
    let inspectionExpenses: Int
    let rate: Double
    let phone: String
}


