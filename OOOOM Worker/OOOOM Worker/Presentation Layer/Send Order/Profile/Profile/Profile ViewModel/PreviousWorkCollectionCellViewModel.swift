//
//  PreviousWorkCollectionCellViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/14/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

struct PreviousWorkCollectionCellViewModel {
    let image: String
    let id: Int
}
