//
//  TechnicianDetailsViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/15/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController {
    
    lazy var viewModel: ProfileViewModel = {
        return ProfileViewModel()
    }()
            
    var error: APIError?
    var saved: Bool?
    
    var myFirstImage: UIImage? = UIImage()
    
    var firstImageData: Data?

    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setNavigationTitle()
        configureView()
        configureTableView()
//        tableView.tableHeaderView?.frame.size = CGSize(width: tableView.frame.width, height: CGFloat(44))
        
        initVM()
    }
    
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.showError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
        
        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        viewModel.updateUploadingImageCheck = { [weak self] () in
            DispatchQueue.main.async { [weak self] () in
                self?.saved = self!.viewModel.getImageUploadingCheck()
                if let saved: Bool = self?.saved {
//                    self!.goToVerifyPhone(id: id)
                    if saved == true {
                        self?.viewModel.initFetch()
                        print("إترفعت")
                    }
                }
            }
        }
        
        viewModel.initFetch()
            
        }
    
    func setNavigationTitle() {
        navigationItem.title = "my account".localized
    }
    
    func configureView() {
//        mainView.roundCorners([.bottomLeft, .bottomRight], radius: 30.0)
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "TechnicianDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "TechnicianDetailsTableViewCell")
        tableView.register(UINib(nibName: "PreviousWorkTableViewCell", bundle: nil), forCellReuseIdentifier: "PreviousWorkTableViewCell")
        tableView.register(UINib(nibName: "CellView", bundle: nil), forHeaderFooterViewReuseIdentifier: CellView.reuseIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
    }
    
    func imagePressed() {
        
        let image = UIImagePickerController()
        
        image.delegate = self
        
        image.sourceType = .photoLibrary
        
        self.present(image, animated: true) {
            
        }
    }
    
    func showDonePopUp() {
        let alertController = UIAlertController(title: "رفع الصورة", message: "جاري رفع الصورة !", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        let openAction = UIAlertAction(title: "موافق", style: .default) { (action) in
            if let imgData = self.myFirstImage {
                self.firstImageData = imgData.jpegData(compressionQuality: 0.3)
            }
            self.viewModel.uploadImage(previousWorkImage: self.firstImageData)
        }
        alertController.addAction(openAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showDeletedPopUp() {
        let alertController = UIAlertController(title: "مسح الصورة", message: "تم مسح الصورة !", preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let cancelAction = UIAlertAction(title: "إغلاق", style: .cancel) { (action) in
        }
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func uploadImage() {
        if let imgData = myFirstImage {
            firstImageData = imgData.jpegData(compressionQuality: 0.3)
        }
        imagePressed()

    }
    
    //MARK:- Navigation
    func goToSendOrder(selectedTechnician: Technician, technicianName: String) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SendOrderViewController") as! SendOrderViewController
        
        viewController.technicianDetails = selectedTechnician
        viewController.technicianName = technicianName
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToImageDetails(imageURL: String) {
        if let imageDetailsVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "ImageZoomedViewController") as? ImageZoomedViewController {
            //                rootViewContoller.test = "test String"
            imageDetailsVC.imageURL = imageURL
            self.present(imageDetailsVC, animated: true, completion: nil)
            print("bidDetailsVC")
        }
    }
    
    func goToSendEditProfile() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        
        navigationController?.pushViewController(viewController, animated: true)
    }

    
    
    func goToChat(technicianID: Int, technicianName: String) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController

        viewController.technicianID = technicianID
        viewController.technicianName = technicianName
//        viewController.technicianImage = technicianImage
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return viewModel.numberOfCells
        } else if section == 1 {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            if let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CellView") as? CellView {
                headerView.headerLabel.text = "info".localized
                headerView.headerImage.image = UIImage(named: "info-button")
                headerView.uploadImage.isHidden = true
                headerView.uploadButton.isHidden = true
                return headerView
            }
        }
        if section == 1 {
            if let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CellView") as? CellView {
                headerView.headerLabel.text = "previous works".localized
                headerView.headerImage.image = UIImage(named: "leader")
                headerView.uploadImage.isHidden = false
                headerView.uploadImage.image = UIImage(named: "upload image")
                headerView.uploadButton.isHidden = false
                
                headerView.didPressOnButton = {
                    print("Upload is pressed")
                    self.uploadImage()
                }
                
                return headerView
            }
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 || section == 1 {
            return 50  // or whatever
        }
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            if let cell: TechnicianDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TechnicianDetailsTableViewCell", for: indexPath) as? TechnicianDetailsTableViewCell {
                
                cell.delegate = self
                let cellVM = viewModel.getCellViewModel(at: indexPath)
                cell.photoListCellViewModel = cellVM
                cell.didPressOnButton = {
                    print("Edit Profile is pressed")
                    self.goToSendEditProfile()
                }
                
                return cell
            }
        } else if indexPath.section == 1 {
            if let cell: PreviousWorkTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviousWorkTableViewCell", for: indexPath) as? PreviousWorkTableViewCell {
                
                cell.viewModels = viewModel.pwcellViewModels
                cell.deletionDelegate = self
                cell.delegate = self
                
                return cell
            }
        }
        return UITableViewCell()
    }
}

// MARK:- Image Picker delegate
extension ProfileViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
//                firstImage.image = image
//                firstImage.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
            myFirstImage = image
            self.dismiss(animated: true, completion: nil)
            self.showDonePopUp()
        }
    }
}

extension ProfileViewController: ReloadAfterDeletePreviousWorkTableViewCellDelegate {
    func didRemovePreviousWork() {
        self.viewModel.initFetch()
        self.showDeletedPopUp()
    }
}

extension ProfileViewController: profileImageTableViewCellDelegate {
    func profileImageButtonPressed() {
        if let image = UserDefaultManager.shared.currentUser?.image {
            self.goToImageDetails(imageURL: ImageURL_USERS + image)
        }
    }
}

extension ProfileViewController: previousWorkImageTableViewCellDelegate {
    func previousWorkImageButtonPressed(image: String) {
            self.goToImageDetails(imageURL: image)
    }
}


