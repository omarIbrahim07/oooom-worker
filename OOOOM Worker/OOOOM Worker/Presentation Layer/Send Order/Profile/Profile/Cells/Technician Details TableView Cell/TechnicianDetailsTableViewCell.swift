//
//  TechnicianDetailsTableViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/15/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import Cosmos

protocol profileImageTableViewCellDelegate {
    func profileImageButtonPressed()
}

class TechnicianDetailsTableViewCell: UITableViewCell {
        
    var delegate: profileImageTableViewCellDelegate?
    
    //MARK:- Delegate Helpers
    func profileImageButtonPressed() {
        if let delegateValue = delegate {
            delegateValue.profileImageButtonPressed()
        }
    }


    @IBOutlet weak var workerNameLabel: UILabel!
    @IBOutlet weak var workerPhoneLabel: UILabel!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var workJopLabel: UILabel!
    @IBOutlet weak var workerImage: UIImageView!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var worksFromLabel: UILabel!
    @IBOutlet weak var worksAtLabel: UILabel!
    @IBOutlet weak var completedOrdersLabel: UILabel!
    @IBOutlet weak var inspectionExpensesLabel: UILabel!
    @IBOutlet weak var worksFromValueLabel: UILabel!
    @IBOutlet weak var worksAtValueLabel: UILabel!
    @IBOutlet weak var jobLabel: UILabel!
    @IBOutlet weak var ageNumberLabel: UILabel!
    @IBOutlet weak var numberOfCompletedOrdersLabel: UILabel!
    @IBOutlet weak var feesLabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var view: UIView!
        
    var photoListCellViewModel : TechnicianDetailsCellViewModel? {
        didSet {
            
            if let name = photoListCellViewModel?.name {
                workerNameLabel.text = name
            }
            
            if let phone = photoListCellViewModel?.phone {
                workerPhoneLabel.text = phone
            }
            
            if let age = photoListCellViewModel?.age {
                ageNumberLabel.text = String(age)
            }
            
            if let duration: String = photoListCellViewModel?.workFrom, let durationEn: String = photoListCellViewModel?.workFromEn {
                if "Lang".localized == "en" {
                    worksFromValueLabel.text = durationEn
                } else if "Lang".localized == "ar" {
                    worksFromValueLabel.text = duration
                }
            }
            
            if let workingAt: String = photoListCellViewModel?.workAt, let workingAtEN: String = photoListCellViewModel?.workAtEn {
                if "Lang".localized == "en" {
                    worksAtValueLabel.text = workingAtEN
                } else if "Lang".localized == "ar" {
                    worksAtValueLabel.text = workingAt
                }
            }
                        
            if let completedOrders = photoListCellViewModel?.completedOrders {
                numberOfCompletedOrdersLabel.text = String(completedOrders)
            }
            
            if let fees = photoListCellViewModel?.inspectionExpenses {
                feesLabel.text = String(fees)
            }
                        
            if let workerImageURL: String = ImageURL_USERS + (photoListCellViewModel?.image ?? "") {
                workerImage.loadImageFromUrl(imageUrl: workerImageURL)
            }
            
            if let rate = photoListCellViewModel?.rate {
                ratingView.rating = rate
            }
            
            if let workJob: String = photoListCellViewModel?.workJop, let workJobEn: String = photoListCellViewModel?.workJopEn {
                if "Lang".localized == "en" {
                    jobLabel.text = workJobEn
                } else if "Lang".localized == "ar" {
                    jobLabel.text = workJob
                }
            }
                                    
        }
    }
    
    var didPressOnButton: ( () -> Void )?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
        configure()
    }
    
    func configureView() {
        cellView.layer.cornerRadius = 8.0
        view.roundCorners([.topLeft, .topRight], radius: 25.0)
        workerImage.layer.cornerRadius = workerImage.frame.height / 2
    }
    
    func configure() {
        workJopLabel.text = "\("job".localized)"
        ageLabel.text = "\("age".localized)"
        worksFromLabel.text = "\("works from".localized)"
        worksAtLabel.text = "\("works at".localized)"
        completedOrdersLabel.text = "\("completed orders".localized)"
        inspectionExpensesLabel.text = "\("inspection expenses".localized)"
    }
    
    @IBAction func editProfileButtonIsPressed(_ sender: Any) {
        didPressOnButton?()
    }
    
    @IBAction func profileImageButtonIsPressed(_ sender: Any) {
        self.profileImageButtonPressed()
    }
    
    
    
}
