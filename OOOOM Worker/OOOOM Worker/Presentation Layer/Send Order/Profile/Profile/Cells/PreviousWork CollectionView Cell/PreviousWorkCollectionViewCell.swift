//
//  PreviousWorkCollectionViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/21/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

protocol PreviousWorkCollectionViewCellDeleteButtonDelegate {
    func didPreviousWorkDeleteButtonPressed(id: Int)
    func didPreviousWorkImagePressed(image: String)
}

class PreviousWorkCollectionViewCell: UICollectionViewCell {
    
    var delegate: PreviousWorkCollectionViewCellDeleteButtonDelegate?
    
    var previousWorkCollectionCellViewModel : PreviousWorkCollectionCellViewModel? {
        didSet {
            if let image = previousWorkCollectionCellViewModel?.image {
                previousWorkImage.loadImageFromUrl(imageUrl: ImageURLWorkerWorks + image)
            }
        }
    }
    
    @IBOutlet weak var previousWorkImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureCell()
        previousWorkImage.isUserInteractionEnabled = true
        previousWorkImage.addGestureRecognizer(UIGestureRecognizer(target: self, action: Selector(("animate"))))
    }
    
    func animate() {
        if let image = previousWorkCollectionCellViewModel?.image {
            self.didPreviousWorkImagePressed(image: ImageURLWorkerWorks + image)
//            print(ImageURLWorkerWorks + image)
        }
    }
        
    //MARK:- Delegate Helpers
    func didPreviousWorkDeleteButtonPressed(id: Int) {
        if let delegateValue = delegate {
            delegateValue.didPreviousWorkDeleteButtonPressed(id: id)
        }
    }
    
    func didPreviousWorkImagePressed(image: String) {
        if let delegateValue = delegate {
            delegateValue.didPreviousWorkImagePressed(image: image)
        }
    }
    
    func configureCell() {
        previousWorkImage.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 0.2901960784, green: 0.6745098039, blue: 0.9137254902, alpha: 1), borderWidth: 2)
    }
    
    @IBAction func removeWorkerWorksButtonIsPressed(_ sender: Any) {
        print("Removed is pressed")
        self.didPreviousWorkDeleteButtonPressed(id: self.previousWorkCollectionCellViewModel!.id)
    }
    
    @IBAction func previousWorkImageButtonIsPressed(_ sender: Any) {
        if let image = previousWorkCollectionCellViewModel?.image {
            self.didPreviousWorkImagePressed(image: ImageURLWorkerWorks + image)
        }
    }
    
    

}
