//
//  EditProfileViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/22/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import MOLH

class EditProfileViewController: BaseViewController {
    
    lazy var viewModel: EditProfileViewModel = {
       return EditProfileViewModel()
    }()
    
    var user: User?
    var cities: [City] = [City]()
    var choosedCity: City?
    var error: APIError?
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var inspectionExpensesTextfield: UITextField!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var updateProfileButton: UIButton!
    @IBOutlet weak var changePasswordButton: UIButton!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var changeLanguageLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        configureView()
        navigationItem.title = "edit my account".localized
        configure()
        closeKeypad()
        
        user = viewModel.initAccountData()
        
        if let user = user {
            bindAccountData(user: user)
        }

        initVM()
    }
    
    // MARK:- Init View Model
    func initVM() {
        viewModel.initFetch(locationID: 1)

        viewModel.updateEditingUser = { [weak self] () in
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.editingUser {
                case .success:
                    self.showDonePopUp()
                case .failed:
                    print("Failed")
                }
            }
        }

        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.showError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
        
        viewModel.reloadCitiesClosure = { [weak self] () in
            DispatchQueue.main.async { [weak self] () in
                self?.cities = self!.viewModel.getCities()
            }
        }
                    
        viewModel.chooseCityClosure = { [weak self] () in
            
            DispatchQueue.main.async { [weak self] in
                self?.choosedCity = self?.viewModel.choosedCity
                if let city = self?.choosedCity {
                    self?.bindCity(city: city)
                }
            }
        }
                    
    }

    
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        firstNameTextField.endEditing(true)
        lastNameTextField.endEditing(true)
        phoneTextField.endEditing(true)
        emailTextField.endEditing(true)
        inspectionExpensesTextfield.endEditing(true)
    }
    
    func showDonePopUp() {
        let alertController = UIAlertController(title: "لقد تم تغيير بيانات الحساب !", message: "الرجوع إلى الرئيسية", preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "تم", style: .default) { (action) in
            self.goToHomePage()
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showChooseLanguagePopUp() {
        let alertController = UIAlertController(title: "تغيير اللغة", message: "اختر اللغة التي ترغب للتغير لها", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        let arabicAction = UIAlertAction(title: "العربية", style: .default) { (action) in
            MOLH.setLanguageTo(MOLHLanguage.currentAppleLanguage() == "en" ? "ar" : "en")
//            MOLH.reset()
            self.goToHomePage()
        }
        let englishAction = UIAlertAction(title: "English", style: .default) { (action) in
            MOLH.setLanguageTo(MOLHLanguage.currentAppleLanguage() == "en" ? "ar" : "en")
//            MOLH.reset()
            self.goToHomePage()
        }
        alertController.addAction(arabicAction)
        alertController.addAction(englishAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
        
    func bindCity(city: City) {
        if "Lang".localized == "ar" {
            self.cityLabel.text = city.name
        } else if "Lang".localized == "en" {
            self.cityLabel.text = city.nameEn
        }
    }


    // MARK:- Show Time Options Picker
    func setupCitiesSheet() {
        
        let sheet = UIAlertController(title: "city alert title".localized, message: "city alert message".localized, preferredStyle: .actionSheet)
        for city in cities {
            if "Lang".localized == "ar" {
                sheet.addAction(UIAlertAction(title: city.name, style: .default, handler: {_ in
                    self.viewModel.userPressed(city: city)
                }))
            } else if "Lang".localized == "en" {
                sheet.addAction(UIAlertAction(title: city.nameEn, style: .default, handler: {_ in
                    self.viewModel.userPressed(city: city)
                }))
            }
        }
        sheet.addAction(UIAlertAction(title: "cancel city".localized, style: .cancel, handler: nil))
        
//        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)

    }


    
    // MARK:- Bind User Data
    func bindAccountData(user: User) {
        print(user)
        if let firstName: String = user.firstName {
            firstNameTextField.text = firstName
        }
        
        if let lastName: String = user.lastName {
            lastNameTextField.text = lastName
        }
        
        if let phone: String = user.phone {
            phoneTextField.text = phone
        }
        
        if let email: String = user.email {
            emailTextField.text = email
        }
        
        if let fees: Int = user.price {
            inspectionExpensesTextfield.text = String(fees)
        }
        
        if "Lang".localized == "en" {
            if let area: String = user.areaEn {
                self.choosedCity?.id = user.areaID
                cityLabel.text = area
            }
        } else if "Lang".localized == "ar" {
            if let area: String = user.area {
                self.choosedCity?.id = user.areaID
                cityLabel.text = area
            }
        }
        
        if let areaID: Int = user.areaID {
            print(areaID)
        }
    }
    
    // MARK:- Configure view
    func configureView() {
        bottomView.roundCorners([.topLeft, .topRight], radius: 30.0)
        updateProfileButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func configure() {
        firstNameTextField.placeholder = "first name".localized
        lastNameTextField.placeholder = "last name".localized
        phoneTextField.placeholder = "phone".localized
        emailTextField.placeholder = "email".localized
        cityLabel.text = "city".localized
        changeLanguageLabel.text = "change language".localized
        updateProfileButton.setTitle("update profile button".localized, for: .normal)
        changePasswordButton.setTitle("change password button".localized, for: .normal)
    }
    
    // MARK:- Networking
    func editProfile() {
        
        guard let firstName: String = firstNameTextField.text, firstName.count > 0 else {
              let apiError = APIError()
              apiError.message = "برجاء إدخال الإسم الأول"
              self.viewModel.error = apiError
              self.viewModel.state = .error
              return
          }
        
        if firstName.count < 16 {
        } else {
            let apiError = APIError()
            apiError.message = "الإسم الأول يزيد عن ١٥ حرف"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let lastName: String = lastNameTextField.text, lastName.count > 0 else {
              let apiError = APIError()
              apiError.message = "برجاء إدخال الإسم الثاني"
              self.viewModel.error = apiError
              self.viewModel.state = .error
              return
          }
        
        if lastName.count < 16 {
        } else {
            let apiError = APIError()
            apiError.message = "الإسم الثاني يزيد عن ١٥ حرف"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let email: String = emailTextField.text, email.count > 0 else {
              let apiError = APIError()
              apiError.message = "برجاء إدخال البريد الإلكتروني"
              self.viewModel.error = apiError
              self.viewModel.state = .error
              return
          }
                
        guard let phone: String = phoneTextField.text, phone.count == 10 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال رقم الجوال بشكل صحيح"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        print(self.choosedCity)
        
        guard let cityID: Int = choosedCity?.id, cityID > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال المدينة"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let fees: String = inspectionExpensesTextfield.text, fees.count > 0 && fees.count <= 4 else {
              let apiError = APIError()
              apiError.message = "برجاء إدخال مصاريف المعاينة بشكل صحيح"
              self.viewModel.error = apiError
              self.viewModel.state = .error
              return
        }
        
        let feess: Int = Int(fees)!
        
        self.viewModel.editProfile(firstName: firstName, lastName: lastName, email: email, phone: phone, areaID: cityID, inspectionExpenses: feess)
    }

    
    // MARK:- Navigation
    func goToChangePassword() {
        if let changePasswordVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChangePasswordViewController") as? ChangePasswordViewController {
            //                rootViewContoller.test = "test String"
            navigationController?.pushViewController(changePasswordVC, animated: true)
            print("Change Password Viewcontroller")
        }
    }
    
    func goToHomePage() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeNavigationVC")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    // MARK:- Actions
    @IBAction func cityButtonIsPressed(_ sender: Any) {
        print("City Button Pressed")
        setupCitiesSheet()
    }

    @IBAction func cityLanguageButtonIsPressed(_ sender: Any) {
        print("Language Button Pressed")
        showChooseLanguagePopUp()
    }
    @IBAction func updateProfileButtonPressed(_ sender: Any) {
        print("Update Profile Pressed")
        self.editProfile()
    }
    
    @IBAction func changePasswordButtonPressed(_ sender: Any) {
        print("Change Password Pressed")
        goToChangePassword()
    }
}
