//
//  EditProfileViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/27/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

class EditProfileViewModel {
    
    public enum editingUser {
        case success
        case failed
    }
    
    var error: APIError?
    
    var reloadUserData: (()->())?
    var reloadCitiesClosure: (()->())?
    var chooseCityClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var updateEditingUser: (()->())?

    // callback for interfaces
    var cities: [City]? {
        didSet {
            self.reloadCitiesClosure?()
        }
    }

    // callback for interfaces
    var choosedCity: City? {
        didSet {
            self.chooseCityClosure?()
        }
    }
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    // callback for interfaces
    var editingUser: editingUser = .failed {
        didSet {
            self.updateEditingUser?()
        }
    }


    func initFetch(locationID: Int) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "location_id" : locationID as AnyObject,
        ]
        
        CitiesAPIManager().getCities(basicDictionary: params, onSuccess: { (cities) in

        self.cities = cities // Cache
        self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func editProfile(firstName: String, lastName: String, email: String, phone: String, areaID: Int, inspectionExpenses: Int) {
        
        let params: [String : AnyObject] = [
            "first_name" : firstName as AnyObject,
            "last_name" : lastName as AnyObject,
            "email" : email as AnyObject,
            "phone" : phone as AnyObject,
            "area_id" : areaID as AnyObject,
            "price" : inspectionExpenses as AnyObject
        ]
        
        state = .loading
        
        AuthenticationAPIManager().editProfile(basicDictionary: params, onSuccess: { (user) in
            
//            UserDefaultManager.shared.currentUser = user
            self.state = .populated
            self.editingUser = .success

        }) { (error) in
            self.error = error
            self.state = .error
            self.editingUser = .failed
        }
    }

    
    func makeError(message: String) {
        let apiError = APIError()
        apiError.message = message
        self.error = apiError
        self.state = .error
    }
    
    func getError() -> APIError {
        return self.error!
    }
    
    
    func initAccountData() -> User {
        if let user = UserDefaultManager.shared.currentUser {
            print(user)
            return user
        }
        return User()
    }
    
    func getCities() -> [City] {
        return cities!
    }
    
    func userPressed(city: City) {
        self.choosedCity = city
    }

    
}
