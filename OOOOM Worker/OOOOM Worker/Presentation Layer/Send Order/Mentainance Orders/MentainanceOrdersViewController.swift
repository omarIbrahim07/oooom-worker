//
//  MentainanceOrdersViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/21/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class MentainanceOrdersViewController: BaseViewController {
    
    lazy var viewModel: MentainanceOrdersViewModel = {
        return MentainanceOrdersViewModel()
    }()
    
    var error: APIError?
    var statusID: Int?
    
    @IBOutlet weak var noMentainanceOrdersLabel: UILabel!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
        self.noMentainanceOrdersLabel.isHidden = true
        
        initVM()
        
        if self.statusID != 0 { // 0 for bids
            if let statusID: Int = self.statusID {
                viewModel.initFetch(orderStatus: statusID)
            }
        } else if self.statusID == 0 {
            // Get Bids
            viewModel.getBids()
        }
    }
    
    func initVM() {
        
         viewModel.setNoTechnician = { [weak self] () in
            guard let self = self else {
                return
            }
            
            switch self.viewModel.techsExistance {
            
            case .notech:
                self.noMentainanceOrdersLabel.isHidden = false
            case .techs:
                self.noMentainanceOrdersLabel.isHidden = true
            }
         }

        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.showError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
            
//                if let service = choosedService {
//                    viewModel.initFetch(service: service, sorting: 1)
//                }
            

        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "MentainanceOrderTableViewCell", bundle: nil), forCellReuseIdentifier: "MentainanceOrderTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func configureView() {
        if self.statusID == 0 {
            navigationItem.title = "bids title".localized
            noMentainanceOrdersLabel.text = "no bids".localized
        } else if self.statusID == 1 {
            navigationItem.title = "new orders".localized
            noMentainanceOrdersLabel.text = "no new orders".localized
        } else if self.statusID == 2 {
            navigationItem.title = "current orders".localized
            noMentainanceOrdersLabel.text = "no current orders".localized
        } else if self.statusID == 3 {
            navigationItem.title = "archeive".localized
            noMentainanceOrdersLabel.text = "no archeive".localized
        }
    }
        
    // MARK:- Navigation
    func goToOrderDetails(orderId: Int) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "OrderDetailsViewController") as! OrderDetailsViewController
        viewController.orderId = orderId
        navigationController?.pushViewController(viewController, animated: true)
    }

//    func goToOrderDetails(orderId: Int) {
//        if let editProfileVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OrderDetailsViewController") as? OrderDetailsViewController {
//            editProfileVC.orderId = orderId
//            //                rootViewContoller.test = "test String"
//            self.present(editProfileVC, animated: true, completion: nil)
//            print("My Account")
//        }
//    }
    
    func goToBidDetails(bidID: Int) {
        if let bidDetailsVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "BidDetailsViewController") as? BidDetailsViewController {
            bidDetailsVC.bidID = bidID
            self.present(bidDetailsVC, animated: true, completion: nil)
            print("bidDetailsVC")
        }
    }

}

extension MentainanceOrdersViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.statusID == 1 || self.statusID == 2 || self.statusID == 3 {
            return viewModel.numberOfCells
        } else if self.statusID == 0 {
            return viewModel.numberOfBids
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: MentainanceOrderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MentainanceOrderTableViewCell") as? MentainanceOrderTableViewCell {
            
            cell.didPressOnButton = { f in
                print(f)
                if self.statusID == 1 || self.statusID == 2 || self.statusID == 3 {
                    self.goToOrderDetails(orderId: f)
                } else if self.statusID == 0 {
                    // Go To Bids Details
                    self.goToBidDetails(bidID: f)
                }
            }
            
            if self.statusID == 1 || self.statusID == 2 || self.statusID == 3 {
                let cellVM = viewModel.getCellViewModel(at: indexPath)
                cell.orderCellViewModel = cellVM
                return cell
            } else if self.statusID == 0 {
                let cellVM = viewModel.getBidCellViewModel(at: indexPath)
                cell.bidCellViewModel = cellVM
                return cell
            }
            
        }
        return UITableViewCell()
    }
    
}
