//
//  MentainanceOrderCellViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/18/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
struct OrderCellViewModel {
    let service: String
    let serviceEn: String
    let createdAt: String
    let id: Int
    let name: String
}
