//
//  MentainanceOrdersViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/18/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

class MentainanceOrdersViewModel {

    private var newOrders: [Order] = [Order]()
    private var bids: [Bid] = [Bid]()
    
    var selectedOrder: Order?
    var selectedBid: Bid?

    var error: APIError?
    
    enum noTechs {
        case notech
        case techs
    }
    
    var ordersCellViewModels: [OrderCellViewModel] = [OrderCellViewModel]() {
           didSet {
               self.reloadTableViewClosure?()
           }
       }
    
    var bidCellViewModels: [BidCellViewModel] = [BidCellViewModel]() {
       didSet {
           self.reloadTableViewClosure?()
       }
    }
    
    var reloadTableViewClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var setNoTechnician: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
        
    var techsExistance: noTechs = .notech {
        didSet {
            self.setNoTechnician?()
        }
    }
    
    var numberOfCells: Int {
         return ordersCellViewModels.count
     }
    
    var numberOfBids: Int {
         return bidCellViewModels.count
     }

    func initFetch(orderStatus: Int) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "status_id" : orderStatus as AnyObject
        ]
        
        OrderAPIManager().getNewOrders(basicDictionary: params, onSuccess: { (newOrders) in
            
            self.processFetchedPhoto(newOrders: newOrders)
            self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func getBids() {
        state = .loading

        let params: [String : AnyObject] = [:]

        OrderAPIManager().getBids(basicDictionary: params, onSuccess: { (bids) in

            self.processBids(bids: bids)
            self.state = .populated

        }) { (error) in
            print(error)
        }
    }

    func processBids( bids: [Bid] ) {
        self.bids = bids // Cache

        if self.bids.count == 0 {
            self.techsExistance = .notech
        } else {
            self.techsExistance = .techs
        }

        var vms = [BidCellViewModel]()
        for bid in bids {
            vms.append( createBidCellViewModel(bid: bid) )
        }
        self.bidCellViewModels = vms
    }

    func createBidCellViewModel( bid: Bid ) -> BidCellViewModel {

        return BidCellViewModel(service: bid.service!, name: bid.name!, id: bid.id!, createdAt: bid.createdAt!)
    }


    func getBidCellViewModel( at indexPath: IndexPath ) -> BidCellViewModel {
        return bidCellViewModels[indexPath.row]
    }

    func bidUserPressed( at indexPath: IndexPath ) {
        let bid = self.bids[indexPath.row]
        self.selectedBid = bid
    }

    
    func processFetchedPhoto(newOrders: [Order] ) {
        self.newOrders = newOrders // Cache
        
        if self.newOrders.count == 0 {
            self.techsExistance = .notech
        } else {
            self.techsExistance = .techs
        }
        
        var vms = [OrderCellViewModel]()
        for newOrder in newOrders {
            vms.append( createCellViewModel(newOrder: newOrder) )
        }
        self.ordersCellViewModels = vms
    }
    
    func createCellViewModel( newOrder: Order ) -> OrderCellViewModel {
                    
        return OrderCellViewModel(service: newOrder.service!, serviceEn: newOrder.serviceEn!, createdAt: newOrder.createdAt!, id: newOrder.id!, name: newOrder.clientName!)
    }


    func getCellViewModel( at indexPath: IndexPath ) -> OrderCellViewModel {
        return ordersCellViewModels[indexPath.row]
    }
    
    func getError() -> APIError {
        return self.error!
    }

    func userPressed( at indexPath: IndexPath ) {
        let order = self.newOrders[indexPath.row]

        self.selectedOrder = order
    }

}
