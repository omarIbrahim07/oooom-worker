//
//  BidCellViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/18/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
struct BidCellViewModel {
    let service: BidService
    let name: String
    let id: Int
    let createdAt: String
}
