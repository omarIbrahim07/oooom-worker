//
//  TechniciansViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/15/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit


class TechniciansViewController: BaseViewController {
    
    var jobName: String?
    var availableTechnicains: Int? = 20
    
    var choosedService: Service?
    var longitude: String?
    var latitude: String?
    
    lazy var viewModel: TechniciansViewModel = {
        return TechniciansViewModel()
    }()

    @IBOutlet weak var logoView: UIView!
    @IBOutlet weak var sortView: UIView!
    @IBOutlet weak var sortByLabel: UILabel!
    @IBOutlet weak var sortChoiceLabel: UILabel!
    @IBOutlet weak var requsetBidLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var requestBidView: UIView!
    @IBOutlet weak var noTechnicianLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavigationTitle()
        configureView()
        configureTableView()
        getLocation()
                
        initVM()
    }
    
    func initVM() {
        guard let longitude: String = self.longitude, longitude.count > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال العنوان"
//            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        guard let latitude: String = self.latitude, latitude.count > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال العنوان"
//            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        
        viewModel.setNoTechnician = { [weak self] () in
            guard let self = self else {
                return
            }
            
            switch self.viewModel.techsExistance {
            
            case .notech:
                self.noTechnicianLabel.isHidden = false
            case .techs:
                self.noTechnicianLabel.isHidden = true
            }
        }

            viewModel.updateLoadingStatus = { [weak self] () in
                guard let self = self else {
                    return
                }

                DispatchQueue.main.async { [weak self] in
                    guard let self = self else {
                        return
                    }
                    switch self.viewModel.state {
                    
                    case .loading:
                        self.startLoading()
                    case .populated:
                        self.stopLoadingWithSuccess()
                    case .error:
    //                    self.stopLoadingWithError(error: <#T##APIError#>)
                        self.stopLoadingWithSuccess()
                    case .empty:
                        print("")
                    }
                }
            }
        
            if let service = choosedService {
                viewModel.initFetch(service: service, sorting: 1, longitude: longitude, latitude: latitude)
            }
        
            viewModel.updateSorting = { [weak self] () in

                DispatchQueue.main.async { [weak self] in

                switch self!.viewModel.sorting {
                                    
                case .rating:
                    self!.viewModel.initFetch(service: self!.choosedService!, sorting: 1, longitude: longitude, latitude: latitude)
                    self!.sortChoiceLabel.text = "sorting by rating".localized
                case .works:
                    self!.viewModel.initFetch(service: self!.choosedService!, sorting: 2, longitude: longitude, latitude: latitude)
                    self!.sortChoiceLabel.text = "sorting by works".localized
                case .distance:
                    self!.viewModel.initFetch(service: self!.choosedService!, sorting: 3, longitude: longitude, latitude: latitude)
                    self!.sortChoiceLabel.text = "sorting by distance".localized
                }
            }
        }

            viewModel.reloadTableViewClosure = { [weak self] () in
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                }
            }
            
        }
    
    func getLocation() {
        let long: Double = UserDefaults.standard.double(forKey: "longitude")
        let lat: Double = UserDefaults.standard.double(forKey: "latitude")

        self.longitude = String(long)
        self.latitude = String(lat)
    }

    
    // MARK:- Show rating Options
    func setupTimeSheet() {
        
        let sheet = UIAlertController(title: "sorting alert title".localized, message: "sorting alert message".localized, preferredStyle: .actionSheet)
        sheet.addAction(UIAlertAction(title: "sorting by rating".localized, style: .default, handler: {_ in
            self.viewModel.sorting = .rating
        }))
        sheet.addAction(UIAlertAction(title: "sorting by works".localized, style: .default, handler: {_ in
            self.viewModel.sorting = .works
        }))
        sheet.addAction(UIAlertAction(title: "sorting by distance".localized, style: .default, handler: {_ in
            self.viewModel.sorting = .distance
        }))
        sheet.addAction(UIAlertAction(title: "الغاء", style: .cancel, handler: nil))
        
//        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)

    }
    
    func setNavigationTitle() {
        
        if "Lang".localized == "en" {
            jobName = choosedService?.nameEN
        } else if "Lang".localized == "ar" {
            jobName = choosedService?.name
        }
                    
        navigationItem.title = jobName
        sortByLabel.text = "sort by".localized
        requsetBidLabel.text = "request bid".localized
        sortChoiceLabel.text = "sorting by rating".localized
        noTechnicianLabel.text = "no technician".localized
    }
    
    func configureView() {
        logoView.roundCorners([.bottomLeft, .bottomRight], radius: 30.0)
        sortView.roundCorners([.topRight, .bottomLeft], radius: 15.0)
        requestBidView.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "TechnicianTableViewCell", bundle: nil), forCellReuseIdentifier: "TechnicianTableViewCell")
        tableView.register(UINib(nibName: "CellView", bundle: nil), forHeaderFooterViewReuseIdentifier: CellView.reuseIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    // MARK:- Navigation
    func goToTechnicianDetails(service: Service, technician: Technician) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        
//        viewController.selectedService = service
//        viewController.selectedTechnician = technician
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToRequestBid() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "RequestBidViewController") as! RequestBidViewController
        viewController.serviceID = self.choosedService?.id
        navigationController?.pushViewController(viewController, animated: true)
    }


    @IBAction func requestBidButtonPressed(_ sender: Any) {
        print("Request bid")
        goToRequestBid()
    }
    
    @IBAction func sortButtonPressed(_ sender: Any) {
        print("Sort pressed")
        setupTimeSheet()
    }
    
    
}

extension TechniciansViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return availableTechnicains!
        return viewModel.numberOfCells
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1

    }
    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        let head = "Available: \(availableTechnicains ?? 0)"
//        return head
//    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            if let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CellView") as? CellView {
                let head = "\("available header".localized): \(viewModel.numberOfCells ?? 0)"
                headerView.headerLabel.text = head
                return headerView
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40  // or whatever
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: TechnicianTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TechnicianTableViewCell", for: indexPath) as? TechnicianTableViewCell {
            
            let cellVM = viewModel.getCellViewModel( at: indexPath )
            cell.photoListCellViewModel = cellVM
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.viewModel.userPressed(at: indexPath)
        
        if let selectedTechnician = self.viewModel.selectedTechnician, let service = self.choosedService {
            
            self.goToTechnicianDetails(service: service, technician: selectedTechnician)
        }
                
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
