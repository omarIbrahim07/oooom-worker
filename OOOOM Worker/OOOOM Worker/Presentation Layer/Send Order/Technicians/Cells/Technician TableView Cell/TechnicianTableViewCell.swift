//
//  TechnicianTableViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/15/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import Cosmos

class TechnicianTableViewCell: UITableViewCell {

    @IBOutlet weak var technicianView: UIView!
    @IBOutlet weak var technicianNameLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var technicianRatingView: CosmosView!
    
    var photoListCellViewModel : TechnicianCellViewModel? {
        didSet {
            if let name = photoListCellViewModel?.name {
                technicianNameLabel.text = name
            }
            
            if let rating = photoListCellViewModel?.rate {
                technicianRatingView.rating = rating
            }
            
            if let distance = photoListCellViewModel?.distance {
                if distance != 0 {
                    distanceLabel.isHidden = false
                    let distance: Int = Int(distance)
                    distanceLabel.text = String(distance) + " Km"
                } else {
                    distanceLabel.isHidden = true
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }

    func configureView() {
        technicianView.layer.cornerRadius = 8.0
    }
    
    
}
