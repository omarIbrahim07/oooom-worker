//
//  TechnicianCellViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/12/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

struct TechnicianCellViewModel {
    let name: String
    let rate: Double
    let distance: Float
}
