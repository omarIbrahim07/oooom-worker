//
//  TechniciansViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/12/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

class TechniciansViewModel {

    private var technicians: [Technician] = [Technician]()
    
    var selectedTechnician: Technician?
    
    enum Sorting {
        case rating
        case works
        case distance
    }
    
    enum noTechs {
        case notech
        case techs
    }
    
    var cellViewModels: [TechnicianCellViewModel] = [TechnicianCellViewModel]() {
           didSet {
               self.reloadTableViewClosure?()
           }
       }
    
    var reloadTableViewClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var updateSorting: (()->())?
    var setNoTechnician: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    // callback for interfaces
    var sorting: Sorting = .rating {
        didSet {
            self.updateSorting?()
        }
    }
    
    var techsExistance: noTechs = .notech {
        didSet {
            self.setNoTechnician?()
        }
    }
    
    var numberOfCells: Int {
         return cellViewModels.count
     }

    func initFetch(service: Service, sorting: Int, longitude: String, latitude: String) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "sorting" : sorting as AnyObject,
            "service_id" : service.id as AnyObject,
            "longitude" : longitude as AnyObject,
            "latitude" : latitude as AnyObject
        ]
        
        TechnicianAPIManager().getTechnicians(basicDictionary: params, onSuccess: { (technicians) in
            
            self.processFetchedPhoto(technicians: technicians)
            self.state = .populated

        }) { (error) in
            print(error)
        }

    }
    
    func processFetchedPhoto( technicians: [Technician] ) {
        self.technicians = technicians // Cache
        
        if self.technicians.count == 0 {
            self.techsExistance = .notech
        } else {
            self.techsExistance = .techs
        }
        
        var vms = [TechnicianCellViewModel]()
        for technician in technicians {
            vms.append( createCellViewModel(technician: technician) )
        }
        self.cellViewModels = vms
    }
    
    func createCellViewModel( technician: Technician ) -> TechnicianCellViewModel {

        //Wrap a description
//        var descTextContainer: [String] = [String]()
//        if let camera = service.name {
//            descTextContainer.append(camera)
//        }
//
//        if let description = service.image {
//            descTextContainer.append( description )
//        }
                    
        return TechnicianCellViewModel(name: technician.name!, rate: Double(technician.rating ?? 0.0), distance: technician.distance ?? 0.0)
    }


    func getCellViewModel( at indexPath: IndexPath ) -> TechnicianCellViewModel {
        return cellViewModels[indexPath.row]
    }

    func userPressed( at indexPath: IndexPath ) {
        let technician = self.technicians[indexPath.row]

        self.selectedTechnician = technician
      }

}
