//
//  SendOrderViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/28/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import Cosmos
import DatePickerDialog
import TimePicker
import DateTimePicker
import Toast_Swift
import CoreLocation

class SendOrderViewController: BaseViewController {
    
    lazy var viewModel: SendOrderViewModel = {
        return SendOrderViewModel()
    }()
    
    let locationManager = CLLocationManager()
    
    var technicianName: String?
    var technicianDetails: Technician?
    var stack: SendOrderStackViewModel?
    var stackData: DataStackViewModel?
    var orderDetails: OrderSent?
    var error: APIError?
    
    var longitude: String?
    var latitude: String?
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var stateView: UIView!
    @IBOutlet weak var effectView: UIVisualEffectView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var chooseAppointmentLabel: UILabel!
    @IBOutlet weak var choosedAppointmentLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var workerNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var reservationDateLabel: UILabel!
    @IBOutlet weak var amountAgreedLabel: UILabel!
    @IBOutlet weak var amountAgreedValueLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var reservationTimeLabel: UILabel!
    @IBOutlet weak var dateStackView: UIStackView!
    @IBOutlet weak var timeStackView: UIStackView!
    @IBOutlet weak var amountAgreedStackView: UIStackView!
    @IBOutlet weak var nameStackView: UIStackView!
    
    func datePickerTapped() {
        DatePickerDialog().show("DatePicker", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                        let formatter = DateFormatter()
                        formatter.dateFormat = "MM/dd/yyyy"
                self.reservationDateLabel.text = formatter.string(from: dt)
                self.reservationDateLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }
        }
    }
    
    func showLocationDisabledPopUp() {
        let alertController = UIAlertController(title: "Background Location Access Disabled", message: "In order to serve u we need your location", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let openAction = UIAlertAction(title: "Open Settigns", style: .default) { (action) in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(openAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavigationTitle()
        configure()
        configureView()
        getLocation()
        
        if CLLocationManager.authorizationStatus() == .denied {
            self.showLocationDisabledPopUp()
        }
        
        initVM()
        
    }
    
    func getLocation() {
        let long: Double = UserDefaults.standard.double(forKey: "longitude")
        let lat: Double = UserDefaults.standard.double(forKey: "latitude")

        self.longitude = String(long)
        self.latitude = String(lat)
    }
    
    // MARK:- Init View Model
    func initVM() {
        
        viewModel.updateStackView = { [weak self] () in
            
            DispatchQueue.main.async { [weak self] in
                self?.stack = self?.viewModel.getStackViewModel()
                if let stack = self?.stack {
                    self?.bindStackView(stackView: stack)
                }
            }
        }
        
        viewModel.updateSendingOrder = { [weak self] () in
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.sendingOrder {
                case .success:
                    self.showDonePopUp()
                case .failed:
                    print("Failed")
                }
            }
        }
        
        viewModel.updateDataStackView = { [weak self] () in
            
            DispatchQueue.main.async { [weak self] in
                self?.stackData = self?.viewModel.getDataStackViewModel()
                if let stack = self?.stackData {
                    self?.bindStackViewData(stackView: stack)
                }
            }
        }
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.showError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
                
        viewModel.updateTime = { [weak self] () in

                DispatchQueue.main.async { [weak self] in

                switch self!.viewModel.sorting {
                                    
                case .now:
                    if let technician = self?.technicianDetails {
                        self!.viewModel.initFetch(status: 0, technician: technician)
                    }
                    self!.choosedAppointmentLabel.text = "now".localized
                case .later:
                    if let technician = self?.technicianDetails {
                        self!.viewModel.initFetch(status: 1, technician: technician)
                    }
                    self!.choosedAppointmentLabel.text = "later".localized
                }
            }
        }
        
        // MARK:- Set now case
        if let technician = self.technicianDetails {
            viewModel.initFetch(status: 0, technician: technician)
        }
        self.stack = self.viewModel.getStackViewModel()
        if let stack = self.stack {
            self.bindStackView(stackView: stack)
        }

    }
    
    // MARK:- Show Time Options Picker
    func setupTimeSheet() {
        
        let sheet = UIAlertController(title: "time alert title".localized, message: "time alert message".localized, preferredStyle: .actionSheet)
        sheet.addAction(UIAlertAction(title: "now".localized, style: .default, handler: {_ in
            self.viewModel.sorting = .now
        }))
        sheet.addAction(UIAlertAction(title: "later".localized, style: .default, handler: {_ in
            self.viewModel.sorting = .later
        }))
        sheet.addAction(UIAlertAction(title: "cancel timing".localized, style: .cancel, handler: nil))
        
//        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)

    }

    // MARK:- Bind Stackview elements
    func bindStackView(stackView: SendOrderStackViewModel) {
        if stackView.name == true {
            nameStackView.isHidden = false
        } else {
            nameStackView.isHidden = true
        }

        if stackView.date == true {
            dateStackView.isHidden = false
        } else {
            dateStackView.isHidden = true
        }
        
        if stackView.time == true {
            timeStackView.isHidden = false
        } else {
            timeStackView.isHidden = true
        }
        
        if stackView.amountAgreed == true {
            amountAgreedStackView.isHidden = false
        } else {
            amountAgreedStackView.isHidden = true
        }
    }
    
    // MARK:- Bind Stackview Data
    func bindStackViewData(stackView: DataStackViewModel) {
        if let name: String = stackView.name {
            workerNameLabel.text = name
        }
        
        if let date: String = stackView.date {
            reservationDateLabel.text = date
            reservationDateLabel.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        }
        
        if let time: String = stackView.time {
            reservationTimeLabel.text = time
            reservationTimeLabel.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        }
        
        if let amountAgreed: Int = stackView.amountAgreed {
//            amountAgreedValueLabel.text = String(amountAgreed)
        }
    }
    
    func setNavigationTitle() {
        if let technicianName: String = self.technicianName {
                navigationItem.title = technicianName
        }
    }
    
    // MARK:- Configure Localization outlets
    func configure() {
        chooseAppointmentLabel.text = "\("choose appointment".localized):"
        nameLabel.text = "\("technician name".localized):"
        dateLabel.text = "\("date".localized):"
        timeLabel.text = "\("time".localized):"
        sendButton.setTitle("send button".localized, for: .normal)
        choosedAppointmentLabel.text = "now".localized
    }
    
    func configureView() {
        sendButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)

        navView.roundCorners([.bottomLeft, .bottomRight], radius: 30.0)
        stateView.roundCorners([.topRight, .bottomLeft], radius: 15.0)
        effectView.roundCorners([.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 7.0)
        
        bottomView.roundCorners([.topLeft, .topRight], radius: 30.0)
    }
    
    // MARK:- Set the time
    func setTheTime() {
        let min = Date().addingTimeInterval(-60 * 60 * 24 * 4)
        let max = Date().addingTimeInterval(60 * 60 * 24 * 4)
        let picker = DateTimePicker.create(minimumDate: min, maximumDate: max)
        picker.frame = CGRect(x: 0, y: 100, width: picker.frame.size.width, height: picker.frame.size.height)
        self.view.addSubview(picker)
        
        picker.isTimePickerOnly = true
        picker.is12HourFormat = false
//        picker.todayButtonTitle = "time now".localized
//        picker.todayButtonTitle = ""
        picker.cancelButtonTitle = ""

        picker.delegate = self
        
        picker.show()
    }
    
    // MARK:- Set date and time
    func setStartDate() {
        guard let startTime: String = reservationTimeLabel.text, startTime.count > 0, startTime.count == 8 else {
                let apiError = APIError()
                apiError.message = "برجاء إدخال التوقيت"
                self.viewModel.error = apiError
                self.viewModel.state = .error
                return
            }

        guard let startDate: String = reservationDateLabel.text, startDate.count > 0, startDate.count == 10 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال التاريخ"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let longitude: String = self.longitude, longitude.count > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال العنوان"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        guard let latitude: String = self.latitude, latitude.count > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال العنوان"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        
        let year: String = startDate.substring(with: 6..<10)
        let day: String = startDate.substring(with: 3..<5)
        let month: String = startDate.substring(with: 0..<2)
        
        let date = startDate.replacingOccurrences(of: "/", with: "-", options: .literal, range: nil)
        let newDate = "\(year)-\(month)-\(day)"
        print(newDate)
        
        let start: String = newDate + " " + startTime

        print("StartDate: \(start)")
        
        if let workerId: Int = self.technicianDetails?.userId {
            viewModel.sendOrderLater(status: 1, startDate: start, orderStatusId: 1, workerId: workerId, longitude: longitude, latitude: latitude)
        }
    }

    // MARK:- Actions
    @IBAction func reservationTimeButtonPressed(_ sender: Any) {
        setTheTime()
    }
    
    @IBAction func reservationDateButtonPressed(_ sender: Any) {
        datePickerTapped()
    }
    
    
    @IBAction func orderStateIsPressed(_ sender: Any) {
        print("Order State Pressed")
        setupTimeSheet()
    }
        
    @IBAction func sendOrderButtonPressed(_ sender: Any) {
        print("Send Order")
        guard let longitude: String = self.longitude, longitude.count > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال العنوان"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        guard let latitude: String = self.latitude, latitude.count > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال العنوان"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }

        if self.viewModel.sorting == .now {
            if let workerId: Int = self.technicianDetails?.userId {
                viewModel.sendOrderNow(status: 0, orderStatusId: 1, workerId: workerId, longitude: longitude, latitude: latitude)
            }
        } else if self.viewModel.sorting == .later {
            setStartDate()
        }
    }
    
    func showDonePopUp() {
        let alertController = UIAlertController(title: "تم إرسال الطلب إلى الفني !", message: "الرجوع إلى الرئيسية", preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "تم", style: .default) { (action) in
            self.goToHomePage()
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func goToHomePage() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeNavigationVC")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
}

// MARK:- Date Time Picker Delegate
extension SendOrderViewController: DateTimePickerDelegate {
    func dateTimePicker(_ picker: DateTimePicker, didSelectDate: Date) {
        var f: String?
        f = picker.selectedDateString
        let subString = f?.prefix(5)
        if "Lang".localized == "ar" {
            self.reservationTimeLabel.text = String(subString!) + ":٠٠"
        } else if "Lang".localized == "en" {
            self.reservationTimeLabel.text = String(subString!) + ":00"
        }
        self.reservationTimeLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
}
