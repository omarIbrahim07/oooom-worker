//
//  DataStackViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/25/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
struct DataStackViewModel {
    let name: String
    let date: String
    let time: String
    let amountAgreed: Int
}
