//
//  SendOrderViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/20/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import Toast_Swift

class SendOrderViewModel {
    
    public enum State {
        case loading
        case error
        case populated
        case empty
    }
    
    public enum sendingOrder {
        case success
        case failed
    }
    
    enum Sorting {
        case now
        case later
    }
        
    var updateSendingOrder: (()->())?
    var updateLoadingStatus: (()->())?
    var updateTime: (()->())?
    var updateStackView: (()->())?
    var updateDataStackView: (()->())?
        
    // callback for interfaces
    var sorting: Sorting = .now {
        didSet {
            self.updateTime?()
        }
    }
    
    // callback for interfaces
    var sendingOrder: sendingOrder = .failed {
        didSet {
            self.updateSendingOrder?()
        }
    }
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var stackViewModel: SendOrderStackViewModel? {
        didSet {
            self.updateStackView?()
        }
    }
    
    var error: APIError?
    
    var dataStackViewModel: DataStackViewModel? {
        didSet {
            self.updateDataStackView?()
        }
    }
    
    func initFetch(status: Int, technician: Technician) {
        self.stackViewModel = processStackView(status: status)
        self.dataStackViewModel = processDataStackView(technician: technician)
    }
    
    
    func sendOrderNow(status: Int, orderStatusId: Int, workerId: Int, longitude: String, latitude: String) {
            
        let params: [String : AnyObject] = [
            "status" : status as AnyObject,
            "orderStatus_id" : orderStatusId as AnyObject,
            "worker_id" : workerId as AnyObject,
            "longitude" : longitude as AnyObject,
            "latitude" : latitude as AnyObject
        ]
        
        self.state = .loading
        
        OrderAPIManager().SendOrder(basicDictionary: params, onSuccess: { (orderID) in
            if orderID != nil {
                print("Success")
                self.state = .populated
                self.sendingOrder = .success
            } else {
                self.state = .empty
                self.sendingOrder = .failed
            }
        }) { (error) in
            self.error = error
            self.state = .error
            self.sendingOrder = .failed
        }
    }
    
    func getError() -> APIError {
        return self.error!
    }
        
    func sendOrderLater(status: Int, startDate: String, orderStatusId: Int, workerId: Int, longitude: String, latitude: String) {
        
        guard let startDate: String = startDate, startDate.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال التاريخ"
            self.error = apiError
            self.state = .error
            return
        }
        
        let params: [String : AnyObject] = [
            "status" : status as AnyObject,
            "start_date" : startDate as AnyObject,
            "orderStatus_id" : orderStatusId as AnyObject,
            "worker_id" : workerId as AnyObject,
            "longitude" : longitude as AnyObject,
            "latitude" : latitude as AnyObject
        ]
        
        self.state = .loading
        
        OrderAPIManager().SendOrder(basicDictionary: params, onSuccess: { (orderID) in
            if orderID != nil {
                print("Success")
                self.state = .populated
                self.sendingOrder = .success
            } else {
                self.state = .empty
                self.sendingOrder = .failed
            }
        }) { (error) in
            self.error = error
            self.state = .error
            self.sendingOrder = .failed
        }
    }
    
    func processStackView(status: Int) -> SendOrderStackViewModel {
        if status == 0 {
            return SendOrderStackViewModel(name: true, date: false, time: false, amountAgreed: false)
        } else if status == 1 {
            return SendOrderStackViewModel(name: true, date: true, time: true, amountAgreed: false)
        }
        return SendOrderStackViewModel(name: true, date: true, time: true, amountAgreed: true)
    }
    
    func processDataStackView(technician: Technician) -> DataStackViewModel {
        return DataStackViewModel(name: technician.name!, date: "choose date".localized, time: "choose time".localized, amountAgreed: 0)
    }
    
    func getStackViewModel() -> SendOrderStackViewModel {
        return self.stackViewModel!
    }
    
    func getDataStackViewModel() -> DataStackViewModel {
        return self.dataStackViewModel!
    }
    
}
