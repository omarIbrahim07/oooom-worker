//
//  RequestBidViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/3/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import AVFoundation
import AssetsPickerViewController
import Toast_Swift
import PKHUD
import Photos

class RequestBidViewController: BaseViewController {
    
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    
    var serviceID: Int?
    var url: URL?
    var myFirstImage: UIImage? = UIImage()
    var mySecondImage: UIImage? = UIImage()
    var myThirdImage: UIImage? = UIImage()
    
    var choosedImage: Int?
    var firstImageData: Data?
    var secondImageData: Data?
    var thirdImageData: Data?
    
    var longitude: String?
    var latitude: String?



    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var firstImage: UIImageView!
    @IBOutlet weak var secondImage: UIImageView!
    @IBOutlet weak var thirdImage: UIImageView!
    @IBOutlet weak var bidView: UIView!
    @IBOutlet weak var requestDetailsTextView: UITextView!
    @IBOutlet weak var requestButton: UIButton!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var recordImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configure()
        closeKeypad()
        recordButton.isEnabled = false
        getLocation()
        
        
        recordingSession = AVAudioSession.sharedInstance()

        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        self.recordButton.isEnabled = true
                    } else {
                        // failed to record!
                    }
                }
            }
        } catch {
            // failed to record!
        }

    }
    
    func showDonePopUp() {
        let alertController = UIAlertController(title: "تم إرسال الطلب إلى الفني !", message: "الرجوع إلى الرئيسية", preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "تم", style: .default) { (action) in
            self.goToHomePage()
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func goToHomePage() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeNavigationVC")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    func getLocation() {
        let long: Double = UserDefaults.standard.double(forKey: "longitude")
        let lat: Double = UserDefaults.standard.double(forKey: "latitude")

        self.longitude = String(long)
        self.latitude = String(lat)
    }
    
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        requestDetailsTextView.endEditing(true)
    }

    
    func imagePressed() {
        
        let image = UIImagePickerController()
        
        image.delegate = self
        
        image.sourceType = .photoLibrary
        
        self.present(image, animated: true) {
            
        }
    }
    
    // MARK:- Send Bid with Record & Photos
    func sendBid() {
        guard let message = requestDetailsTextView.text, message.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال وصف المشكلة لديك"
            showError(error: apiError)
            return
        }
        
        guard let serviceID = self.serviceID, serviceID > 0 else {
            let apiError = APIError()
            apiError.message = "لا يوجد اختيار لطبيعة العمل"
            showError(error: apiError)
            return
        }

        guard let longitude: String = self.longitude, longitude.count > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال العنوان"
            showError(error: apiError)
            return
        }

        guard let latitude: String = self.latitude, latitude.count > 0 else {
            let apiError = APIError()
            apiError.message = "يرجى إدخال العنوان"
            showError(error: apiError)
            return
        }
        
        if let imgData = myFirstImage {
            firstImageData = imgData.jpegData(compressionQuality: 0.3)
        }
        
        if let imgData = mySecondImage {
            secondImageData = imgData.jpegData(compressionQuality: 0.3)
        }

        if let imgData = myThirdImage {
            thirdImageData = imgData.jpegData(compressionQuality: 0.3)
        }
        
        //MARK:- Convert file url into Data
        let fileURl = self.url
        let audioData = try? Data(contentsOf: fileURl!)

        let parameters: [String : AnyObject] = [
            "message" : message as AnyObject,
            "service_id" : serviceID as AnyObject,
            "status" : 0 as AnyObject,
            "orderStatus_id" : 1 as AnyObject,
            "longitude" : longitude as AnyObject,
            "latitude" : latitude as AnyObject
        ]
        
        self.startLoading()

        OrderAPIManager().sendBid(recordData: audioData, recordName: "record", imgData: firstImageData, imgData2: secondImageData, imgData3: thirdImageData, basicDictionary: parameters, onSuccess: { (message) in

            self.stopLoadingWithSuccess()
            //                UserDefaultManager.shared.currentUser = newUser
            print(message)
            
            self.showDonePopUp()
            
        }) { (error) in
            self.stopLoadingWithError(error: error)
        }
    }
    
    func startRecording() {
        let audioFilename = getDocumentsDirectory().appendingPathComponent("recording.m4a")

        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]

        do {
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder.delegate = self
            audioRecorder.record()

            print("Tap to Stop")
            recordImage.image = UIImage(named: "stop")
        } catch {
            finishRecording(success: false)
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }

    func finishRecording(success: Bool) {
        let fileUrl = audioRecorder.url
        url = fileUrl
        print(fileUrl)
        
        audioRecorder.stop()
        audioRecorder = nil

        if success {
            print("Tap to Re-record")
            recordImage.image = UIImage(named: "microphone")
        } else {
            print("Tap to Record")
            // recording failed :(
        }
    }
    


    
    func configure() {
        requestDetailsTextView.delegate = self
        requestDetailsTextView.text = "textview placeholder".localized
        requestDetailsTextView.textColor = UIColor.lightGray
        requestButton.setTitle("request button".localized, for: .normal)
        self.navigationItem.title = "request bid title".localized
    }
    
    func configureView() {
        navView.roundCorners([.bottomLeft, .bottomRight], radius: 30.0)
        bottomView.roundCorners([.topLeft, .topRight], radius: 30.0)
        requestButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        bidView.roundCorners([.topLeft, .bottomRight], radius: 8.0)
        requestDetailsTextView.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    
    @IBAction func requestBidButtonPressed(_ sender: Any) {
        print("Request Button pressed")
        sendBid()
    }
    
    
    @IBAction func firstImagePressed(_ sender: Any) {
        print("First image pressed")
        self.choosedImage = 1
        imagePressed()
    }
    
    @IBAction func secondImagePressed(_ sender: Any) {
        print("Second image pressed")
        self.choosedImage = 2
        imagePressed()
    }
    
    @IBAction func thirdImagePressed(_ sender: Any) {
        print("Third image pressed")
        self.choosedImage = 3
        imagePressed()
    }
    
    @IBAction func recordButtonPressed(_ sender: Any) {
        if audioRecorder == nil {
            startRecording()
        } else {
            finishRecording(success: true)
        }
    }
    
    // MARK:- send Photos Only
    //    func sendPhotosOnly() {
    //
    //        guard let message = requestDetailsTextView.text, message.count > 0 else {
    //            let apiError = APIError()
    //            apiError.message = "برجاء إدخال وصف المشكلة لديك"
    //            showError(error: apiError)
    //            return
    //        }
    //
    //        //MARK:- Convert file url into Data
    //        let fileURl = self.url
    //        let audioData = try? Data(contentsOf: fileURl!)
    //
    //        let parameters: [String : AnyObject] = [
    //            "message" : message as AnyObject,
    //            "service_id" : 1 as AnyObject
    //        ]
    //
    //
    //        //            OrderAPIManager().sendBid(recordData: audioData, recordName: "record", basicDictionary: parameters, onSuccess: { (message) in
    //
    //
    //        if let imgData = myFirstImage {
    //            firstImageData = imgData.jpegData(compressionQuality: 0.3)
    //        }
    //
    //        if let imgData = mySecondImage {
    //            secondImageData = imgData.jpegData(compressionQuality: 0.3)
    //        }
    //
    //        if let imgData = myThirdImage {
    //            thirdImageData = imgData.jpegData(compressionQuality: 0.3)
    //        }
    //
    //        startLoading()
    //
    //        OrderAPIManager().sendBidThreeImages(imgData: firstImageData, imgData2: secondImageData, imgData3: thirdImageData, basicDictionary: parameters, onSuccess: { (message) in
    //
    //            self.stopLoadingWithSuccess()
    //            //                UserDefaultManager.shared.currentUser = newUser
    //            print(message)
    //
    //            //                print("Photo: \(UserDefaultManager.shared.currentUser?.photo)")
    //            //                self.showDonePopUp()
    //
    //
    //        }) { (error) in
    //            self.stopLoadingWithError(error: error)
    //        }
    //    }

    // MARK:- Send Record Only
    //    func sendRecordOnly() {
    //        guard let message = requestDetailsTextView.text, message.count > 0 else {
    //            let apiError = APIError()
    //            apiError.message = "برجاء إدخال وصف المشكلة لديك"
    //            showError(error: apiError)
    //            return
    //        }
    //
    //        //MARK:- Convert file url into Data
    //        let fileURl = self.url
    //        let audioData = try? Data(contentsOf: fileURl!)
    //
    //        let parameters: [String : AnyObject] = [
    //            "message" : message as AnyObject,
    //            "service_id" : 1 as AnyObject
    //        ]
    //
    //            startLoading()
    //
    //            OrderAPIManager().sendBid(recordData: audioData, recordName: "record", basicDictionary: parameters, onSuccess: { (message) in
    //
    //                self.stopLoadingWithSuccess()
    //                //                UserDefaultManager.shared.currentUser = newUser
    //                print(message)
    //
    //                //                print("Photo: \(UserDefaultManager.shared.currentUser?.photo)")
    //                //                self.showDonePopUp()
    //
    //
    //            }) { (error) in
    //                self.stopLoadingWithError(error: error)
    //            }
    //    }

    
}


extension RequestBidViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "textview placeholder".localized
            textView.textColor = UIColor.lightGray
        }
    }

    
}

extension RequestBidViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            if choosedImage == 1 {
                firstImage.image = image
                firstImage.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
                myFirstImage = image
            } else if choosedImage == 2 {
                secondImage.image = image
                secondImage.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
                mySecondImage = image
            } else if choosedImage == 3 {
                thirdImage.image = image
                thirdImage.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
                myThirdImage = image
            }
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension RequestBidViewController: AVAudioRecorderDelegate {
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
}
