//
//  WorkerImageTableViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/22/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

protocol WorkerChatImageTableViewCellDelegate {
    func WorkerChatImageButtonPressed(image: String)
}

class WorkerImageTableViewCell: UITableViewCell {

    var delegate: WorkerChatImageTableViewCellDelegate?
    
        //MARK:- Delegate Helpers
    func WorkerChatImageButtonPressed(image: String) {
        if let delegateValue = delegate {
            delegateValue.WorkerChatImageButtonPressed(image: image)
        }
    }

    var selectedImagee: String?
    
    @IBOutlet weak var workerNameLabel: UILabel!
    @IBOutlet weak var messageDateLabel: UILabel!
    @IBOutlet weak var uploadedMessageImageView: UIImageView!
    @IBOutlet weak var workerImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configure()
    }
    
    func configure() {
        containerView.roundCorners([.topLeft, .bottomRight, .bottomLeft ], radius: 20)
        containerView.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        workerImageView.addCornerRadius(raduis: workerImageView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        uploadedMessageImageView.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func chatImageButtonIsPressed(_ sender: Any) {
        if let image = self.selectedImagee {
            WorkerChatImageButtonPressed(image: image)
        }
    }
    
    
}
