//
//  ClientImageTableViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/22/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

protocol ClientChatImageTableViewCellDelegate {
    func ClientChatImageButtonPressed(image: String)
}

class ClientImageTableViewCell: UITableViewCell {
    
    var delegate: ClientChatImageTableViewCellDelegate?
    
    var selectedImagee: String?
    
        //MARK:- Delegate Helpers
    func ClientChatImageButtonPressed(image: String) {
        if let delegateValue = delegate {
            delegateValue.ClientChatImageButtonPressed(image: image)
        }
    }

    @IBOutlet weak var clientNameLabel: UILabel!
    @IBOutlet weak var messageDateLAbel: UILabel!
    @IBOutlet weak var uploadedMessageImageView: UIImageView!
    @IBOutlet weak var clientImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configure()
    }
    
    func configure() {
        containerView.roundCorners([.topRight, .bottomRight, .bottomLeft ], radius: 20)
        containerView.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        clientImageView.addCornerRadius(raduis: clientImageView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
//        uploadedMessageImageView.addCornerRadius(raduis: 30.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        uploadedMessageImageView.roundCorners([.topRight, .bottomRight, .bottomLeft, .topLeft], radius: 20)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func clientImageButtonIsPressed(_ sender: Any) {
        if let image = self.selectedImagee {
            ClientChatImageButtonPressed(image: image)
        }
    }
    
}
