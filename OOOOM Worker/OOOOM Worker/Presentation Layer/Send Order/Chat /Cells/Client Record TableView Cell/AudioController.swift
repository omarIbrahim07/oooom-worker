//
//  AudioController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/23/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import SwiftAudio


class AudioController {
    
    static let shared = AudioController()
    let player: QueuedAudioPlayer
    let audioSessionController = AudioSessionController.shared
    
    func add()  {
        try? player.add(items: sources, playWhenReady: true)
    }
    
var sources: [AudioItem] = []
    
    init() {
        let controller = RemoteCommandController()
        player = QueuedAudioPlayer(remoteCommandController: controller)
        player.remoteCommands = [
            .stop,
            .play,
            .pause,
            .togglePlayPause,
            .next,
            .previous,
            .changePlaybackPosition
        ]
        try? audioSessionController.set(category: .playback)
//        try? player.add(items: sources, playWhenReady: false)
    }
    
}
