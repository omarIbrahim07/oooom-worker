//
//  WorkerRecordTableViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/22/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import SwiftAudio
import AVFoundation
import MediaPlayer

protocol workerplayButtonTableViewCellDelegate {
    func workerdidPlayButtonPressed(tag: Int)
}

class WorkerRecordTableViewCell: UITableViewCell {
    
    var sound: String?

    private var isScrubbing: Bool = false
    let player = AudioPlayer()
    private let controller = AudioController.shared
    private var lastLoadFailed: Bool = false

    var delegate: workerplayButtonTableViewCellDelegate?


    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var soundSlider: UISlider!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var timeElapsedLabel: UILabel!
    @IBOutlet weak var clientImageView: UIImageView!
    
    var playedCellIndex: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        configureAudioPlayerEventHandlers()
        configure()
    }
    
    func configure() {
        containerView.roundCorners([.topLeft, .bottomRight, .bottomLeft ], radius: 20)
        containerView.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        clientImageView.addCornerRadius(raduis: clientImageView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func workerdidPlayButtonPressed(tag: Int) {
        if let delegateValue = delegate {
            delegateValue.workerdidPlayButtonPressed(tag: tag)
        }
    }
    
    func configureAudioPlayerEventHandlers() {
        controller.player.event.stateChange.addListener(self, handleAudioPlayerStateChange)
        controller.player.event.secondElapse.addListener(self, handleAudioPlayerSecondElapsed)
        controller.player.event.seek.addListener(self, handleAudioPlayerDidSeek)
        controller.player.event.updateDuration.addListener(self, handleAudioPlayerUpdateDuration)
        controller.player.event.didRecreateAVPlayer.addListener(self, handleAVPlayerRecreated)
        updateMetaData()
        handleAudioPlayerStateChange(data: controller.player.playerState)
    }
    
    func updateTimeValues() {
        self.soundSlider.maximumValue = Float(self.controller.player.duration)
        self.soundSlider.setValue(Float(self.controller.player.currentTime), animated: true)
        self.timeElapsedLabel.text = self.controller.player.currentTime.secondsToString()
//        self.remainingTimeLabel.text = (self.controller.player.duration - self.controller.player.currentTime).secondsToString()
    }
    
    func updateMetaData() {
        if let item = controller.player.currentItem {
            item.getArtwork({ (image) in
//                self.imageView.image = image
            })
        }
    }

    // MARK: - AudioPlayer Event Handlers
    
    func handleAudioPlayerStateChange(data: AudioPlayer.StateChangeEventData) {
        guard let _ = playedCellIndex else {
            return
        }
        
        print(data)
        DispatchQueue.main.async {
            self.setPlayButtonState(forAudioPlayerState: data)
            switch data {
            case .loading:
//                self.loadIndicator.startAnimating()
                self.updateMetaData()
                self.updateTimeValues()
            case .buffering:
//                self.loadIndicator.startAnimating()
                print("")
            case .ready:
//                self.loadIndicator.stopAnimating()
                self.updateMetaData()
                self.updateTimeValues()
            case .playing, .paused, .idle:
//                self.loadIndicator.stopAnimating()
                self.updateTimeValues()
            }
        }
    }
    
    func setPlayButtonState(forAudioPlayerState state: AudioPlayerState) {
//        playButton.setTitle(state == .playing ? "Pause" : "Play", for: .normal)
        playButton.setImage(state == .playing ? #imageLiteral(resourceName: "pause (1)") : #imageLiteral(resourceName: "play-button-1"), for: .normal)
    }
    
    func handleAudioPlayerSecondElapsed(data: AudioPlayer.SecondElapseEventData) {
        guard let _ = playedCellIndex else {
            return
        }

        if !isScrubbing {
            DispatchQueue.main.async {
                self.updateTimeValues()
            }
        }
    }
    
    func handleAudioPlayerDidSeek(data: AudioPlayer.SeekEventData) {
        guard let _ = playedCellIndex else {
            return
        }

        isScrubbing = false
    }
    
    func handleAudioPlayerUpdateDuration(data: AudioPlayer.UpdateDurationEventData) {
        guard let _ = playedCellIndex else {
            return
        }

        DispatchQueue.main.async {
            self.updateTimeValues()
        }
    }
    
    func handleAVPlayerRecreated() {
        guard let _ = playedCellIndex else {
            return
        }

        try? controller.audioSessionController.set(category: .playback)
    }
    
    func setupPlayer(sound: String) {

        // To reset the current sound
//        controller.player.reset()
                    
        // load to remove any previous first book track
        controller.sources.append(DefaultAudioItem(audioUrl: sound, sourceType: .stream))
        controller.add()
        
        controller.sources = []
        controller.player.reset()

    }
    

    
        @IBAction func togglePlay(_ sender: Any) {
            print("Feeees")
            
            workerdidPlayButtonPressed(tag: playButton.tag)
            guard let sound = sound else {
                return
            }
            setupPlayer(sound: sound)
            
            if !controller.audioSessionController.audioSessionIsActive {
                try? controller.audioSessionController.activateSession()
            }
            if lastLoadFailed, let item = controller.player.currentItem {
                lastLoadFailed = false
    //            errorLabel.isHidden = true
                try? controller.player.load(item: item, playWhenReady: true)
            }
            else {
                controller.player.togglePlaying()
            }
        }


    
        @IBAction func startScrubbing(_ sender: UISlider) {
            guard let _ = playedCellIndex else {
                return
            }

            isScrubbing = true
        }
        
        @IBAction func scrubbing(_ sender: UISlider) {
            guard let _ = playedCellIndex else {
                return
            }

            controller.player.seek(to: Double(soundSlider.value))
        }
        
        @IBAction func scrubbingValueChanged(_ sender: UISlider) {
            guard let _ = playedCellIndex else {
                return
            }

            let value = Double(soundSlider.value)
            timeElapsedLabel.text = (controller.player.duration - value).secondsToString()
    //        timeElapsedLabel.text = value.secondsToString()
    //        remainingTimeLabel.text = (controller.player.duration - value).secondsToString()
        }

}


