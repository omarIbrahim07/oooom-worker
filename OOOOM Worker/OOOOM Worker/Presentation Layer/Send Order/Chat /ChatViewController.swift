//
//  ChatViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/28/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import Firebase
import AVFoundation
import AssetsPickerViewController
import Toast_Swift
import PKHUD
import Photos

class ChatViewController: BaseViewController {
    
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    
    var url: URL?
    var myFirstImage: UIImage? = UIImage()

    var firstImageData: Data?
    
    var isBid: Bool? = false
    var orderId: Int?
    var technicianName: String?
    var technicianID: Int?
    var bidPrice: Int?
    var technicianImage: String?
    var roomID: Int?
    var firstTime: Bool? = false
    var technicianDetails: Technician?

    var idString: String?
    
    var chatMessages: [ChatMessage] = [ChatMessage]()

    var playedCellIndex: Int?

    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var chatTextField: UITextField!
    @IBOutlet weak var recordImage: UIImageView!
    @IBOutlet weak var recordButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let technicianName: String = self.technicianName {
            setNavigationTitle(technicianName: technicianName)
        }
        configure()
        configureNavigationBar()
        configureTableView()
        
//        getCurrentTime()
        
        // Check if there is room id or not
        closeKeypad()
        chatTextField.delegate = self

        sendRoomID(check: 1)
        
        recordingSession = AVAudioSession.sharedInstance()

        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        self.recordButton.isEnabled = true
                    } else {
                        // failed to record!
                    }
                }
            }
        } catch {
            // failed to record!
        }

    }
    
    func startRecording() {
        let audioFilename = getDocumentsDirectory().appendingPathComponent("recording.m4a")

        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]

        do {
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder.delegate = self
            audioRecorder.record()

            print("Tap to Stop")
            recordImage.image = UIImage(named: "stop")
        } catch {
            finishRecording(success: false)
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }

    func finishRecording(success: Bool) {
        let fileUrl = audioRecorder.url
        url = fileUrl
        print(fileUrl)
        
        audioRecorder.stop()
        audioRecorder = nil

        if success {
            print("Tap to Re-record")
            recordImage.image = UIImage(named: "microphone")
        } else {
            print("Tap to Record")
            // recording failed :(
        }
    }

    
    func imagePressed() {
        
        let image = UIImagePickerController()
        
        image.delegate = self
        
        image.sourceType = .photoLibrary
        
        self.present(image, animated: true) {
            
        }
    }

    
    // MARK:- Get Current Time
    func getCurrentTime() -> String {
        let timestamp = Date().timeIntervalSince1970

        // gives date with time portion in UTC 0
        let date = Date(timeIntervalSince1970: timestamp)

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"  // change to your required format
        dateFormatter.timeZone = TimeZone.current

        // date with time portion in your specified timezone
        let dateString: String = dateFormatter.string(from: date)
        print(dateFormatter.string(from: date))
        
        return dateString
    }
    
    // MARK:- Configuration
    func configure() {
        chatTextField.placeholder = "chat placeholder".localized
    }
    
    func setNavigationTitle(technicianName: String) {
        navigationItem.title = technicianName
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "WorkerTableViewCell", bundle: nil), forCellReuseIdentifier: "WorkerTableViewCell")
        tableView.register(UINib(nibName: "ClientTableViewCell", bundle: nil), forCellReuseIdentifier: "ClientTableViewCell")
        tableView.register(UINib(nibName: "WorkerImageTableViewCell", bundle: nil), forCellReuseIdentifier: "WorkerImageTableViewCell")
        tableView.register(UINib(nibName: "ClientImageTableViewCell", bundle: nil), forCellReuseIdentifier: "ClientImageTableViewCell")
        tableView.register(UINib(nibName: "ClientRecordTableViewCell", bundle: nil), forCellReuseIdentifier: "ClientRecordTableViewCell")
        tableView.register(UINib(nibName: "WorkerRecordTableViewCell", bundle: nil), forCellReuseIdentifier: "WorkerRecordTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }

    func configureNavigationBar() {
    self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "report".localized, style: .plain, target: self, action: #selector(report))
    }
    
    @objc fileprivate func report() {
        print("Report is pressed")
        setupOptionSheet()
    }
    
    // MARK:- Show Options Picker
    func setupOptionSheet() {
        let sheet = UIAlertController()
//        let sheet = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        sheet.addAction(UIAlertAction(title: "report option".localized, style: .default, handler: {_ in
            if let fromUserID = UserDefaultManager.shared.currentUser?.id, let toUserID = self.technicianID {
                self.showReportPopUp(fromUserID: fromUserID, toUserId: toUserID)
            }
        }))
        sheet.addAction(UIAlertAction(title: "block option".localized, style: .default, handler: {_ in
            if let toUserID = self.technicianID {
                self.showBlockPopUp(toId: toUserID)
            }
        }))
        sheet.addAction(UIAlertAction(title: "report cancel button".localized, style: .cancel, handler: nil))
//        sheet.popoverPresentationController?.sourceView = s1WeigthLabel
        self.present(sheet, animated: true, completion: nil)
    }
            
    func showBlockPopUp(toId: Int) {
        let alertController = UIAlertController(title: "block title popup".localized, message: "block message popup".localized, preferredStyle: .alert)
        let openAction = UIAlertAction(title: "block okay button".localized, style: .default) { (action) in
            self.blockUser(toUserID: toId)
        }
        let cancelAction = UIAlertAction(title: "block cancel button".localized, style: .cancel)
        alertController.addAction(openAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
        
    func showReportPopUp(fromUserID: Int, toUserId: Int) {
        let alertController = UIAlertController(title: "report title popup".localized, message: "report message popup".localized, preferredStyle: .alert)
        let openAction = UIAlertAction(title: "report okay button".localized, style: .default) { (action) in
//            self.goToHomePage()
            self.sendReport(fromUserID: fromUserID, toUserID: toUserId)
        }
        let cancelAction = UIAlertAction(title: "report cancel button".localized, style: .cancel)
        alertController.addAction(openAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }

    func checkFirstMessage() {
        // Create Room Id
        if self.roomID == nil {
            sendRoomID(check: 0)
        }
    }
    
    func sendRoomID(check: Int) {
        if self.chatMessages.count == 0 {
            guard let toID = UserDefaultManager.shared.currentUser?.id, toID > 0 else {
                let apiError = APIError()
                apiError.message = "برجاء إدخال رقم العميل"
                showError(error: apiError)
                return
            }
            
            guard let fromID = self.technicianID, fromID > 0 else {
                let apiError = APIError()
                apiError.message = "برجاء إدخال رقم الفني"
                showError(error: apiError)
                return
            }


            let parameters: [String : AnyObject] = [
                "from_id" : fromID as AnyObject,
                "to_id" : toID as AnyObject,
                "check" : check as AnyObject,
            ]
            
            weak var weakSelf = self
            
//            startLoading()
            
            ChatAPIManager().getRoomID(basicDictionary: parameters, onSuccess: { (roomObj) in
                
//                self.stopLoadingWithSuccess()
                print(roomObj)
                
                if roomObj.found == true || roomObj.saved == true {
                    if let roomID = roomObj.roomID {
                        self.roomID = roomID
                        self.observeMessages(roomID: roomID)
                    }
                } else {
                    
                }
                            
            }) { (error) in
                weakSelf?.stopLoadingWithError(error: error)
            }
        }
    }
    
    // MARK:- Observe room id
    func observeMessages(roomID: Int) {
        let str = "c-"
        let roomID: String = str + String(roomID)
        let dataBaseRef = Database.database().reference()
        dataBaseRef.child(roomID).observe(.childAdded) { (snapShot) in
            print(snapShot)
            if let dataArray = snapShot.value as? [String : Any] {
                guard let fromUserID = dataArray["from_user_id"] as? String, let messageText = dataArray["message"] as? String, let messageType = dataArray["message_type"] as? String, let toUserID = dataArray["to_user_id"] as? String else { return }

                let message = ChatMessage(fromUSerID: fromUserID, toUSerID: toUserID, message: messageText, messageType: messageType)

                self.chatMessages.append(message)
                self.tableView.reloadData()
                self.scrollToTableViewBottom()
            }
        }
    }
    
//    // MARK:- Observe room id -- old
//    func observeMessages(roomID: Int) {
//        let roomID: String = String(roomID)
//        let dataBaseRef = Database.database().reference()
//        dataBaseRef.child(roomID).observe(.childAdded) { (snapShot) in
//            print(snapShot)
//            if let dataArray = snapShot.value as? [String : Any] {
//                guard let fromUserID = dataArray["from_user_id"] as? String, let messageText = dataArray["message"] as? String, let messageType = dataArray["message_type"] as? String, let toUserID = dataArray["to_user_id"] as? String else { return }
//
//                let message = ChatMessage(fromUSerID: fromUserID, toUSerID: toUserID, message: messageText, messageType: messageType)
//
//                self.chatMessages.append(message)
//                self.tableView.reloadData()
//                self.scrollToTableViewBottom()
//            }
//        }
//    }
    
    private func scrollToTableViewBottom()
    {
        guard self.chatMessages.count > 0 else { return }
        
        self.tableView.scrollToRow(at: IndexPath(row: self.chatMessages.count - 1, section: 0),
                                   at: UITableView.ScrollPosition.bottom,
                                   animated: false)
    }

    func sendReport(fromUserID: Int, toUserID: Int) {
        let params: [String : AnyObject] = [
            "from_userid" : fromUserID as AnyObject,
            "to_userid" : toUserID as AnyObject,
            "is_chat" : 1 as AnyObject,
        ]

        self.startLoading()
        OrderAPIManager().sendReport(basicDictionary: params, onSuccess: { (saved) in
            
            if saved == true {
                self.stopLoadingWithSuccess()
            } else if saved == false {
                
            }
            self.stopLoadingWithSuccess()
                        
        }) { (error) in
            self.stopLoadingWithError(error: error)
        }
    }
    
    func blockUser(toUserID: Int) {
        let params: [String : AnyObject] = [
            "to_id" : toUserID as AnyObject,
        ]
        
        self.startLoading()
        BlockAPIManager().blockUser(basicDictionary: params, onSuccess: { (saved) in
            
            
            if saved == true {
                self.stopLoadingWithSuccess()
                self.goToHomePage()
            } else if saved == false {
                
            }
            
            self.stopLoadingWithSuccess()

        }) { (error) in
            self.stopLoadingWithError(error: error)
        }
    }


    
    // MARK:- Navigation
    func goToSendOrder(technicianDetails: Technician) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SendOrderViewController") as! SendOrderViewController
        
        viewController.technicianDetails = technicianDetails
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToImageDetails(imageURL: String) {
        if let imageDetailsVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "ImageZoomedViewController") as? ImageZoomedViewController {
            //                rootViewContoller.test = "test String"
            imageDetailsVC.imageURL = imageURL
            self.present(imageDetailsVC, animated: true, completion: nil)
            print("bidDetailsVC")
        }
    }
    
//    func goToSendBidOrder(technicianDetails: Technician, price: Int, offerID: Int) {
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        let viewController = storyboard.instantiateViewController(withIdentifier: "SendOrderViewController") as! SendOrderViewController
//
//        viewController.technicianDetails = technicianDetails
//        viewController.price = price
//        viewController.orderID = offerID
//        viewController.isBid = true
//
//        navigationController?.pushViewController(viewController, animated: true)
//    }
    
    //MARK:- Send Record Only
    func sendRecordOnly() {
        
        guard let fromID = UserDefaultManager.shared.currentUser?.id, fromID > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال رقم العميل"
            showError(error: apiError)
            return
        }
        
        guard let toID = self.technicianID, toID > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال رقم الفني"
            showError(error: apiError)
            return
        }

        //MARK:- Convert file url into Data
        let fileURl = self.url
        let audioData = try? Data(contentsOf: fileURl!)

        let parameters: [String : AnyObject] = [:]

            startLoading()

            ChatAPIManager().sendRecord(recordData: audioData, recordName: "file", basicDictionary: parameters, onSuccess: { (message) in

                self.stopLoadingWithSuccess()
                //                UserDefaultManager.shared.currentUser = newUser
                print(message)

                //                print("Photo: \(UserDefaultManager.shared.currentUser?.photo)")
                //                self.showDonePopUp()
                self.sendMessage(message: message, fromID: fromID, toID: toID, messageType: "2")


            }) { (error) in
                self.stopLoadingWithError(error: error)
            }
    }
    
    func edit() {
        guard let fromID = UserDefaultManager.shared.currentUser?.id, fromID > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال رقم العميل"
            showError(error: apiError)
            return
        }
        
        guard let toID = self.technicianID, toID > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال رقم الفني"
            showError(error: apiError)
            return
        }
        
        if let imgData = myFirstImage {
            firstImageData = imgData.jpegData(compressionQuality: 0.3)
        }

        let parameters: [String : AnyObject] = [:]
            
        startLoading()
            
        ChatAPIManager().sendImage(imageData: firstImageData, basicDictionary: parameters, onSuccess: { (message) in
            
            self.stopLoadingWithSuccess()
            //                UserDefaultManager.shared.currentUser = newUser
            print(message)
            
            self.sendMessage(message: message, fromID: fromID, toID: toID, messageType: "1")
            
            //                print("Photo: \(UserDefaultManager.shared.currentUser?.photo)")
            //                self.showDonePopUp()
            
            
        }) { (error) in
            print(error)
        }
    }


    // MARK:- Actions
    @IBAction func recordButtonIsPressed(_ sender: Any) {
        print("recordButtonIsPressed")
        self.checkFirstMessage()

        if audioRecorder == nil {
            startRecording()
        } else {
            finishRecording(success: true)
            sendRecordOnly()
        }
    }
    
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        chatTextField.endEditing(true)
    }
    
    func sendMessage(message: String, fromID: Int, toID: Int, messageType: String) {
        let fromIDString: String = String(fromID)
        let toIDString: String = String(toID)
        let timeStamp: String = getCurrentTime()
        
        let dataArray: [String : Any] = ["from_user_id" : fromIDString,
                                         "message" : message,
                                         "message_type" : messageType,
                                         "to_user_id" : toIDString,
                                         "timestamp" : timeStamp]
        
        let refrence = Database.database().reference()
        if let roomID: Int = self.roomID {
            let str = "c-"
            let room = refrence.child(str + String(roomID))
            room.child(timeStamp).setValue(dataArray) { (err, ref) in
                if err == nil {
                    print("Zay elfol")
                    self.chatTextField.text = ""
                } else {
                    print("Zay elzeft")
                }
            }
        }
    }
    
    func showDonePopUp() {
        let alertController = UIAlertController(title: "رفع الصورة", message: "جاري رفع الصورة !", preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "تم", style: .default) { (action) in
            self.edit()
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }

        
    @IBAction func sendMessageButtonIsPressed(_ sender: Any) {
        print("sendMessageButtonIsPressed")
        
        guard let textMessage = self.chatTextField.text, textMessage.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء كتابة الرسالة"
            showError(error: apiError)
            return
        }
        
        guard let fromID = UserDefaultManager.shared.currentUser?.id, fromID > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال رقم العميل"
            showError(error: apiError)
            return
        }
        
        guard let toID = self.technicianID, toID > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال رقم الفني"
            showError(error: apiError)
            return
        }
        
        self.checkFirstMessage()
        self.sendMessage(message: textMessage, fromID: fromID, toID: toID, messageType: "0")
    }
    
    @IBAction func cameraButtonIsPressed(_ sender: Any) {
        print("cameraButtonIsPressed")
        self.checkFirstMessage()

        if let imgData = myFirstImage {
            firstImageData = imgData.jpegData(compressionQuality: 0.3)
            
        }
        imagePressed()


    }
    
    //MARK:- Navigation
    func goToHomePage() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeNavigationVC")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
}

// MARK:- Tableview Delegate
extension ChatViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chatMessages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let message: ChatMessage = self.chatMessages[indexPath.row]
        
        if let userID: Int = UserDefaultManager.shared.currentUser?.id {
            self.idString = String(userID)
            
            if chatMessages[indexPath.row].fromUSerID == self.idString {
                    // check message type
                if message.messageType == "0" {
                    if let cell: ClientTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ClientTableViewCell") as? ClientTableViewCell {
                        if let firstName: String = UserDefaultManager.shared.currentUser?.firstName, let lastName: String = UserDefaultManager.shared.currentUser?.lastName {
                            cell.clientNameLabel.text = firstName + " " + lastName
                        }
                        if let clientImage: String = UserDefaultManager.shared.currentUser?.image {
                            print(clientImage)
                            cell.clientImageView.loadImageFromUrl(imageUrl: ImageURL_USERS + clientImage)
                        }
                        if let textMessage: String = message.message {
                            cell.clientTextMessageLAbel.text = textMessage
                        }
                        return cell
                    }
                } else if message.messageType == "1" {
                    if let cell: ClientImageTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ClientImageTableViewCell") as? ClientImageTableViewCell {
                        if let firstName: String = UserDefaultManager.shared.currentUser?.firstName, let lastName: String = UserDefaultManager.shared.currentUser?.lastName {
                            //cell.clientNameLabel.text = firstName + " " + lastName
                        }
                        if let clientImage: String = UserDefaultManager.shared.currentUser?.image {
                            cell.clientImageView.loadImageFromUrl(imageUrl: ImageURL_USERS + clientImage)
                        }
                        if let imageMessage: String = message.message {
                            cell.uploadedMessageImageView.loadImageFromUrl(imageUrl: imageMessage)
                            cell.selectedImagee = imageMessage
                            cell.delegate = self
                        }
                        return cell
                    }
                } else if message.messageType == "2" {
                    if let cell: ClientRecordTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ClientRecordTableViewCell") as? ClientRecordTableViewCell {
                        if let clientRecord: String = message.message {
                            cell.playButton.tag = indexPath.row
                            cell.soundSlider.tag = indexPath.row
                            cell.delegate = self
                            cell.sound = clientRecord
                            if let index = playedCellIndex, indexPath.row == index {
                                cell.playedCellIndex = index
                            }
                            else {
                                cell.playedCellIndex = nil
                            }
                        }
                        return cell
                    }
                }
            } else if chatMessages[indexPath.row].fromUSerID != self.idString {
                    // check message type
                if message.messageType == "0" {
                    if let cell: WorkerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "WorkerTableViewCell") as? WorkerTableViewCell {
                        if let workerName: String = self.technicianName {
                            cell.workerName.text = workerName
                        }
                        if let textMessage: String = message.message {
                            cell.workerTextMessageLabel.text = textMessage
                        }
                        return cell
                    }
                } else if message.messageType == "1" {
                    if let cell: WorkerImageTableViewCell = tableView.dequeueReusableCell(withIdentifier: "WorkerImageTableViewCell") as? WorkerImageTableViewCell {
                        if let workerName: String = self.technicianName {
                            cell.workerNameLabel.text = workerName
                        }
                        if let imageMessage: String = message.message {
                            cell.uploadedMessageImageView.loadImageFromUrl(imageUrl: imageMessage)
                            cell.selectedImagee = imageMessage
                            cell.delegate = self
                        }
                        return cell
                    }
                } else if message.messageType == "2" {
                    if let cell: WorkerRecordTableViewCell = tableView.dequeueReusableCell(withIdentifier: "WorkerRecordTableViewCell") as? WorkerRecordTableViewCell {
                        if let clientRecord: String = message.message {
                            cell.playButton.tag = indexPath.row
                            cell.soundSlider.tag = indexPath.row
                            cell.delegate = self
                            cell.sound = clientRecord
                            if let index = playedCellIndex, indexPath.row == index {
                                cell.playedCellIndex = index
                            }
                            else {
                                cell.playedCellIndex = nil
                            }
                        }
                        return cell
                    }
                }
            }
        }
        return UITableViewCell()
    }
    
}

extension ChatViewController: WorkerChatImageTableViewCellDelegate {
    func WorkerChatImageButtonPressed(image: String) {
        self.goToImageDetails(imageURL: image)
    }
}

extension ChatViewController: ClientChatImageTableViewCellDelegate {
    func ClientChatImageButtonPressed(image: String) {
        self.goToImageDetails(imageURL: image)
    }
}

extension ChatViewController: playButtonTableViewCellDelegate {
    
    
    func didPlayButtonPressed(tag: Int) {
        playedCellIndex = tag
        tableView.reloadData()
    }
    
}

extension ChatViewController: workerplayButtonTableViewCellDelegate {
    
    func workerdidPlayButtonPressed(tag: Int) {
        playedCellIndex = tag
        tableView.reloadData()
    }
        
}

extension ChatViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
//                firstImage.image = image
//                firstImage.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
            myFirstImage = image
            self.dismiss(animated: true, completion: nil)
            self.showDonePopUp()
        }
    }
}

extension ChatViewController: AVAudioRecorderDelegate {
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
}

extension ChatViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        UIView.animate(withDuration: 0.5) {
            self.heightConstraint.constant = 375
            self.view.layoutIfNeeded()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.1) {
            self.heightConstraint.constant = 50
            self.view.layoutIfNeeded()
        }
    }
    
}




