//
//  ListOfChatsViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/21/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import Firebase

class ListOfChatsViewController: BaseViewController {
    
    var reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
    
    var chatLists: [ChatList] = [ChatList]()
    var chatMessage: ChatMessage?
    var chatMessages: [ChatMessage] = [ChatMessage]()
    
    var online: [String] = [String]()

//    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        navigationItem.title = "list of chats".localized
        configureTableView()
        
        getListOfChats()
    }

    func configureView() {
//        self.navView.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
//        let reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
//        reusableView.frame = self.navView.bounds
//        reusableView.titleLabel.text = "list of chats".localized
//        reusableView.titleImage.image = UIImage(named: "chat-bubble")
//        reusableView.backButtonDelegate = self
//        self.navView.addSubview(reusableView)
        
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "ListOfChatsTableViewCell", bundle: nil), forCellReuseIdentifier: "ListOfChatsTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
//     MARK:- Observe last message -- New
    func observeMessages(roomID: String) {
        let dataBaseRef = Database.database().reference()
        dataBaseRef.child(roomID).queryLimited(toLast: 1).observe(.childAdded) { [weak self] (snapShot) in
            
            guard let self = self else { return }
            print(snapShot)
            if let dataArray = snapShot.value as? [String : Any] {

                guard let fromUserID = dataArray["from_user_id"] as? String, let messageText = dataArray["message"] as? String, let messageType = dataArray["message_type"] as? String, let toUserID = dataArray["to_user_id"] as? String, let timeStamp = dataArray["timestamp"] as? String else { return }
                let message = ChatMessage(fromUSerID: fromUserID, toUSerID: toUserID, message: messageText, messageType: messageType,timeStamp: timeStamp)
                self.chatMessages.append(message)
                self.reloadTable()
            }
        }
    }

    func observeOnline(workerID: Int) {
        let workerID: String = String(workerID)
        let dataBaseRef = Database.database().reference()
        dataBaseRef.child("online").child(workerID).child("state").observe(.value) { [weak self] (snapShot) in
            print(snapShot)
            
            guard let self = self else { return }

            if let dataArray = snapShot.value as? [String : Any] {

                guard let state = dataArray["state"] as? String else { return }
                self.online.append(state)
                self.reloadTable()
            }
        }
    }
    
    func reloadTable() {
        if online.count == chatMessages.count {
            tableView.reloadData()
        }
    }
    
    //delay to time
    public func delay(_ delay: Double, closure: @escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
    public func mainQueue(_ closure: @escaping ()->()){
        DispatchQueue.main.async(execute: closure)
    }

    
    // MARK:- Networking
    func getListOfChats() {
        
        startLoading()
        
        weak var weakSelf = self

        let parameters: [String : AnyObject] = [:]

        ChatAPIManager().getListOfChats(basicDictionary: parameters, onSuccess: { [weak self] (listOfChats) in

            self!.stopLoadingWithSuccess()

            guard let self = self else { return }

            print("List Of Chats")
            self.chatLists = listOfChats
            print(self.chatLists.count)
            for i in 0..<self.chatLists.count {
                print("Loop \(i)")
                self.observeMessages(roomID: self.chatLists[i].newID!)
            }
            
            self.delay(10) {
                print("")
            }
            
            for i in 0..<self.chatLists.count {
                self.observeOnline(workerID: self.chatLists[i].toID!)
            }
            
        self.stopLoadingWithSuccess()
            self.reloadTable()


        }) { (error) in
            weakSelf?.stopLoadingWithSuccess()
        }
    }
        
    // MARK:- Navigation
//    func goToChat(chatList: ChatList) {
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        let viewController = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
//
//        viewController.technicianID = chatList.fromID
//        viewController.technicianName = chatList.fromUserName
//
//        self.present(viewController, animated: true, completion: nil)
////        navigationController?.pushViewController(viewController, animated: true)
//    }
    
        func goToChat(chatList: ChatList) {
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                viewController.technicianID = chatList.fromID
                viewController.technicianName = chatList.fromUserName
    //        viewController.statusID = statusID
            navigationController?.pushViewController(viewController, animated: true)
        }

    


}

extension ListOfChatsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chatMessages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: ListOfChatsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ListOfChatsTableViewCell", for: indexPath) as? ListOfChatsTableViewCell {
            if let chatList: ChatList = self.chatLists[indexPath.row], let chatMessage: ChatMessage = self.chatMessages[indexPath.row] {
                cell.bindTableViewCell(chatList: chatList, chatMessage: chatMessage)
            }
            
            if let online: String = self.online[indexPath.row] {
                cell.checkOnline(onlineCheck: online)
            }
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Hey")
        if let chatList: ChatList = self.chatLists[indexPath.row] {
            goToChat(chatList: chatList)
        }
    }
    
}

//extension ListOfChatsViewController: backButtonViewDelegate {
//
//    func didbackButtonPressed() {
//        print("Delegate fired")
//        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
//    }
//}
