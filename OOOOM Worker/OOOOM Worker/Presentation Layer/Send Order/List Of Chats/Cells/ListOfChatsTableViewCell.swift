//
//  ListOfChatsTableViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/22/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class ListOfChatsTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var workerImage: UIImageView!
    @IBOutlet weak var workerNameLabel: UILabel!
    @IBOutlet weak var lastMessageLabel: UILabel!
    @IBOutlet weak var lastMessageDateLabel: UILabel!
    @IBOutlet weak var lastMessageTimeLabel: UILabel!
    @IBOutlet weak var seenImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureCell()
    }
    
    func configureCell() {
        containerView.layer.cornerRadius = 8.0
        workerImage.addCornerRadius(raduis: workerImage.frame.height / 2 , borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        selectionStyle = .none
    }
    
    func checkOnline(onlineCheck: String) {
        if onlineCheck == "0" {
            print("Offline")
            seenImage.image = UIImage(named: "offline")
        } else if onlineCheck == "1" {
            print("Online")
            seenImage.image = UIImage(named: "ic_radio_button_checked_24px")
        }
    }
    
    func bindTableViewCell(chatList: ChatList, chatMessage: ChatMessage) {
        
        if let messageType: String = chatMessage.messageType, let lastMessage: String = chatMessage.message {
            if messageType == "0" {
                lastMessageLabel.text = lastMessage
            } else if messageType == "1" || messageType == "2" {
                lastMessageLabel.text = "Attachment"
            }
        }
        
        if let workerName: String = chatList.fromUserName {
            workerNameLabel.text = workerName
        }
        
        if let workerImagee: String = chatList.fromUserImage {
            let url = URL(string: ImageURL_USERS + workerImagee)
            workerImage.kf.setImage(with: url)
        }
        
        if let timeStamp: String = chatMessage.timeStamp {
            lastMessageDateLabel.text = timeStamp
        }
    }
}
