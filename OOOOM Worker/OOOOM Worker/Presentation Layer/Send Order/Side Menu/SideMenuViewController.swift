//
//  SideMenuViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/10/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {
    
    let menuArray: [String] = [
        "Main",
        "Offers",
        "Profile",
        "My Wallet",
        "Rate App",
        "Saftey procedures",
        "Share your experience",
        "Terms and conditions",
        "Contact us",
        "About app",
        "Block list",
    ]
    
    let arabicMenuArray: [String] = [
        "الرئيسية",
        "العروض",
        "الصفحة الشخصية",
        "محفظتي",
        "تقييم البرنامج",
        "إجراءات السلامة",
        "شارك تجربتك",
        "الشروط والأحكام",
        "تواصل معنا",
     "حول التطبيق",
     "قائمة المحظورين",
    ]
    
    let menuImages: [String] = [
        "main",
        "offers",
        "profile",
        "wallet",
        "rate_us",
        "safety",
        "shares",
        "terms",
        "about_us",
        "contact_us",
        "512",
    ]

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureMenuTableViewCell()
    }
    
    func configureView() {
        
    }
    
    func configureMenuTableViewCell() {
        tableView.register(UINib(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuTableViewCell")
        tableView.register(UINib(nibName: "InfoTableViewCell", bundle: nil), forCellReuseIdentifier: "InfoTableViewCell")
        tableView.register(UINib(nibName: "LogoutTableViewCell", bundle: nil), forCellReuseIdentifier: "LogoutTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func goToEditAddAdvertisementLevelOne() {
        
        if let addNavigationController = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "OffersViewControllerNav") as? UINavigationController, let rootViewContoller = addNavigationController.viewControllers[0] as? OffersViewController {
            //                rootViewContoller.test = "test String"
            self.present(addNavigationController, animated: true, completion: nil)
            print("Fes")
        }
    }
    
    func goToOffers() {
        if let offersController = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "OffersViewController") as? OffersViewController {
            //                rootViewContoller.test = "test String"
//            offersController.choosedServiceEnum = .menu
            offersController.viewModel.choosedServiceEnum = .menu
            self.present(offersController, animated: true, completion: nil)
            print("Offers")
        }
    }
        
    func goToProfile() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToMentainanceOrders() {
        if let mentainanceOrdersController = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "MentainanceOrdersViewController") as? MentainanceOrdersViewController {
            //                rootViewContoller.test = "test String"
            self.present(mentainanceOrdersController, animated: true, completion: nil)
            print("Mentainance Orders")
        }
    }
    
    func goToBids() {
//        if let myAccountNavigationController = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "BidsViewControllernav") as? UINavigationController, let rootViewContoller = myAccountNavigationController.viewControllers[0] as? BidsViewController {
//            //                rootViewContoller.test = "test String"
//            self.present(myAccountNavigationController, animated: true, completion: nil)
//            print("Fes")
//        }
        
        if let bidsController = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "BidsViewController") as? BidsViewController {
            //                rootViewContoller.test = "test String"
            self.present(bidsController, animated: true, completion: nil)
            print("Bids")
        }
    }
    
    func goToListOfChats() {
        if let listOfChats = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "ListOfChatsViewController") as? ListOfChatsViewController {
            //                rootViewContoller.test = "test String"
            self.present(listOfChats, animated: true, completion: nil)
            print("List Of Chats")
        }
    }
    
    func goToMyAccount() {
        if let myAccount = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "MyAccountViewController") as? MyAccountViewController {
            //                rootViewContoller.test = "test String"
            self.present(myAccount, animated: true, completion: nil)
            print("My Account")
        }
    }
    
    func goToSafteyProcedures() {
        if let safteyProceduresVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "SafteyProceduresViewController") as? SafteyProceduresViewController {
            //                rootViewContoller.test = "test String"
            self.present(safteyProceduresVC, animated: true, completion: nil)
            print("Saftey Procedures")
        }
    }
    
    func goToTermsAndConditions() {
        if let termsAndConditionsVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "TermsAndConditionsViewController") as? TermsAndConditionsViewController {
            //                rootViewContoller.test = "test String"
            self.present(termsAndConditionsVC, animated: true, completion: nil)
            print("Terms and conditions")
        }
    }
    
    func goToPromoCode() {
        if let promoCodeVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "PromoCodeViewController") as? PromoCodeViewController {
            //                rootViewContoller.test = "test String"
            self.present(promoCodeVC, animated: true, completion: nil)
            print("Terms and conditions")
        }
    }
    
    func goToComplainsAndSuggestions() {
        if let complainsAndSuggstionsVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "ComplainsAndSuggestionsViewController") as? ComplainsAndSuggestionsViewController {
            //                rootViewContoller.test = "test String"
            self.present(complainsAndSuggstionsVC, animated: true, completion: nil)
            print("Terms and conditions")
        }
    }
    
    func goToConnectWithUs() {
        if let complainsAndSuggstionsVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "ContactWithUsViewController") as? ContactWithUsViewController {
            //                rootViewContoller.test = "test String"
            self.present(complainsAndSuggstionsVC, animated: true, completion: nil)
            print("Connect With Us")
        }
    }
    
    func goToMyWallet() {
        if let myWalletVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "MyWalletViewController") as? MyWalletViewController {
            //                rootViewContoller.test = "test String"
            self.present(myWalletVC, animated: true, completion: nil)
            print("MyWallet")
        }
    }
    
    func goToAboutApp() {
        if let complainsAndSuggstionsVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "AboutAppViewController") as? AboutAppViewController {
            //                rootViewContoller.test = "test String"
            self.present(complainsAndSuggstionsVC, animated: true, completion: nil)
            print("About App")
        }
    }
    
    func logout() {
                
        AuthenticationAPIManager().logout(basicDictionary: [:], onSuccess: { (message) -> String in
            
            self.goToLogIn()
            
            return "70bby"
            
        }) { (error) in
            print(error)
        }
    }
    
    func goToLogIn() {
        let storyboard = UIStoryboard.init(name: "Authentication", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "LoginNavigationVC")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    func goToBlockList() {
        if let blockListVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "BlockListViewController") as? BlockListViewController {
            //                rootViewContoller.test = "test String"
//            blockListVC.imageURL = imageURL
            self.present(blockListVC, animated: true, completion: nil)
            print("Block List VC")
        }
    }


    
}

extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArray.count + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            if let cell: InfoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "InfoTableViewCell", for: indexPath) as? InfoTableViewCell {
                if let user = UserDefaultManager.shared.currentUser {
                    cell.emailLabel.text = user.email
                    if let firstName: String = user.firstName, let lastName: String = user.lastName {
                        cell.userNameLabel.text = firstName + " " + lastName
                    }
                    
                    if let image: String = user.image {
                        cell.userImage.loadImageFromUrl(imageUrl: ImageURL_USERS + image)
                    }
                    
                    if let email: String = user.email {
                        cell.emailLabel.text = email
                    }
                }
                return cell
            }
        }
        
        else if indexPath.row > 0 && indexPath.row < 12 {
            if let cell: MenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as? MenuTableViewCell {
                
                
                if "Lang".localized == "en" {
                    cell.menuLabel.text = menuArray[indexPath.row - 1]
                } else if "Lang".localized == "ar" {
                    cell.menuLabel.text = arabicMenuArray[indexPath.row - 1]
                }
                cell.menuImage.image = UIImage(named: menuImages[indexPath.row - 1])
                return cell
            }
        }
        
        else if indexPath.row == 12 {
            if let cell: LogoutTableViewCell = tableView.dequeueReusableCell(withIdentifier: "LogoutTableViewCell", for: indexPath) as? LogoutTableViewCell {
                if "Lang".localized == "en" {
                    cell.logoutLabel.text = "logout".localized
                } else if "Lang".localized == "ar" {
                    cell.logoutLabel.text = "logout".localized
                }
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 1 {
            dismiss(animated: true, completion: nil)
        }
        else if indexPath.row == 2 {
            print("GO TO OFFERS")
//            self.goToOffers()
            self.goToEditAddAdvertisementLevelOne()
        } else if indexPath.row == 3 {
            print("GO TO PROFILE")
            self.goToProfile()
        } else if indexPath.row == 4 {
            goToMyWallet()
        } else if indexPath.row == 5 {
            print("GO TO RATE")
        } else if indexPath.row == 6 {
            goToSafteyProcedures()
        } else if indexPath.row == 7 {
            print("SHARE YOUR EXPERIENCE")
        } else if indexPath.row == 8 {
            goToTermsAndConditions()
        } else if indexPath.row == 9 {
            goToConnectWithUs()
        } else if indexPath.row == 10 {
            goToAboutApp()
        } else if indexPath.row == 11 {
            self.goToBlockList()
        } else if indexPath.row == 12 {
            self.logout()
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
}
