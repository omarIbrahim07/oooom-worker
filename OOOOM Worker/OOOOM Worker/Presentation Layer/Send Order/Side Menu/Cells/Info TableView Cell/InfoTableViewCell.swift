//
//  InfoTableViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/12/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class InfoTableViewCell: UITableViewCell {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureInfoTableViewCell()
        self.selectionStyle = .none
    }

    func configureInfoTableViewCell() {
        userImage.addCornerRadius(raduis: 50.0, borderColor: #colorLiteral(red: 0.8745098039, green: 0.9215686275, blue: 0.9647058824, alpha: 1), borderWidth: 1)
    }
}
