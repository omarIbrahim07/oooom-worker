//
//  HomeCellViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/11/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

struct HomeCellViewModel {
    let titleText: String
    let image: String
}

