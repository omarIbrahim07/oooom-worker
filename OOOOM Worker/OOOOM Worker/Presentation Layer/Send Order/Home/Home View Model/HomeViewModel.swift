//
//  HomeViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/11/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import Toast_Swift

public enum State {
    case loading
    case error
    case populated
    case empty
}

class HomeViewModel {
    
    var selectedService: Service?
    var error: APIError?
    
    private var services: [Service] = [Service]()
    
    var cellViewModels: [HomeCellViewModel] = [HomeCellViewModel]() {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
    
    var reloadUserData: (()->())?
    var reloadTableViewClosure: (()->())?
    var showAlertClosure: (()->())?
    var updateLoadingStatus: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var numberOfCells: Int {
         return cellViewModels.count
     }
    
    func initAccountData() -> User {
        
        AuthenticationAPIManager().getUserProfile(onSuccess: { (user) in
            
            UserDefaultManager.shared.currentUser = user
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
        
        if let user = UserDefaultManager.shared.currentUser {
            print(user)
            return user
        }
        return User()
    }
    
//    func initFetch() {
//        state = .loading
//        
//        ServicesAPIManager().getServices(basicDictionary: [:], onSuccess: { (services) in
//            
//            self.processFetchedPhoto(services: services)
//            self.state = .populated
//
//            }) { (error) in
//                self.error = error
//                self.state = .error
//            }
//    }
    
//    func createCellViewModel( service: Service ) -> HomeCellViewModel {
//
//        //Wrap a description
//        var descTextContainer: [String] = [String]()
//        if let camera = service.name {
//            descTextContainer.append(camera)
//        }
//
//        if let description = service.image {
//            descTextContainer.append( description )
//        }
        
//        if "Lang".localized == "ar" {
//            return HomeCellViewModel(titleText: service.name!, imageUrl: service.image!)
//        } else if "Lang".localized == "en" {
//            return HomeCellViewModel(titleText: service.nameEN!, imageUrl: service.image!)
//        }
        
//        return HomeCellViewModel(titleText: service.name!, imageUrl: service.image!)
//    }
    
//    func processFetchedPhoto( services: [Service] ) {
//        self.services = services // Cache
//        var vms = [HomeCellViewModel]()
//        for service in services {
//            vms.append( createCellViewModel(service: service) )
//        }
//        self.cellViewModels = vms
//    }
    
    func getCellViewModel( at indexPath: IndexPath ) -> HomeCellViewModel {
        return cellViewModels[indexPath.row]
    }
    
    func getError() -> APIError {
        return self.error!
    }
    
    func userPressed( at indexPath: IndexPath ){
        let service = self.services[indexPath.row]

        self.selectedService = service
    }
    
}
