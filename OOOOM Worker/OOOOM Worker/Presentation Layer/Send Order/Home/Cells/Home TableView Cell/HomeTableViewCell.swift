//
//  HomeTableViewCell.swift
//  OOOOM Worker
//
//  Created by Omar Ibrahim on 1/9/20.
//  Copyright © 2020 Egy Designer. All rights reserved.
//

import UIKit
import Kingfisher

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var menuImage: UIImageView!
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var numberOfOrdersImageView: UIImageView!
    @IBOutlet weak var numberOfOrdersLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }
    
    func configureView() {
        view.layer.cornerRadius = 8.0
        selectionStyle = .none
        numberOfOrdersImageView.isHidden = true
        numberOfOrdersLabel.isHidden = true
    }

    func setHomeTableView(title: String, image: String) {
        if let title: String = title {
            titleLabel.text = title
        }
        
        if let image: String = image {
            menuImage.image = UIImage(named: image)
        }
        
    }
    
    func setNumbers(orderNumbers: Int) {
        if let orders: Int = orderNumbers {
            numberOfOrdersLabel.isHidden = false
            numberOfOrdersImageView.isHidden = false
            numberOfOrdersLabel.text = String(orders)
        }
    }
    
}
