//
//  HomeViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/10/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import SideMenu
import CoreLocation

class HomeViewController: BaseViewController {
    
    lazy var viewModel: HomeViewModel = {
        return HomeViewModel()
    }()
    
    var user: User?
    
    let menuArray: [String] = [
        "Profile",
        "List Of Chats",
        "New Orders",
        "Current Orders",
        "Bids",
        "Archeive",
    ]

    let arabicMenuArray: [String] = [
        "الصفحة الشخصية",
        "قائمة المحادثات",
        "الطلبات الجديدة",
        "الطلبات الحالية",
        "طلبات التسعير",
        "الأرشيف"
    ]
    
    let menuImages: [String] = [
        "profile",
        "chats1",
        "new_orders",
        "current_orders",
        "bids",
        "document",
    ]

    
    var error: APIError?
    let locationManager = CLLocationManager()
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setNavigationTitle()
        configureSideMenu()
        configureView()
        makeCustomLogoutBarButtonItem(imageName: "Hamburger", buttonType: "left")
//        makeNotificationIcon(imageName: "bell", buttonType: "right")
        configureSideMenu()
        configureTableView()
        
        user = viewModel.initAccountData()
        
        locationManager.requestWhenInUseAuthorization()
        checkLocationManager()
        
//        initVM()
        setTimer(seconds: 3600)
    }
    
    func setTimer(seconds: Double) {
        Timer.scheduledTimer(timeInterval: seconds, target: self, selector: #selector(repeatCodeEveryOneHour), userInfo: nil, repeats: true)
    }
    
    @objc func repeatCodeEveryOneHour()
    {
        self.viewDidLoad()
    }

    
    func checkLocationManager() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    func initVM() {

        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.showError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }

        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
//        viewModel.initFetch()

    }
    
    func setNavigationTitle() {
        navigationItem.title = "home".localized
    }
    
    func configureView() {
        mainView.roundCorners([.bottomLeft, .bottomRight], radius: 30.0)
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func makeCustomLogoutBarButtonItem(imageName: String, buttonType: String) {
        
        let barButton = UIButton()
        
        let containerButton = UIButton(frame: CGRect(x: -5, y: 0, width: 35, height: 35))
        
        containerButton.setImage(UIImage(named: imageName), for: .normal)
        
        containerButton.setImage(UIImage(named: imageName), for: .highlighted)
        
        containerButton.imageView?.contentMode = .scaleAspectFit
        
        if "Lang".localized == "en" {
            containerButton.addTarget(self, action: #selector(self.configureLeftSideMenu), for: UIControl.Event.touchUpInside)
        } else if "Lang".localized == "ar" {
            containerButton.addTarget(self, action: #selector(self.configureRightSideMenu), for: UIControl.Event.touchUpInside)
        }
        
        barButton.addSubview(containerButton)
        
        if buttonType == "right" {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: barButton)
        } else if buttonType == "left" {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: barButton)
        }
    }
    
    func makeNotificationIcon(imageName: String, buttonType: String) {
        
        let barButton = UIButton()
        
        let containerButton = UIButton(frame: CGRect(x: -5, y: 0, width: 35, height: 35))
        
        containerButton.setImage(UIImage(named: imageName), for: .normal)
        
        containerButton.setImage(UIImage(named: imageName), for: .highlighted)
        
        containerButton.imageView?.contentMode = .scaleAspectFit
        
        containerButton.addTarget(self, action: #selector(self.goToNotifications), for: UIControl.Event.touchUpInside)
        
        barButton.addSubview(containerButton)
        
        if buttonType == "right" {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: barButton)
        } else if buttonType == "left" {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: barButton)
        }
    }
    
    func saveLocation(longitude: Double, latitude: Double) {
        UserDefaults.standard.set(longitude, forKey: "longitude") //Bool
        UserDefaults.standard.set(latitude, forKey: "latitude") //Bool
    }
    
    @objc func configureSideMenu() {
        // Define the menus
        let leftMenuNavigationControllerr = storyboard?.instantiateViewController(withIdentifier: "LefttSideMenu") as? UISideMenuNavigationController

        SideMenuManager.default.leftMenuNavigationController?.menuWidth = view.frame.size.width - 100
        SideMenuManager.default.leftMenuNavigationController = leftMenuNavigationControllerr
        
        if let x = self.navigationController
        {
            
            SideMenuManager.default.addPanGestureToPresent(toView: x.navigationBar)
            SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: x.view, forMenu: SideMenuManager.PresentDirection.left)
        }
        
        leftMenuNavigationControllerr!.statusBarEndAlpha = 0
    }
    
    @objc func configureLeftSideMenu() {
        // Define the menus
        let menu = storyboard!.instantiateViewController(withIdentifier: "LefttSideMenu") as! UISideMenuNavigationController
        SideMenuManager.default.leftMenuNavigationController = menu

        present(menu, animated: true, completion: nil)
        
        // (Optional) Prevent status bar area from turning black when menu appears:
        menu.statusBarEndAlpha = 0
    }
    
    @objc func configureRightSideMenu() {
        // Define the menus
        let menu = storyboard!.instantiateViewController(withIdentifier: "LefttSideMenu") as! UISideMenuNavigationController
        SideMenuManager.default.leftMenuNavigationController?.leftSide = false
        SideMenuManager.default.leftMenuNavigationController = menu
        
        present(menu, animated: true, completion: nil)
        
        // (Optional) Prevent status bar area from turning black when menu appears:
        menu.statusBarEndAlpha = 0
    }
    
    // MARK:- Navigation
    func goToProfile() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func goToNotifications() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "NotificationsViewController") as! NotificationsViewController
        //        viewController.typeIdSelected = self.typeIdSelected
        navigationController?.pushViewController(viewController, animated: true)
    }

    func goToOrders(statusID: Int) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MentainanceOrdersViewController") as! MentainanceOrdersViewController
        viewController.statusID = statusID
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToTechnicians(service: Service) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "TechniciansViewController") as! TechniciansViewController
        viewController.choosedService = service
        //        viewController.typeIdSelected = self.typeIdSelected
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToListOfChats() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ListOfChatsViewController") as! ListOfChatsViewController
//        viewController.statusID = statusID
        navigationController?.pushViewController(viewController, animated: true)
    }

}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return viewModel.numberOfCells
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: HomeTableViewCell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell") as? HomeTableViewCell {
            if "Lang".localized == "ar" {
                cell.setHomeTableView(title: arabicMenuArray[indexPath.row], image: menuImages[indexPath.row])
            } else if "Lang".localized == "en" {
                cell.setHomeTableView(title: menuArray[indexPath.row], image: menuImages[indexPath.row])
            }
            
            if indexPath.row == 2 {
                if let user = UserDefaultManager.shared.currentUser {
                    if let newOrders: Int = user.newOrdersCount {
                        cell.setNumbers(orderNumbers: newOrders)
                    }
                }
            }
            
            if indexPath.row == 4 {
                if let user = UserDefaultManager.shared.currentUser {
                    if let bids: Int = user.newBidsCount {
                        cell.setNumbers(orderNumbers: bids)
                    }
                }
            }
                        
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            goToProfile()
        } else if indexPath.row == 1 {
            print("GO TO LIST OF CHATS")
            self.goToListOfChats()
        } else if indexPath.row == 2 {
            self.goToOrders(statusID: 1)
        } else if indexPath.row == 3 {
            self.goToOrders(statusID: 2)
        } else if indexPath.row == 4 {
            print("GO TO BIDS")
            self.goToOrders(statusID: 0)
        } else if indexPath.row == 5 {
            self.goToOrders(statusID: 3)
        }
    }
}

extension HomeViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        UserDefaults.standard.set(, forKey: "ID") //Bool
        if let location = locations.first {
            print(location.coordinate)
            saveLocation(longitude: location.coordinate.longitude, latitude: location.coordinate.latitude)
        }
    }
}
