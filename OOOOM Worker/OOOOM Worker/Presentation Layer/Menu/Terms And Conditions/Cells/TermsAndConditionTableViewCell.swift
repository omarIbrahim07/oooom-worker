//
//  TermsAndConditionTableViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/20/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class TermsAndConditionTableViewCell: UITableViewCell {

    @IBOutlet weak var termsAndConditionsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setTermsAndConditions(title: String) {
        termsAndConditionsLabel.text = title
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
