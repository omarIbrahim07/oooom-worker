//
//  ComplainsAndSuggestionsViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/28/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

class ComplainsAndSuggestionsViewModel {
    
    public enum State {
        case loading
        case error
        case populated
        case empty
    }
    
    var updateLoadingStatus: (()->())?
    
    var error: APIError?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    func sendFeedback(comment: String) {
        
        let params: [String : AnyObject] = [
            "comment" : comment as AnyObject,
        ]
        
        self.state = .loading
        
        ComplainsAPIManager().sendFeedback(basicDictionary: params, onSuccess: { (saved) in
            if saved == true {
            print("Success")
            self.state = .populated
            } else if saved == false {
            self.state = .error
            }
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func getError() -> APIError {
        return self.error!
    }
    
}
