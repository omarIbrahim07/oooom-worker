//
//  ComplainsAndSuggestionsViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/21/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class ComplainsAndSuggestionsViewController: BaseViewController {
    
    lazy var viewModel: ComplainsAndSuggestionsViewModel = {
        return ComplainsAndSuggestionsViewModel()
    }()
    
    var error: APIError?
    
    var reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View

    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var complainTextView: UITextView!
    @IBOutlet weak var middleView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configure()
        
        initVM()
    }
    
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                    self.configure()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.showError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
        
    }

    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        complainTextView.endEditing(true)
    }
    
    func configure() {
        complainTextView.delegate = self
        complainTextView.text = "rate text view placeholder".localized
        complainTextView.textColor = UIColor.white
        sendButton.setTitle("rate now button".localized, for: .normal)
        closeKeypad()
    }

    func configureView() {
        self.navView.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        bottomView.roundCorners([.topLeft, .topRight], radius: 30.0)
        sendButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        let reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
        reusableView.frame = self.navView.bounds
        reusableView.titleLabel.text = "complaints".localized
        reusableView.titleImage.image = UIImage(named: "complaint")
        reusableView.backButtonDelegate = self
        self.navView.addSubview(reusableView)
        complainTextView.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    @IBAction func sendButtonPressed(_ sender: Any) {
        print("Send")
        guard let comment: String = complainTextView.text, comment.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء كتابة شكواك أو مقترحك"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        self.viewModel.sendFeedback(comment: comment)

    }
    
}

extension ComplainsAndSuggestionsViewController: backButtonViewDelegate {

    func didbackButtonPressed() {
        print("Delegate fired")
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
}

extension ComplainsAndSuggestionsViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.white {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "rate text view placeholder".localized
            textView.textColor = UIColor.white
        }
    }
    
}
