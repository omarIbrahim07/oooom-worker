//
//  AboutAppViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/20/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

class AboutAppViewModel {
        
    var reloadStaticPage: (()->())?
    var updateLoadingStatus: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var aboutApp: StaticPage? {
        didSet {
            self.reloadStaticPage?()
        }
    }
    
    func initSafteyProcedures(lang: String) {
        state = .loading
        
        if lang == "ar" {
            getSafteyProcedures(pageID: 7)
        } else if lang == "en" {
            getSafteyProcedures(pageID: 8)
        }
    }
    
    private func getSafteyProcedures(pageID: Int) {
        
        let params: [String : AnyObject] = [
            "page_id" : pageID as AnyObject
        ]
        
        StaticAPIManager().getStaticPage(basicDictionary: params, onSuccess: { (staticPage) in
            self.aboutApp = staticPage
            self.state = .populated

        }) { (error) in
            self.state = .error
        }
    }
    
}
