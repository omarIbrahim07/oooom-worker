//
//  AboutAppViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/21/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class AboutAppViewController: BaseViewController {
    
    lazy var viewModel: AboutAppViewModel = {
        return AboutAppViewModel()
    }()
    
    var reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View

    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var aboutAppLabel: UILabel!
    @IBOutlet weak var egyDesignerLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        
        viewModel.updateLoadingStatus = { [weak self] () in
                    guard let self = self else {
                        return
                    }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
//                    self.stopLoadingWithError(error: <#T##APIError#>)
                    self.stopLoadingWithSuccess()
                case .empty:
                    print("")
                }
            }
        }
                                                
        viewModel.reloadStaticPage = { [weak self] () in
                DispatchQueue.main.async {
                    if let saftey = self?.viewModel.aboutApp {
                        self!.setData(staticPage: saftey)
                }
            }
        }
        
        self.viewModel.initSafteyProcedures(lang: "Lang".localized)

    }
    
    func setData(staticPage: StaticPage) {
        aboutAppLabel.text = staticPage.content
    }
    
    func configureView() {
                self.navView.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
                let reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
                reusableView.frame = self.navView.bounds
        reusableView.titleLabel.text = "about app".localized
                reusableView.titleImage.image = UIImage(named: "15579")
                reusableView.backButtonDelegate = self
                self.navView.addSubview(reusableView)
        egyDesignerLabel.text = "Copyright © OOOOM \nwww.oooomapp.com All Rights Reserved \nDesigned and Developed by egydesigner"
    }


}

extension AboutAppViewController: backButtonViewDelegate {

    func didbackButtonPressed() {
        print("Delegate fired")
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
}
