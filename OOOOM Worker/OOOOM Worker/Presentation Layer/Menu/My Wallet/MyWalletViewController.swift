//
//  MyWalletViewController.swift
//  OOOOM Worker
//
//  Created by Omar Ibrahim on 1/19/20.
//  Copyright © 2020 Egy Designer. All rights reserved.
//

import UIKit

class MyWalletViewController: BaseViewController {
    
    lazy var viewModel: MyWalletViewModel = {
        return MyWalletViewModel()
    }()

    var error: APIError?
    var user: User?
    
    var reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
    
    @IBOutlet weak var youhaveLabel: UILabel!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var walletView: UIView!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configure()
        configureView()
                
        initVM()
                
        if let user = user {
            bindAccountData(user: user)
        }
    }
    
    // MARK:- Bind User Data
    func bindAccountData(user: User) {
        print(user)
                
        if let balance: Int = user.balance {
            balanceLabel.text = String(balance)
            if balance >= 0 {
                balanceLabel.textColor = .green
            } else if balance < 0 {
                balanceLabel.textColor = .red
            }
        }
    }

    
    func configure() {
        youhaveLabel.text = "you have".localized
        currencyLabel.text = "currency".localized
    }

    func configureView() {
        self.navView.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        let reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
        reusableView.frame = self.navView.bounds
        reusableView.titleLabel.text = "my wallet".localized
        reusableView.titleImage.image = UIImage(named: "percentage")
        reusableView.backButtonDelegate = self
        self.navView.addSubview(reusableView)
        walletView.addCornerRadius(raduis: walletView.frame.height / 2, borderColor: #colorLiteral(red: 0.2901960784, green: 0.6745098039, blue: 0.9137254902, alpha: 1), borderWidth: 5.0)
    }
    
    // MARK:- Init View Model
    func initVM() {
        self.user = viewModel.initAccountData()
    }

}

extension MyWalletViewController: backButtonViewDelegate {

    func didbackButtonPressed() {
        print("Delegate fired")
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
}
