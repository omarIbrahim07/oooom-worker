//
//  MyWalletViewModel.swift
//  OOOOM Worker
//
//  Created by Omar Ibrahim on 1/19/20.
//  Copyright © 2020 Egy Designer. All rights reserved.
//

import Foundation

class MyWalletViewModel {
                    
    func initAccountData() -> User {
        if let user = UserDefaultManager.shared.currentUser {
            print(user)
            return user
        }
        return User()
    }
                        
}
