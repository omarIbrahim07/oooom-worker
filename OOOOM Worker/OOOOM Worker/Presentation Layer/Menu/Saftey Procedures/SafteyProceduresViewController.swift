//
//  SafteyProceduresViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/21/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class SafteyProceduresViewController: BaseViewController {
    

    lazy var viewModel: SafteyProceduresViewModel = {
        return SafteyProceduresViewModel()
    }()
    
    var reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
    
    @IBOutlet weak var logoView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
//                    self.stopLoadingWithError(error: <#T##APIError#>)
                    self.stopLoadingWithSuccess()
                case .empty:
                    print("")
                }
            }
        }
                                                
        viewModel.reloadStaticPage = { [weak self] () in
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
            }
        }
        
        self.viewModel.initSafteyProcedures(lang: "Lang".localized)
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "TermsAndConditionTableViewCell", bundle: nil), forCellReuseIdentifier: "TermsAndConditionTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
    }

    func configureView() {
                self.logoView.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
                let reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
                reusableView.frame = self.logoView.bounds
        reusableView.titleLabel.text = "saftey procedures".localized
                reusableView.titleImage.image = UIImage(named: "15579")
                reusableView.backButtonDelegate = self
                self.logoView.addSubview(reusableView)
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SafteyProceduresViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: TermsAndConditionTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TermsAndConditionTableViewCell") as? TermsAndConditionTableViewCell {
            
            if let title: String = self.viewModel.termsAndConditions?.content {
                cell.setTermsAndConditions(title: title)
                return cell
            }
        }
        return UITableViewCell()
    }
}

//    var safteyProcedure: StaticPage?
//        
//    lazy var viewModel: SafteyProceduresViewModel = {
//        return SafteyProceduresViewModel()
//    }()
//
//    var reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
//
//    @IBOutlet weak var navView: UIView!
//    @IBOutlet weak var safteyProceduresLabel: UILabel!
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        // Do any additional setup after loading the view.
//        configureView()
//        
//        viewModel.updateLoadingStatus = { [weak self] () in
//            guard let self = self else {
//                return
//            }
//
//            DispatchQueue.main.async { [weak self] in
//                guard let self = self else {
//                    return
//                }
//                switch self.viewModel.state {
//                
//                case .loading:
//                    self.startLoading()
//                case .populated:
//                    self.stopLoadingWithSuccess()
//                case .error:
////                    self.stopLoadingWithError(error: <#T##APIError#>)
//                    self.stopLoadingWithSuccess()
//                case .empty:
//                    print("")
//                }
//            }
//        }
//                                                
//        viewModel.reloadStaticPage = { [weak self] () in
//                DispatchQueue.main.async {
//                    if let saftey = self?.viewModel.safteyProcedure {
//                        self!.setData(staticPage: saftey)
//                }
//            }
//        }
//        
//        self.viewModel.initSafteyProcedures(lang: "Lang".localized)
//    }
//    
//    func setData(staticPage: StaticPage) {
//        safteyProceduresLabel.text = staticPage.content
//    }
//    
//    func configureView() {
//        self.navView.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
//        let reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
//        reusableView.frame = self.navView.bounds
//        reusableView.titleLabel.text = "saftey procedures".localized
//        reusableView.titleImage.image = UIImage(named: "report-symbol")
//        reusableView.backButtonDelegate = self
//        self.navView.addSubview(reusableView)
//    }
//
//}
//
extension SafteyProceduresViewController: backButtonViewDelegate {

    func didbackButtonPressed() {
        print("Delegate fired")
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
}
