//
//  BlockListTableViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 5/3/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class BlockListTableViewCell: UITableViewCell {
    
    var blockCellViewModel: BlockListCellViewModel? {
        didSet {

            if let name: String = blockCellViewModel?.name {
                self.workerNameLabel.text = name
            }
            
//            if let createdAt: String = mentainanceOrderCellViewModel?.createdAt {
//                mentainanceOrderTimeLabel.text = createdAt
//            }
            
            if let toId: Int = blockCellViewModel?.toId {
                removeBlockListButton.tag = toId
            }
        }
    }
    
    var didPressOnButton: ( (Int) -> Void )?

    @IBOutlet weak var View: UIView!
    @IBOutlet weak var workerNameLabel: UILabel!
    @IBOutlet weak var removeBlockLabel: UILabel!
    @IBOutlet weak var blockListImage: UIImageView!
    @IBOutlet weak var removeBlockListButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureCell()
        configure()
    }
    
    func configureCell() {
        View.layer.cornerRadius = 8.0
        blockListImage.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func configure() {
        removeBlockLabel.text = "remove block button".localized
    }
    
    @IBAction func removeBlockButtonPressed(_ sender: Any) {
        print("Details is pressed")
        if let selectedButton = (sender as AnyObject).tag {
            print((sender as AnyObject).tag)
            //            switch selectedButton {
            //            case .plus:
            //                count += 1
            //            case .minus:
            //                count -= 1
            //            }
            didPressOnButton?(selectedButton)
        }

    }
    
}
