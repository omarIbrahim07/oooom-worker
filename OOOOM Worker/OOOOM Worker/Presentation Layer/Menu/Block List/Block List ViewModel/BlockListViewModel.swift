//
//  BlockListViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 5/3/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class BlockListViewModel {

    private var blockList: [BlockList] = [BlockList]()
    
    var selectedBlockedUser: BlockList?
        
    enum noTechs {
        case notech
        case techs
    }
    
    var blockListCellViewModels: [BlockListCellViewModel] = [BlockListCellViewModel]() {
           didSet {
               self.reloadTableViewClosure?()
           }
       }
    
    var reloadTableViewClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var setNoTechnician: (()->())?
    var removeBlock: (()->())?
    
    var error: APIError?

    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
        
    var techsExistance: noTechs = .notech {
        didSet {
            self.setNoTechnician?()
        }
    }
    
    var isRemovedBlock: Bool = false {
        didSet {
            self.removeBlock?()
        }
    }
    
    var numberOfCells: Int {
         return blockListCellViewModels.count
     }

    func initFetch() {
        state = .loading
        
        let params: [String : AnyObject] = [:]
        
        BlockAPIManager().getBlockList(basicDictionary: params, onSuccess: { (blockList) in
            
            self.processFetchedPhoto(blockList: blockList)
            self.state = .populated

        }) { (error) in
            print(error)
        }

    }
    
    func removeBlock(toUserID: Int) {
        let params: [String : AnyObject] = [
            "to_id" : toUserID as AnyObject,
        ]

//        self.state = .loading
        BlockAPIManager().removeBlock(basicDictionary: params, onSuccess: { (saved) in
            
            if saved == true {
                self.isRemovedBlock = true
            } else if saved == false {
                
            }
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }

    
    func processFetchedPhoto( blockList: [BlockList] ) {
        self.blockList = blockList // Cache
        
        if self.blockList.count == 0 {
            self.techsExistance = .notech
        } else {
            self.techsExistance = .techs
        }
        
        var vms = [BlockListCellViewModel]()
        for blockList in blockList {
            vms.append( createCellViewModel(blockList: blockList) )
        }
        self.blockListCellViewModels = vms
    }
    
    func createCellViewModel( blockList: BlockList ) -> BlockListCellViewModel {
                    
        return BlockListCellViewModel(toId: blockList.toId, name: blockList.name, updatedAt: blockList.updatedAt)
    }


    func getCellViewModel( at indexPath: IndexPath ) -> BlockListCellViewModel {
        return blockListCellViewModels[indexPath.row]
    }

    func userPressed( at indexPath: IndexPath ) {
        let blockedUser = self.blockList[indexPath.row]

        self.selectedBlockedUser = blockedUser
    }
    
    func makeError(message: String) {
        let apiError = APIError()
        apiError.message = message
        self.error = apiError
        self.state = .error
    }

    func getError() -> APIError {
        return self.error!
    }

}
