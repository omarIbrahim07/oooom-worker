//
//  BlockListViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 5/3/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class BlockListViewController: BaseViewController {

    lazy var viewModel: BlockListViewModel = {
        return BlockListViewModel()
    }()

    var reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View

    @IBOutlet weak var noBlockListLabel: UILabel!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
        
        initVM()
    }
    
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
//                    self.stopLoadingWithError(error: <#T##APIError#>)
                    self.stopLoadingWithSuccess()
                case .empty:
                    print("")
                }
            }
        }

        viewModel.initFetch()
        self.noBlockListLabel.isHidden = true

            
//                if let service = choosedService {
//                    viewModel.initFetch(service: service, sorting: 1)
//                }
            

        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        viewModel.setNoTechnician = { [weak self] () in
           guard let self = self else {
               return
           }
           
           switch self.viewModel.techsExistance {
           
           case .notech:
               self.noBlockListLabel.isHidden = false
           case .techs:
               self.noBlockListLabel.isHidden = true
           }
        }
        
        viewModel.removeBlock = { [weak self] () in
            DispatchQueue.main.async {
                if self!.viewModel.isRemovedBlock == true {
                    self?.showUnblockPopUp()
                }
            }
        }

    }
    
    func showUnblockPopUp() {
        let alertController = UIAlertController(title: "unblock title popup".localized, message: "unblock message popup".localized, preferredStyle: .alert)
        let openAction = UIAlertAction(title: "unblock okay button".localized, style: .default) { (action) in
//            self.goToHomePage()
            self.viewModel.initFetch()
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //delay to time
    public func delay(_ delay: Double, closure: @escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
    public func mainQueue(_ closure: @escaping ()->()){
        DispatchQueue.main.async(execute: closure)
    }
    
    func configureView() {
        self.navView.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        let reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
        reusableView.frame = self.navView.bounds
        reusableView.titleLabel.text = "block list title".localized
        reusableView.titleImage.image = UIImage(named: "improvement")
        reusableView.backButtonDelegate = self
        self.navView.addSubview(reusableView)
        noBlockListLabel.text = "no block lists".localized
    }

    func configureTableView() {
        tableView.register(UINib(nibName: "BlockListTableViewCell", bundle: nil), forCellReuseIdentifier: "BlockListTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BlockListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfCells
//        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: BlockListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "BlockListTableViewCell") as? BlockListTableViewCell {
//            cell.orderDetailsButton.tag = indexPath.row
            
            cell.didPressOnButton = { f in
                print("Closure Activated")
//                self.goToOrderDetails(orderId: f)
                self.viewModel.removeBlock(toUserID: f)
            }
            
            let cellVM = viewModel.getCellViewModel(at: indexPath)
            cell.blockCellViewModel = cellVM
            
            return cell
        }
        return UITableViewCell()
    }
    
    
}

extension BlockListViewController: backButtonViewDelegate {
    
    func didbackButtonPressed() {
        print("Delegate fired")
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
}
