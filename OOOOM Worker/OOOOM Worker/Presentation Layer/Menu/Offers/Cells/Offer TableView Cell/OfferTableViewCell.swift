//
//  OfferTableViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 2/11/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import Cosmos

enum actionFires {
    case activate
    case special
}

enum actionToken {
    case notApproved
    case approvedWithSpecial
}

protocol checkActionsOnOfferTableViewCellDelegate {
    func checkActionsOnOfferButtonPressed(actionFires: actionFires, actionToken: actionToken)
}

class OfferTableViewCell: UITableViewCell {
    
    var delegate: checkActionsOnOfferTableViewCellDelegate?
        
    
    //MARK:- Delegate Helpers
    func checkActionsOnOfferButtonPressed(actionFires: actionFires, actionToken: actionToken) {
        if let delegateValue = delegate {
            delegateValue.checkActionsOnOfferButtonPressed(actionFires: actionFires, actionToken: actionToken)
        }
    }

    var didPressOnRemoveButton: ( (Int) -> Void )?
    var didPressOnActivateButton: ( (Int) -> Void )?
    var didPressOnSpecialButton: ( (Int) -> Void )?
    
    var offerCellViewModel: OfferCellViewModel? {
        didSet {
                        
            if let id: Int = offerCellViewModel?.id {
                specialButton.tag = id
                removeButton.tag = id
                specialCosmosView.tag = id
                activateSwitchButton.tag = id
            }
            
            if let isApproved = offerCellViewModel?.approved {
                if isApproved == 1 {
                    activateSwitchButton.isUserInteractionEnabled = true
                } else if isApproved == 0 {
                    activateSwitchButton.isUserInteractionEnabled = false
                }
            }

            
            if let isActivate = offerCellViewModel?.active {
                if isActivate == 1 {
                    activateSwitchButton.setOn(true, animated: true)
                } else if isActivate == 0 {
                    activateSwitchButton.setOn(false, animated: true)
                }
            }
            
            if let isSpecial = offerCellViewModel?.isSpecial {
                if isSpecial == 1 {
                    specialCosmosView.rating = 1
                    specialButton.isHidden = true
                } else if isSpecial == 0 {
                    specialCosmosView.rating = 0
                    specialButton.isHidden = false
                }
            }

            if let description: String = offerCellViewModel?.description {
                descriptionLabel.text = description
            }
                        
            if let image: String = offerCellViewModel?.firstImage {
                let url = URL(string: ImageURLOffers + image)
                offerImageView.kf.setImage(with: url)
            }
                        
            if "Lang".localized == "ar" {
                if let price: Int = offerCellViewModel?.price {
                    priceLabel.text = String(price) + " ريال"
                }
            } else if "Lang".localized == "en" {
                if let price: Int = offerCellViewModel?.price {
                    priceLabel.text = String(price) + " SAR"
                }
            }
        }
    }

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var offerContainerView: UIView!
    @IBOutlet weak var offerImageView: UIImageView!
    @IBOutlet weak var specialCosmosView: CosmosView!
    @IBOutlet weak var specialButton: UIButton!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var activateSwitchButton: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureCell()
        selectionStyle = .none
    }

    func configureCell() {
        offerContainerView.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
//        offerImageView.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        activateSwitchButton.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
    }
    
    @IBAction func specialButtonIsPressed(_ sender: Any) {
        if let selectedButton = (sender as AnyObject).tag {
            print((sender as AnyObject).tag)
            if self.offerCellViewModel?.approved == 0 {
                self.checkActionsOnOfferButtonPressed(actionFires: .special, actionToken: .notApproved)
            } else if self.offerCellViewModel?.approved == 1 {
                didPressOnSpecialButton?(selectedButton)
            }
        }
    }
    
    @IBAction func removeButtonIsPressed(_ sender: Any) {
        if let selectedButton = (sender as AnyObject).tag {
            print((sender as AnyObject).tag)
            didPressOnRemoveButton?(selectedButton)
        }
    }
    
    @IBAction func activateOfferButtonIsPressed(_ sender: Any) {
        if let selectedButton = (sender as AnyObject).tag {
            print((sender as AnyObject).tag)
            print("")
            if self.offerCellViewModel?.approved == 0 {
                self.checkActionsOnOfferButtonPressed(actionFires: .activate, actionToken: .notApproved)
            } else if self.offerCellViewModel?.approved == 1 {
                didPressOnActivateButton?(selectedButton)
            }
        }
    }
    
    
}
