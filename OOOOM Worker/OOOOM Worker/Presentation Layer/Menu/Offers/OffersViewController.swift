//
//  OffersViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/31/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class OffersViewController: BaseViewController {
    
    lazy var viewModel: OffersViewModel = {
        return OffersViewModel()
    }()
    
    enum ChoosedService {
        case menu
        case techs
    }
    
    var error: APIError?
    var choosedServiceEnum: ChoosedService?
    var serviceID: Int?
    var serviceNameOfTechnician: String?
    
    var window: UIWindow?
    var orderId: Int?
    
    var numberOfOffers: Int? = 10
    
    var services: [Service] = [Service]()
    var choosedService: Service?
    var permittedOrdersNumber: Int?
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addOfferButton: UIButton!
    
    var reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        configureView()
        navigationItem.title = "offers title".localized
        configureTableView()
        initVM()
    }
    
// MARK:- Init View Model
    func initVM() {
        self.viewModel.checkNumberOfOffersAvailableToAddOffer()
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.showError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
                                                            
        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
                if "Lang".localized == "en" {
                    if let offersCount: Int = self!.viewModel.numberOfCells {
//                        self!.offersCountLabel.text = "\(offersCount) offers available in section"
                    }
                } else if "Lang".localized == "ar" {
                    if let offersCount: Int = self!.viewModel.numberOfCells {
//                        self!.offersCountLabel.text = "متاح عدد \(offersCount) عروض في قسم"
                    }
                }
            }
        }
        
        viewModel.loadNumberOfOffersPermitAddOffer = { [weak self] () in
            DispatchQueue.main.async {
                self?.permittedOrdersNumber = self!.viewModel.getNumberOfOfferPermitAddOffer()
            }
        }
                
        self.viewModel.fetchOffers()
    }
    
    func checkLimitOfOrders() {
        if let permitedOrders = self.permittedOrdersNumber, let ordersCompleted = UserDefaultManager.shared.currentUser?.ordersCompletedCount {
            if ordersCompleted >= permitedOrders {
                print("Tmam")
            } else {
                print("Hntl3 alert")
                showAddOfferNotPermittedAlert(numberOfOrders: permitedOrders)
            }
        }
    }

    func bindService(service: Service) {
        if "Lang".localized == "ar" {
//            self.serviceLabel.text = service.name
        } else if "Lang".localized == "en" {
//            self.serviceLabel.text = service.nameEN
        }
    }
    
    func configureView() {
//        self.navView.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
//        let reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
//        reusableView.frame = self.navView.bounds
//        reusableView.titleLabel.text = "offers title".localized
//        reusableView.titleImage.image = UIImage(named: "offer")
//        reusableView.backButtonDelegate = self
//        self.navView.addSubview(reusableView)
        self.addOfferButton.addCornerRadius(raduis: addOfferButton.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "OfferTableViewCell", bundle: nil), forCellReuseIdentifier: "OfferTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func showActivationAlert() {
        let alertController = UIAlertController(title: "not approved offer is activated alert title".localized, message: "not approved offer is activated alert message".localized, preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let cancelAction = UIAlertAction(title: "not approved cancel".localized, style: .cancel)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAddOfferNotPermittedAlert(numberOfOrders: Int) {
        let alertController = UIAlertController(title: "offers permission alert title".localized, message: "offers permission not allowed alert message".localized + String(numberOfOrders), preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let cancelAction = UIAlertAction(title: "offers permission cancel".localized, style: .cancel)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK:- Actions
    @IBAction func addOfferButtonIsPressed(_ sender: Any) {
        print("Add offer is pressed")
        checkLimitOfOrders()
        self.goToAddOffer()
    }
    
    
    
// MARK:- Navigation
    func goToOfferDetails(offerID: Int) {
        let storyboard = UIStoryboard.init(name: "Menu", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "OfferDetailsViewController") as! OfferDetailsViewController
        viewController.offerID = offerID
        navigationController?.pushViewController(viewController, animated: true)

//        if let offerDetailsVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "OfferDetailsViewController") as? OfferDetailsViewController {
//            offerDetailsVC.offerID = offerID
//            //                rootViewContoller.test = "test String"
//            self.present(offerDetailsVC, animated: true, completion: nil)
//            print("Offer Details")
//        }
    }
    
    func goToAddOffer() {
        let storyboard = UIStoryboard.init(name: "Menu", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "AddOfferViewController") as! AddOfferViewController
        //        viewController.postID = postID
        navigationController?.pushViewController(viewController, animated: true)

//        if let offerDetailsVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "AddOfferViewController") as? AddOfferViewController {
////            offerDetailsVC.offerID = offerID
//            self.present(offerDetailsVC, animated: true, completion: nil)
//            print("Add Offer")
//        }
    }
    
    func goToChat2(orderId: Int) {
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        if let viewController = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController {
//            viewController.orderId = orderId
//            window!.rootViewController = viewController
//            window!.makeKeyAndVisible()
//        }
    }
    
    func goToChat(orderId: Int) {
        print("RRRRRRRRRRRRRRRR")
        if let editProfileVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController {
            editProfileVC.orderId = orderId
            let navigationController = UINavigationController(rootViewController: editProfileVC)
            //                rootViewContoller.test = "test String"
            navigationController.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: true, completion: nil)
        }
        
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        let viewController = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
//        //        viewController.postID = postID
//        navigationController?.pushViewController(viewController, animated: true)

    }
}

extension OffersViewController: checkActionsOnOfferTableViewCellDelegate {
    func checkActionsOnOfferButtonPressed(actionFires: actionFires, actionToken: actionToken) {
        if actionFires == .activate {
            print("Not approved not actiovate")
            self.showActivationAlert()
//            if self.viewModel.offers[id].approved == 1 {
//                if self.viewModel.offers[id].active == 1 {
//                    print("Is active before")
//                } else if self.viewModel.offers[id].active == 0 {
//                    print("Isn't active")
//                }
//            } else if self.viewModel.offers[id].approved == 0 {
//                if self.viewModel.offers[id].active == 1 {
//                    print("Isn't approved Is active before")
//                } else if self.viewModel.offers[id].active == 0 {
//                    print("Isn't approved Isn't active")
//                }
//            }
        } else if actionFires == .special {
            print("Not approved no special")
        }
    }
}

// MARK:- UITableView Delegate
extension OffersViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfCells
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell: OfferTableViewCell = tableView.dequeueReusableCell(withIdentifier: "OfferTableViewCell") as? OfferTableViewCell {
                
            let cellVM = viewModel.getCellViewModel(at: indexPath)
            cell.offerCellViewModel = cellVM
            
            cell.delegate = self
            
            cell.didPressOnRemoveButton = { offerID in
                print(offerID)
                self.viewModel.deleteOffer(offerID: offerID)
            }
            
            cell.didPressOnSpecialButton = { offerID in
                print(offerID)
                self.viewModel.makeOfferSpecial(offerID: offerID)
            }

            cell.didPressOnActivateButton = { offerID in
                print(offerID)
                self.viewModel.activateOffer(offerID: offerID)
            }
                        
            return cell
        }
                
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel.userPressed(at: indexPath)
        
        if let selectedOffer = self.viewModel.selectedOffer {
            if let offerID = selectedOffer.id {
                goToOfferDetails(offerID: offerID)
            }
        }
                
        tableView.deselectRow(at: indexPath, animated: true)

    }
    
    
}

// MARK:- Back Button Delegate
extension OffersViewController: backButtonViewDelegate {
    
    func didbackButtonPressed() {
        print("Delegate fired")
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
}
