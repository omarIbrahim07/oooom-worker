//
//  OffersViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 2/13/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class OffersViewModel {
    
    var services: [Service]? {
        didSet {
            self.reloadServicesClosure?()
        }
    }
    
    var numberOfOrdersPermitAddOffer: Int? {
        didSet {
            self.loadNumberOfOffersPermitAddOffer?()
        }
    }
    
    enum ChoosedService {
        case menu
        case techs
    }
    
    var choosedServiceEnum: ChoosedService?
    
    var choosedService: Service? {
        didSet {
            self.chooseServiceClosure?()
        }
    }
    
    var offers: [Offer] = [Offer]()

    var selectedOffer: Offer? {
        didSet {
            self.updateSelectedOffer?()
        }
    }
    var error: APIError?

    enum noTechs {
        case notech
        case techs
    }

    var offerCellViewModels: [OfferCellViewModel] = [OfferCellViewModel]() {
        didSet {
            self.reloadTableViewClosure?()
        }
    }

    var reloadTableViewClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var updateSelectedOffer: (()->())?
    var setNoTechnician: (()->())?
    var reloadServicesClosure: (()->())?
    var chooseServiceClosure: (()->())?
    var loadNumberOfOffersPermitAddOffer: (()->())?

    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }

    var techsExistance: noTechs = .notech {
        didSet {
            self.setNoTechnician?()
        }
    }

    var numberOfCells: Int {
         return offerCellViewModels.count
     }
        
    func fetchOffers() {
        state = .loading
        
        let params: [String : AnyObject] = [:]
        
        OffersAPIManager().getOffers(basicDictionary: params, onSuccess: { (offers) in

        self.processFetchedPhoto(offers: offers)
        self.state = .populated

//        self.updateFirstService()
            
        self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func deleteOffer(offerID: Int) {
        state = .loading
        
        let params: [String : AnyObject] = ["offer_id" : offerID as AnyObject]
        
        OffersAPIManager().deleteOffer(basicDictionary: params, onSuccess: { (message) in

        if message != nil {
            print("deleted")
            self.fetchOffers()
        }
                        
        self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func makeOfferSpecial(offerID: Int) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "offer_id" : offerID as AnyObject,
            "special_days" : 7 as AnyObject
        ]
        
        OffersAPIManager().makeOfferSpecial(basicDictionary: params, onSuccess: { (message) in

        if message != nil {
            print("Special")
            self.fetchOffers()
        }
                        
        self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func checkNumberOfOffersAvailableToAddOffer() {
        state = .loading
                
        let params: [String : AnyObject] = [
            "setting_id" : 11 as AnyObject
        ]
        
        OffersAPIManager().getSpecialOfferPrice(basicDictionary: params, onSuccess: { (val) in

            if let value: String = val {
                self.numberOfOrdersPermitAddOffer = Int(value) ?? 1
            }
                                    
        self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }


    func activateOffer(offerID: Int) {
        state = .loading
        
        let params: [String : AnyObject] = ["offer_id" : offerID as AnyObject]
        
        OffersAPIManager().activeOffer(basicDictionary: params, onSuccess: { (active) in

        if active != nil {
            print("deleted")
            self.fetchOffers()
        }
                        
        self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }

    func getServices() -> [Service] {
        return services!
    }

    func getService(service: Service) {
        self.choosedService = service
    }

    func processFetchedPhoto( offers: [Offer] ) {
        self.offers = offers // Cache

        if self.offers.count == 0 {
            self.techsExistance = .notech
        } else {
            self.techsExistance = .techs
        }

        var vms = [OfferCellViewModel]()
        for offer in offers {
            vms.append( createCellViewModel(offer: offer) )
        }
        self.offerCellViewModels = vms
    }

    func createCellViewModel( offer: Offer ) -> OfferCellViewModel {

        return OfferCellViewModel(id: offer.id, price: offer.price, expireDate: offer.expireDate, workerID: offer.workerId, title: offer.title, description: offer.description, serviceId: offer.serviceId, firstImage: offer.firstImage, isSpecial: offer.isSpecial, approved: offer.approved, active: offer.active, hide: offer.hide)
    }


    func getCellViewModel( at indexPath: IndexPath ) -> OfferCellViewModel {
        return offerCellViewModels[indexPath.row]
    }
    
    func getOffer() -> Offer {
        return selectedOffer!
    }

    func getNumberOfOfferPermitAddOffer() -> Int {
        return numberOfOrdersPermitAddOffer!
    }
    
    func userPressed( at indexPath: IndexPath ) {
        let offer = self.offers[indexPath.row]

        self.selectedOffer = offer
    }
    
    func getError() -> APIError {
        return self.error!
    }

}
