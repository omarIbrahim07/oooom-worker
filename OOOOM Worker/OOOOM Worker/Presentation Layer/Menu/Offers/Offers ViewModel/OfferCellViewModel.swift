//
//  OfferCellViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 2/13/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct OfferCellViewModel {
    var id: Int?
    var price: Int?
    var expireDate: String?
    var workerID: Int?
    var title: String?
    var description: String?
    var serviceId: Int?
    var firstImage: String?
    var isSpecial: Int?
    var approved: Int?
    var active: Int?
    var hide: Int?
}
