//
//  ImageZoomedViewController.swift
//  OOOOM Worker
//
//  Created by Omar Ibrahim on 3/16/20.
//  Copyright © 2020 Egy Designer. All rights reserved.
//

import UIKit

class ImageZoomedViewController: BaseViewController {
    
    var imageURL: String?
    @IBOutlet weak var image: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if let imageURL = self.imageURL {
            image.loadImageFromUrl(imageUrl: imageURL)
        }
    }
    
}
