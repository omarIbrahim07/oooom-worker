//
//  MyAccountViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/21/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import MOLH
import AssetsPickerViewController
import Toast_Swift
import PKHUD
import Photos

class MyAccountViewController: BaseViewController {
    
    lazy var viewModel: AccountViewModel = {
       return AccountViewModel()
    }()
    
    var user: User?
    var myImage: UIImage? = UIImage()

    var reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View

    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userPhoneLabel: UILabel!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var updateProfileButton: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var langLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configure()
                
        user = viewModel.initAccountData()
        
        if let user = user {
            bindAccountData(user: user)
        }
    }
    
    func imagePressed() {
        
        let image = UIImagePickerController()
        
        image.delegate = self
        
        image.sourceType = .photoLibrary
        
        self.present(image, animated: true) {
            
        }
    }
            
    func edit() {
        if myImage?.jpegData(compressionQuality: 0.3) != nil {
            
        let imgData = myImage!.jpegData(compressionQuality: 0.3)
            
            let parameters: [String : AnyObject] = [:]
            
            startLoading()
            
            AuthenticationAPIManager().changeUserProfileWithImage(imageData: imgData!, basicDictionary: parameters, onSuccess: { (message) in
                
                print(message)
                self.getUserProfile()
                                
                
            }) { (error) in
                self.stopLoadingWithError(error: error)
            }
        }
    }
    
    func getUserProfile() {
        weak var weakSelf = self
        
        AuthenticationAPIManager().getUserProfile(onSuccess: { (_) in
            
            weakSelf?.stopLoadingWithSuccess()
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
    }

    
    
    func bindAccountData(user: User) {
        if let firstName: String = user.firstName, let lastName: String = user.lastName {
            userNameLabel.text = firstName + " " + lastName
        }
        if let phone = user.phone {
            userPhoneLabel.text = phone
        }
        if let userImagee = user.image {
            userImage.loadImageFromUrl(imageUrl: ImageURL_USERS + userImagee)
        }
    }
    
    func configure() {
        updateProfileButton.setTitle("edit profile button".localized, for: .normal)
        userImage.addCornerRadius(raduis: userImage.frame.height / 2, borderColor: #colorLiteral(red: 0.8745098039, green: 0.9215686275, blue: 0.9647058824, alpha: 1), borderWidth: 1)
    }

    func configureView() {
        self.navView.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        bottomView.roundCorners([.topLeft, .topRight], radius: 30.0)
        updateProfileButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        let reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
        reusableView.frame = self.navView.bounds
        reusableView.titleLabel.text = "my account".localized
        reusableView.titleImage.image = UIImage(named: "man-user")
        reusableView.backButtonDelegate = self
        self.navView.addSubview(reusableView)
        languageLabel.text = "Language".localized
        langLabel.text = "language label".localized
    }
    
    func goToEditProfile() {
        if let editProfileVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "EditProfileViewController") as? EditProfileViewController {
            //                rootViewContoller.test = "test String"
            self.present(editProfileVC, animated: true, completion: nil)
            print("My Account")
        }
    }
    
    @IBAction func changeUserImagePressed(_ sender: Any) {
        print("Change image pressed")
        imagePressed()
    }
    
    
    @IBAction func saveImageIsPressed(_ sender: Any) {
        edit()
    }
    
    @IBAction func editProfileButtonPressed(_ sender: Any) {
        print("Edit profile pressed")
        goToEditProfile()
    }
    
    @IBAction func languageIsPressed(_ sender: UISwitch) {
        MOLH.setLanguageTo(MOLHLanguage.currentAppleLanguage() == "en" ? "ar" : "en")
        MOLH.reset()
        languageLabel.text = "Language".localized
        if sender.isOn {
            print("English")
        } else {
            print("Arabic")
        }
    }
    
    
}

extension MyAccountViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            userImage.image = image
            userImage.addCornerRadius(raduis: userImage.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 1)
            myImage = image
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension MyAccountViewController: backButtonViewDelegate {

    func didbackButtonPressed() {
        print("Delegate fired")
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
}
