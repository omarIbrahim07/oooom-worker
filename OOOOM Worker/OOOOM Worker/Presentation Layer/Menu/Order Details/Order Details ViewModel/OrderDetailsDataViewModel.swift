//
//  OrderDetailsDataViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/24/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

struct OrderDetailsDataViewModel {
    var state: String?
    var name: String?
    var city: String?
    var cityEn: String?
    var distance: Int?
    var date: String?
    var service: String?
    var serviceEn: String?
    var time: String?
    var amountAgreed: Int?
    var longitude: String?
    var latitude: String?
    var rate: Double?
}
