//
//  OrderDetailsViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/24/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

class OrderDetailsViewModel {
    
    var orderDetails: OrderSent? {
        didSet {
            self.updateOrderDetails?()
        }
    }
    
    var error: APIError?
    
    enum OrderState: Int {
        case firstTime = 0
        case waitingForWorker = 1
        case waitingForPreview
        case waitingForClientConfirmation
        case hapenning
        case complete
        case refused
        case canceled
    }
    
    var updateOrderDetails: (()->())?
    var updateOrderState: (()->())?
    var updateLoadingStatus: (()->())?
    var reloadTableViewClosure: (()->())?
    
    // callback for interfaces
    var orderStatusID: OrderState?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var stackViewModel: OrderDetailsStackViewModel? {
        didSet {
            self.updateOrderState?()
        }
    }
    
    var cellViewModels: OrderDetailsDataViewModel? {
        didSet {
            self.reloadTableViewClosure?()
        }
    }

    func chooseStatus(orderStatusID: Int) -> String {
        if orderStatusID == 1 {
            return "State1".localized
        } else if orderStatusID == 2 {
            return "State2".localized
        } else if orderStatusID == 3 {
            return "State3".localized
        } else if orderStatusID == 4 {
            return "State4".localized
        } else if orderStatusID == 5 {
            return "State5".localized
        } else if orderStatusID == 6 {
            return "State6".localized
        } else if orderStatusID == 7 {
            return "State7".localized
        }
        return "Not found"
    }
    
    func initFetch(orderID: Int) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "order_id" : orderID as AnyObject,
        ]
        OrderAPIManager().getOrderDetails(basicDictionary: params, onSuccess: { (orderDetails) in
            self.orderDetails = orderDetails

//            self.processFetchedPhoto(technicians: technicians)
            print(orderDetails)

            if let orderStatusId = orderDetails.orderStatusId {
                self.stackViewModel = self.checkState(orderStatusID: orderStatusId)
            }

            self.cellViewModels = self.processTechnicianDetails(orderDetails: orderDetails)

            self.state = .populated

        }) { (error) in
            print(error)
        }
    }
    
    func submitAction(orderID: Int, orderStatusID: Int) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "order_id" : orderID as AnyObject,
            "orderStatus_id" : orderStatusID as AnyObject
        ]
        OrderAPIManager().submitActionOnOrder(basicDictionary: params, onSuccess: { (orderID) in
//            self.processFetchedPhoto(technicians: technicians)
            print(orderID)
                                    
            self.state = .populated
            self.initFetch(orderID: orderID)

        }) { (error) in
            print(error)
        }
    }
    
    func acceptOrder(orderID: Int, orderStatusID: Int, price: Int) {
        state = .loading
        
        let params: [String : AnyObject] = [
            "order_id" : orderID as AnyObject,
            "orderStatus_id" : orderStatusID as AnyObject,
            "price" : price as AnyObject,
        ]
        OrderAPIManager().submitActionOnOrder(basicDictionary: params, onSuccess: { (orderID) in
//            self.processFetchedPhoto(technicians: technicians)
            print(orderID)
                                    
            self.state = .populated
            self.initFetch(orderID: orderID)

        }) { (error) in
            print(error)
        }
    }

    
    func processTechnicianDetails(orderDetails: OrderSent) -> OrderDetailsDataViewModel {
        var statee: String?
        if let orderStatusID: Int = orderDetails.orderStatusId {
            statee = chooseStatus(orderStatusID: orderStatusID)
        }
        
        if let state: String = statee {
            return OrderDetailsDataViewModel(state: state, name: orderDetails.workerName, city: orderDetails.area, cityEn: orderDetails.areaEN, distance: Int(orderDetails.distance ?? 0), date: orderDetails.startDate, service: orderDetails.service, serviceEn: orderDetails.serviceEN, time: orderDetails.timestamp2, amountAgreed: orderDetails.price, rate: orderDetails.rating)
        }
        return OrderDetailsDataViewModel(state: "not found", name: orderDetails.workerName!, city: orderDetails.area!, cityEn: orderDetails.areaEN!, distance: Int(orderDetails.distance!), date: orderDetails.startDate!, service: orderDetails.service!, serviceEn: orderDetails.serviceEN!, time: orderDetails.timestamp2!, amountAgreed: orderDetails.price!)
    }

    func checkState(orderStatusID: Int) -> OrderDetailsStackViewModel {
        if orderStatusID == 1 {
            return OrderDetailsStackViewModel(state: true, locationButton: true, rate: false, name: true, city: true, distance: true, date: true, service: true, time: true, amountAgreed: false, accept: true, refuse: true, confirm: false, cancel: false)
        } else if orderStatusID == 2 {
            return OrderDetailsStackViewModel(state: true, locationButton: true, rate: false, name: true, city: true, distance: true, date: true, service: true, time: true, amountAgreed: false, accept: false, refuse: false, confirm: true, cancel: true)
        } else if orderStatusID == 3 {
            return OrderDetailsStackViewModel(state: true, locationButton: true, rate: false, name: true, city: true, distance: true, date: true, service: true, time: true, amountAgreed: true, accept: false, refuse: false, confirm: false, cancel: false)
        } else if orderStatusID == 4 {
            return OrderDetailsStackViewModel(state: true, locationButton: true, rate: false, name: true, city: true, distance: true, date: true, service: true, time: true, amountAgreed: true, accept: false, refuse: false, confirm: false, cancel: false)
        } else if orderStatusID == 5 {
            return OrderDetailsStackViewModel(state: true, locationButton: false, rate: true, name: true, city: true, distance: true, date: true, service: true, time: true, amountAgreed: true, accept: false, refuse: false, confirm: false, cancel: false)
        } else if orderStatusID == 6 {
            return OrderDetailsStackViewModel(state: true, locationButton: false, rate: false, name: true, city: true, distance: true, date: true, service: true, time: true, amountAgreed: false, accept: false, refuse: false, confirm: false, cancel: false)
        } else if orderStatusID == 7 {
            return OrderDetailsStackViewModel(state: true, locationButton: false, rate: false, name: true, city: true, distance: true, date: true, service: true, time: true, amountAgreed: false, accept: false, refuse: false, confirm: false, cancel: false)
        }
        return OrderDetailsStackViewModel(state: true, locationButton: true, rate: false, name: true, city: true, distance: true, date: true, service: true, time: true, amountAgreed: false, accept: false, refuse: false, confirm: false, cancel: false)
    }
    
    func getError() -> APIError {
        return self.error!
    }
        
    func getStackViewModel() -> OrderDetailsStackViewModel {
        return self.stackViewModel!
    }

    func getDataStackViewModel() -> OrderDetailsDataViewModel {
        return self.cellViewModels!
    }
    
    
}
