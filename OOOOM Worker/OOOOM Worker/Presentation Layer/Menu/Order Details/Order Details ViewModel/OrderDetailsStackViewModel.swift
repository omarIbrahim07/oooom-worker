//
//  OrderDetailsStackViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/24/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

struct OrderDetailsStackViewModel {
    let state: Bool
    let locationButton: Bool
    let rate: Bool
    let name: Bool
    let city: Bool
    let distance: Bool
    let date: Bool
    let service: Bool
    let time: Bool
    let amountAgreed: Bool
    let accept: Bool
    let refuse: Bool
    let confirm: Bool
    let cancel: Bool
}
