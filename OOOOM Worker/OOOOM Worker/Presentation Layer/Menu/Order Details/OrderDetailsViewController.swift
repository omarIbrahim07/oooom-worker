//
//  OrderDetailsViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/30/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import Cosmos
import MapKit

class OrderDetailsViewController: BaseViewController {
        
    lazy var viewModel: OrderDetailsViewModel = {
        return OrderDetailsViewModel()
    }()
    
    var orderSent: OrderSent?
    var stack: OrderDetailsStackViewModel?
    var stack2: OrderDetailsDataViewModel?
    
    var currentDate: Date?
    
    var orderId: Int?
    var error: APIError?
    
    var longitude: String?
    var latitude: String?
    
    var reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
    
    @IBOutlet weak var stateView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var clientNameLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var cityValueLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var distanceValueLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var reservationDateLabel: UILabel!
    @IBOutlet weak var serviceLabel: UILabel!
    @IBOutlet weak var serviceValueLabel: UILabel!
    @IBOutlet weak var amountAgreedLabel: UILabel!
    @IBOutlet weak var amountAgreedValueLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var reservationTimeLabel: UILabel!
    @IBOutlet weak var stateStackView: UIStackView!
    @IBOutlet weak var nameStackView: UIStackView!
    @IBOutlet weak var dateStackView: UIStackView!
    @IBOutlet weak var cityStackView: UIStackView!
    @IBOutlet weak var distanceStackView: UIStackView!
    @IBOutlet weak var serviceStackView: UIStackView!
    @IBOutlet weak var timeStackView: UIStackView!
    @IBOutlet weak var agreeRefuseStackView: UIStackView!
    @IBOutlet weak var confirmationStackView: UIStackView!
    @IBOutlet weak var amountAgreedStackView: UIStackView!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var refuseButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var rateCosmosView: CosmosView!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var rateButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        configureView()
        configure()
        navigationItem.title = "order details title".localized
        
        initVM()
        
        if let orderId = self.orderId {
            viewModel.initFetch(orderID: orderId)
        }
    }
    
    // MARK: Change String into Date
    func getDate(date: String) -> Date {

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.timeZone = TimeZone.current
        let date = dateFormatter.date(from:date)!
        print(date)

        return date
    }
    
    // MARK:- Calculate differnce between two dates in hours
    func calculateDifference(from: Date, to: Date) -> Int {
        var cal = Calendar.current
        cal.timeZone = NSTimeZone.local
        let components = cal.dateComponents([.hour], from: from, to: to)
        let diff = components.hour!
        print(diff)
        return diff
    }
    
    // MARK:- Get Current Time
    func getCurrentTime() -> String {
        
        let currentDate = Date()
         
        // 1) Create a DateFormatter() object.
        let format = DateFormatter()
         
        // 2) Set the current timezone to .current
        format.timeZone = .current
         
        // 3) Set the format of the altered date.
        format.dateFormat = "yyyy-MM-dd HH:mm:ss"
         
        // 4) Set the current date, altered by timezone.
        let dateString = format.string(from: currentDate)
        print(dateString)

        return dateString
    }

    func initVM() {
        
        viewModel.updateOrderState = { [weak self] () in
            DispatchQueue.main.async {
                self?.stack = self!.viewModel.getStackViewModel()
                if let stack = self?.stack {
                    self!.bindStackView(stackView: stack)
                }
            }
        }
        
        viewModel.updateOrderDetails = { [weak self] () in
            DispatchQueue.main.async {
                self?.orderSent = self!.viewModel.orderDetails
            }
        }
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.showError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }

        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.stack2 = self!.viewModel.getDataStackViewModel()
                    if let stack2 = self?.stack2 {
                    self!.bindDataStackView(stackView: stack2)
                }
            }
        }
        
    }
    
    func bindDataStackView(stackView: OrderDetailsDataViewModel) {
        print("Bind")
        print(stackView)
        if let name: String = stackView.name {
            clientNameLabel.text = name
        }
        
        if "Lang".localized == "en" {
            if let city: String = stackView.cityEn {
                cityValueLabel.text = city
            }
        } else if "Lang".localized == "ar" {
            if let city: String = stackView.city {
                cityValueLabel.text = city
            }
        }
        
        if let lat = stackView.latitude {
            print(lat)
            self.latitude = lat
        }
        
        if let long = stackView.longitude {
            print(long)
            self.longitude = long
        }
        
        if let distance: Int = stackView.distance {
            distanceValueLabel.text = String(distance)
        }
        
        if let date: String = stackView.date {
//            let dateNew = date.substring(to: 10)
            var updateDate = date.substring(with: 0..<10)
            let year: String = updateDate.substring(with: 0..<4)
            let month: String = updateDate.substring(with: 5..<7)
            let day: String = updateDate.substring(with: 8..<10)
            updateDate = day + "-" + month + "-" + year
            reservationDateLabel.text = updateDate
        }
        
        if "Lang".localized == "en" {
            if let service: String = stackView.serviceEn {
                serviceValueLabel.text = service
            }
        } else if "Lang".localized == "ar" {
            if let service: String = stackView.service {
                serviceValueLabel.text = service
            }
        }
        
        if let time: String = stackView.time {
            let timeNew = time.substring(from: 11)
            reservationTimeLabel.text = timeNew
        }
        
        if let state: String = stackView.state {
            stateLabel.text = state
        }
        
        if let price: Int = stackView.amountAgreed {
            amountAgreedValueLabel.text = String(price)
        }
        
        if let rate: Double = stackView.rate {
            rateCosmosView.isHidden = true
        }
    }
    
    func bindStackView(stackView: OrderDetailsStackViewModel) {
        
        stateStackView.isHidden = false
        nameStackView.isHidden = false
        dateStackView.isHidden = false
        cityStackView.isHidden = false
        distanceStackView.isHidden = false
        serviceStackView.isHidden = false
        timeStackView.isHidden = false
        agreeRefuseStackView.isHidden = false
        confirmationStackView.isHidden = false
        amountAgreedStackView.isHidden = false

        if stackView.state == true {
            stateLabel.isHidden = false
        } else {
            stateLabel.isHidden = true
        }
        
        if stackView.locationButton == true {
            locationButton.isHidden = false
        } else {
            locationButton.isHidden = true
        }
                                
        if stackView.rate == true {
            rateCosmosView.isHidden = false
            rateButton.isHidden = false
        } else {
            rateCosmosView.isHidden = true
            rateButton.isHidden = true
        }

//        if stackView.state == true || stackView.cancelOrder == true || stackView.rate == true {
//            stateStackView.isHidden = false
//        } else {
//            stateStackView.isHidden = true
//        }
        
        if stackView.name == true {
            nameLabel.isHidden = false
            clientNameLabel.isHidden = false
        } else {
            nameLabel.isHidden = true
            clientNameLabel.isHidden = true
        }
        
        if stackView.city == true {
            cityLabel.isHidden = false
            cityValueLabel.isHidden = false
        } else {
            cityLabel.isHidden = true
            cityValueLabel.isHidden = true
        }
        
        if stackView.distance == true {
            distanceLabel.isHidden = false
            distanceValueLabel.isHidden = false
        } else {
            distanceLabel.isHidden = true
            cityValueLabel.isHidden = true
        }
        
        if stackView.date == true {
            dateLabel.isHidden = false
            reservationDateLabel.isHidden = false
        } else {
            dateLabel.isHidden = true
            reservationDateLabel.isHidden = true
        }
        
        if stackView.service == true {
            serviceLabel.isHidden = false
            serviceValueLabel.isHidden = false
        } else {
            serviceLabel.isHidden = true
            serviceValueLabel.isHidden = true
        }
        
        if stackView.time == true {
            timeLabel.isHidden = false
            reservationTimeLabel.isHidden = false
        } else {
            timeLabel.isHidden = true
            reservationTimeLabel.isHidden = true
        }
        
        if stackView.amountAgreed == true {
            amountAgreedStackView.isHidden = false
//            amountAgreedValueLabel.isHidden = false
        } else {
            amountAgreedStackView.isHidden = true
//            amountAgreedValueLabel.isHidden = true
        }
                
        if stackView.accept == true || stackView.refuse == true {
            agreeRefuseStackView.isHidden = false
        } else {
            agreeRefuseStackView.isHidden = true
        }
        
        if stackView.confirm == true || stackView.cancel == true {
            confirmationStackView.isHidden = false
        } else {
            confirmationStackView.isHidden = true
        }
        
    }

    
    func configure() {
        nameLabel.text = "\("technician name".localized):"
        dateLabel.text = "\("date".localized):"
        timeLabel.text = "\("time".localized):"
        cityLabel.text = "\("city label".localized):"
        distanceLabel.text = "\("distance label".localized):"
        serviceLabel.text = "\("service label".localized):"
        amountAgreedLabel.text = "\("price label".localized):"
        refuseButton.setTitle("refuse button".localized, for: .normal)
        acceptButton.setTitle("accept button".localized, for: .normal)
        confirmButton.setTitle("confirm price button".localized, for: .normal)
        cancelButton.setTitle("cancel button".localized, for: .normal)
    }
        
    func configureView() {
        acceptButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        refuseButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        confirmButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        cancelButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        
        stateStackView.isHidden = true
        nameStackView.isHidden = true
        dateStackView.isHidden = true
        cityStackView.isHidden = true
        distanceStackView.isHidden = true
        serviceStackView.isHidden = true
        timeStackView.isHidden = true
        agreeRefuseStackView.isHidden = true
        confirmationStackView.isHidden = true
        amountAgreedStackView.isHidden = true

//        cancelOrderButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)

//        navView.roundCorners([.bottomLeft, .bottomRight], radius: 30.0)
        stateView.roundCorners([.topRight, .bottomLeft], radius: 15.0)
//        effectView.roundCorners([.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 7.0)
        
//        self.navView.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
//        let reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
//        reusableView.frame = self.navView.bounds
//        reusableView.titleLabel.text = "order details title".localized
//        reusableView.titleImage.image = UIImage(named: "improvement")
//        reusableView.backButtonDelegate = self
//        self.navView.addSubview(reusableView)
    }
    
    func showNoTrackingPopUp() {
        let alertController = UIAlertController(title: "عذرًا لا يمكنك تتبُع الفني الآن !", message: "يمكنك تتبُع الفني قبل ساعة من ميعاد تتبُع الفني", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
//        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }

    
    // MARK:- Navigation
    func goToRate(orderID: Int) {
        print("Go to rate")
        if let rateVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "RateNowViewController") as? RateNowViewController {
            rateVC.orderID = orderID
            self.present(rateVC, animated: true, completion: nil)
        }
    }
    
    func goToTracking(orderID: Int, workerID: Int) {
        print("Go to Tracking")
        if let trackingVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "TrackingViewController") as? TrackingViewController {
            trackingVC.orderId = orderID
            trackingVC.workerID = workerID
            self.present(trackingVC, animated: true, completion: nil)
        }
    }
    
    func alertWithTextField() {
        var textField = UITextField()
        
        // Create Alert
        let alert = UIAlertController(title: "إدخال السعر", message: "ادخل السعر", preferredStyle: .alert)
        
        // The alert has an action
        let action = UIAlertAction(title: "ارسل السعر", style: .default) { (action) in
            // What will happen once the user clicks the add button item on alert
            
            if textField.text == "" {
                print("NIL")
            } else {
                if let orderID: Int = self.orderId, let price = textField.text {
                    let price: Int = Int(price)!
                    self.viewModel.acceptOrder(orderID: orderID, orderStatusID: 3, price: price)
                }
            }
        }
        
        let cancelAction = UIAlertAction(title: "إغلاق", style: .cancel)
        
        // add textfield to alert
        alert.addTextField { (alertTextField) in
            alertTextField.placeholder = "ادخل السعر"
            alertTextField.keyboardType = .asciiCapableNumberPad
            
            // it doesn't type anything as the action of add item pressed is in ((( let action ))) so we need a textfield as the closure of add textfield is called once u add textfield to alert so in this time the textfield is empty so it doesn't print anything.
            print(alertTextField.text)
            
            textField = alertTextField
        }
        
        // add the action to the alert
        alert.addAction(action)
        alert.addAction(cancelAction)
        
        // present the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func openMapForPlace(longitude: String, latitude: String) {
        let lat1 : String = longitude
        let lng1 : String = latitude

        let latitude:CLLocationDegrees =  Double(lat1)!
        let longitude:CLLocationDegrees =  Double(lng1)!

        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
//        let options = [
//            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
//            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
//        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
//        mapItem.
        let options = [MKLaunchOptionsDirectionsModeKey:
            MKLaunchOptionsDirectionsModeDriving]

        mapItem.openInMaps(launchOptions: options)
    }


        
    // MARK:- Actions
    @IBAction func confirmButtonIsPressed(_ sender: Any) {
        print("Confirm")
        alertWithTextField()
    }
    
    @IBAction func cancelButtonIsPressed(_ sender: Any) {
        print("Cancel")
        if let orderID: Int = self.orderId {
            viewModel.submitAction(orderID: orderID, orderStatusID: 7)
        }
    }
    
    @IBAction func acceptButtonIsPressed(_ sender: Any) {
        print("Accept")
        if let orderID: Int = self.orderId {
            viewModel.submitAction(orderID: orderID, orderStatusID: 2)
        }
    }
    
    @IBAction func refuseButtonIsPressed(_ sender: Any) {
        print("Refuse")
        if let orderID: Int = self.orderId {
            viewModel.submitAction(orderID: orderID, orderStatusID: 6)
        }
    }
    
    @IBAction func locationButtonIsPressed(_ sender: Any) {
        if let lat = self.latitude, let long = self.longitude {
            openMapForPlace(longitude: long, latitude: lat)
        }
    }
    
    @IBAction func trackNowButtonIsPressed(_ sender: Any) {
        print("Track")
        if let orderID: Int = self.orderId, let orderStartDate: String = orderSent?.startDate, let workerID: Int = self.orderSent?.workerId {
            let currentDate: String = getCurrentTime()
            let currntDate: Date = getDate(date: currentDate)
            let orderDate: Date = getDate(date: orderStartDate)
            
            let diff = calculateDifference(from: orderDate, to: currntDate)
            
            if diff == 0 {
                goToTracking(orderID: orderID, workerID: workerID)
            } else {
                self.showNoTrackingPopUp()
            }
        }
    }
    
    @IBAction func completeButtonIsPressed(_ sender: Any) {
        print("Complete")
        if let orderID: Int = self.orderId {
            viewModel.submitAction(orderID: orderID, orderStatusID: 5)
        }
    }
    
    @IBAction func rateButtonIsPressed(_ sender: Any) {
        print("Rate")
        if let orderID: Int = self.orderId {
            self.goToRate(orderID: orderID)
        }
    }
    
}

extension OrderDetailsViewController: backButtonViewDelegate {
    
    func didbackButtonPressed() {
        print("Delegate fired")
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
}

