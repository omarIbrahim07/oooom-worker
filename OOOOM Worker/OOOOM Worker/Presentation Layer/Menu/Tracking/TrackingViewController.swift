//
//  TrackingViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/30/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import GoogleMaps
import Firebase

class TrackingViewController: BaseViewController {
    
    var reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View

    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var addressLabel: UILabel!

    var latitude: Double?
    var longitude: Double?
    var timeStamp: String?
    
    var orderId: Int?
    var workerID: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        mapView.delegate = self
        getFirebaseData()
    }
    
    func configureView() {
        self.navView.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        let reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
        reusableView.frame = self.navView.bounds
        reusableView.titleLabel.text = "tracking title".localized
        reusableView.titleImage.image = UIImage(named: "trucking")
        reusableView.backButtonDelegate = self
        self.navView.addSubview(reusableView)
    }

    
    func congi(latitude: Double, longitude: Double) {
//        let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 13.0)
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude), zoom: 19.0)
        self.mapView.camera = camera
        
        let marker = GMSMarker()
//        marker.position = position
        marker.map = mapView
    }
    
    func getFirebaseData() {
        
//        if let OrderId = self.orderId {
        if let workerID = self.workerID {
            Database.database().reference().child("Tracking").child("Orders").child(String(workerID)).observe(.value, with: { (snapshot) in
                let postDict = snapshot.value as? [String : AnyObject] ?? [:]
                
                print("Besm allah")
                
                if let lat = postDict["lat"] as? Double, let long = postDict["longe"] as? Double, let timeStamp = postDict["timestamp"] as? String {
                    //                self.latitudeLabel.text = String(lat)
                    //                self.longitudeLabel.text = String(long)
                    self.timeStamp = timeStamp
                    self.longitude = long
                    self.latitude = lat
                    
                    self.congi(latitude: lat, longitude: long)
                }
            })
        }
    }
    
    private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        
        // 1
        let geocoder = GMSGeocoder()
        
        // 2
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            guard let address = response?.firstResult(), let lines = address.lines else {
                return
            }
            
            // 3
            self.addressLabel.text = lines.joined(separator: "\n")
            
            // 4
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
    }

}

// MARK: - GMSMapViewDelegate
extension TrackingViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        print("5amorgia")
        reverseGeocodeCoordinate(position.target)
        
        if let lat = self.latitude, let long = self.longitude {
            self.congi(latitude: lat, longitude: long)
        }
    }
    
}

// MARK: - BackButtonDelegate
extension TrackingViewController: backButtonViewDelegate {

    func didbackButtonPressed() {
        print("Delegate fired")
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
}

