//
//  BidsTableViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/22/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class BidsTableViewCell: UITableViewCell {
    
    var bidCellViewModel: BidCellViewModel? {
        didSet {
            if let service: BidService = bidCellViewModel?.service {
                if "Lang".localized == "ar" {
                    if let serviceName: String = service.serviceName {
                        bidOrderLabel.text = serviceName
                    }
                } else if "Lang".localized == "en" {
                    if let serviceNameEn: String = service.serviceNameEn {
                        bidOrderLabel.text = serviceNameEn
                    }
                }
            }
            
            if let createdAt: String = bidCellViewModel?.createdAt {
                bidDateLabel.text = createdAt
            }
            
            if let id: Int = bidCellViewModel?.id {
                offersButton.tag = id
            }
        }
    }
    
    var didPressOnButton: ( (Int) -> Void )?

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var bidOrderLabel: UILabel!
    @IBOutlet weak var bidDateLabel: UILabel!
    @IBOutlet weak var bidImage: UIImageView!
    @IBOutlet weak var offersLabel: UILabel!
    @IBOutlet weak var offersButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureCell()
        configure()
    }
    
    func configure() {
        offersLabel.text = "offers button".localized
    }
    
    func configureCell() {
        containerView.layer.cornerRadius = 8.0
        bidImage.layer.cornerRadius = 8.0
    }
    
    @IBAction func offersButtonPressed(_ sender: Any) {
        print("Offers is pressed")
        
        print("Offers is pressed")
        if let selectedButton = (sender as AnyObject).tag {
            print((sender as AnyObject).tag)
            //            switch selectedButton {
            //            case .plus:
            //                count += 1
            //            case .minus:
            //                count -= 1
            //            }
            didPressOnButton?(selectedButton)
        }
    }
}
