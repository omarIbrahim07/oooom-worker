//
//  BidsViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/18/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

class BidsViewModel {

    private var bids: [Bid] = [Bid]()

    var selectedBid: Bid?

    enum noTechs {
        case notech
        case techs
    }

    var bidCellViewModels: [BidCellViewModel] = [BidCellViewModel]() {
           didSet {
               self.reloadTableViewClosure?()
           }
       }

    var reloadTableViewClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var setNoTechnician: (()->())?

    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }

    var techsExistance: noTechs = .notech {
        didSet {
            self.setNoTechnician?()
        }
    }

    var numberOfCells: Int {
         return bidCellViewModels.count
     }

    func initFetch() {
        state = .loading

        let params: [String : AnyObject] = [:]

        TechnicianAPIManager().getBids(basicDictionary: params, onSuccess: { (bids) in

            self.processFetchedPhoto(bids: bids)
            self.state = .populated

        }) { (error) in
            print(error)
        }
    }

    func processFetchedPhoto( bids: [Bid] ) {
        self.bids = bids // Cache

        if self.bids.count == 0 {
            self.techsExistance = .notech
        } else {
            self.techsExistance = .techs
        }

        var vms = [BidCellViewModel]()
        for bid in bids {
            vms.append( createCellViewModel(bid: bid) )
        }
        self.bidCellViewModels = vms
    }

    func createCellViewModel( bid: Bid ) -> BidCellViewModel {

        return BidCellViewModel(service: bid.service!, name: bid.name!, id: bid.id!, createdAt: bid.createdAt!)
    }


    func getCellViewModel( at indexPath: IndexPath ) -> BidCellViewModel {
        return bidCellViewModels[indexPath.row]
    }

    func userPressed( at indexPath: IndexPath ) {
        let bid = self.bids[indexPath.row]
        self.selectedBid = bid
    }

}
