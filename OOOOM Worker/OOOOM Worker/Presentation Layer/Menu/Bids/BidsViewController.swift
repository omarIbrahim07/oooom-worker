//
//  BidsViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/21/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class BidsViewController: BaseViewController {
    
    lazy var viewModel: BidsViewModel = {
        return BidsViewModel()
    }()
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
        
        viewModel.setNoTechnician = { [weak self] () in
            guard let self = self else {
                return
            }
            
            switch self.viewModel.techsExistance {
            
            case .notech:
                print("")
//                self.noMentainanceOrdersLabel.isHidden = false
            case .techs:
                print("")
//                self.noMentainanceOrdersLabel.isHidden = true
            }
         }

        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
//                    self.stopLoadingWithError(error: <#T##APIError#>)
                    self.stopLoadingWithSuccess()
                case .empty:
                    print("")
                }
            }
        }
            
//                if let service = choosedService {
//                    viewModel.initFetch(service: service, sorting: 1)
//                }
            

        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        viewModel.initFetch()
    }
    
    func configureView() {
        self.navView.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        let reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
        reusableView.frame = self.navView.bounds
        reusableView.titleLabel.text = "bids".localized
        reusableView.titleImage.image = UIImage(named: "offer")
        reusableView.backButtonDelegate = self
        self.navView.addSubview(reusableView)
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "BidsTableViewCell", bundle: nil), forCellReuseIdentifier: "BidsTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    // MARK:- Navigation
    func goToOrderOffers(orderId: Int) {
//        if let editProfileVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "OffersViewController") as? OffersViewController {
//            editProfileVC.orderId = orderId
//            //                rootViewContoller.test = "test String"
//            self.present(editProfileVC, animated: true, completion: nil)
//            print("My Account")
//        }
        let storyboard = UIStoryboard.init(name: "Menu", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "OffersViewController") as! OffersViewController
        //        viewController.postID = postID
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToBidDetails(bidID: Int) {
        if let bidDetailsVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "BidDetailsViewController") as? BidDetailsViewController {
            //                rootViewContoller.test = "test String"
            self.present(bidDetailsVC, animated: true, completion: nil)
            print("bidDetailsVC")
        }
    }

}

extension BidsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return numberOfBids ?? 0
        return viewModel.numberOfCells
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: BidsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "BidsTableViewCell", for: indexPath) as? BidsTableViewCell {
                        
            cell.didPressOnButton = { f in
                print("Closure Activated")
                self.goToOrderOffers(orderId: f)
            }
            
            let cellVM = viewModel.getCellViewModel(at: indexPath)
            cell.bidCellViewModel = cellVM
            
            return cell
        }
        return UITableViewCell()
    }
    
    
}

extension BidsViewController: backButtonViewDelegate {
    
    func didbackButtonPressed() {
        print("Delegate fired")
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
}
