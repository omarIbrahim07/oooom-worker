//
//  OfferDetailsTableViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 2/12/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class OfferDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var offerDetailsLabel: UILabel!
    @IBOutlet weak var footerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
