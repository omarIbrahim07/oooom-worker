//
//  cell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 2/16/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import FSPagerView

class cell: FSPagerViewCell {

    @IBOutlet weak var image: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    var viewModels : String? {
        didSet {
            bindData()
        }
    }
    
    func bindData() {
        
        if let image = viewModels {
            self.image.loadImageFromUrl(imageUrl: ImageURLOffers + image)
        }
        
    }

}
