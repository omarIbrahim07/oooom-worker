//
//  OfferDetailsCellViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 2/14/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct OfferDetailsCellViewModel {
    var id: Int?
    var price: Int?
    var createdAt: String?
    var workerID: Int?
    var title: String?
    var description: String?
    var serviceId: Int?
    var firstImage: String?
    var isSpecial: Int?
}
