//
//  OfferDetailsViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 2/14/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

class OfferDetailsViewModel {
    
    var reloadOfferDetails: (()->())?
    var updateLoadingStatus: (()->())?
    var updateImages: (()->())?
    var updateSelectedOffer: (()->())?
    
    var selectedOffer: OfferDetails? {
        didSet {
            self.updateSelectedOffer?()
        }
    }
    
    var error: APIError?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var offerDetails: OfferDetails? {
        didSet {
            self.reloadOfferDetails?()
        }
    }
        
    var technician: Technician?
    
//    var technician: Technician? {
//        didSet {
//            updateTechnicianDetails?()
//        }
//    }
    
    var offerDetailsImages: [String]? {
        didSet {
            self.updateImages?()
        }
    }
        
    func initOfferDetails(offerID: Int) {
        
        let params: [String : AnyObject] = [
            "offer_id" : offerID as AnyObject
        ]
        
        OffersAPIManager().getOfferDetails(basicDictionary: params, onSuccess: { (offerDetails) in
            self.offerDetails = offerDetails
            
            var offerImages: [String] = [String]()
            
            if let mainOfferImage = offerDetails.firstImage {
                offerImages.append(mainOfferImage)
                if let offerImagesArray = offerDetails.images {
                    for i in 0..<offerImagesArray.count {
                        offerImages.append(offerImagesArray[i])
                    }
                }
                self.processFetchedPhoto(images: offerImages)
            }
            
            self.state = .populated

        }) { (error) in
            self.state = .error
        }
    }
    
    func processFetchedPhoto( images: [String] ) {
        self.offerDetailsImages = images // Cache

    }

    var numberOfCells: Int {
        return offerDetailsImages?.count ?? 0
     }
    
    func getError() -> APIError {
        return self.error!
    }
        
}
