//
//  RateNowViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/1/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import Cosmos

class RateNowViewController: BaseViewController {
    
    var reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
    
    lazy var viewModel: RateNowViewModel = {
        return RateNowViewModel()
    }()
    
    var orderID: Int?
    var error: APIError?
    var status: Bool?

    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var cosmosRateView: CosmosView!
    @IBOutlet weak var rateTextView: UITextView!
    @IBOutlet weak var rateNowButton: UIButton!
    @IBOutlet weak var bottomView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configure()
        
        initVM()
    }
    
    func closeKeyPad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        rateTextView.endEditing(true)
    }
    
    // MARK: - Configuration
    func configureView() {
        self.navView.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        bottomView.roundCorners([.topLeft, .topRight], radius: 30.0)
        let reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
        reusableView.frame = self.navView.bounds
        reusableView.titleLabel.text = "rate now title".localized
        reusableView.titleImage.image = UIImage(named: "improvement")
        reusableView.backButtonDelegate = self
        self.navView.addSubview(reusableView)
        rateNowButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        rateTextView.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        closeKeyPad()
    }
    
    func configure() {
        rateTextView.delegate = self
        rateTextView.text = "complaints textview placeholder".localized
        rateTextView.textColor = UIColor.white
        rateNowButton.setTitle("send button".localized, for: .normal)
    }
    
    // MARK:- Init View Model
    func initVM() {
        
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.showError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
        
        viewModel.updateStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
               
                self.status = self.viewModel.getStatus()
                if let status = self.status {
                    if status == true {
                        self.goToHome()
                    }
                }
            }
        }
    }
    
    
    // MARK:- Actions
    @IBAction func rateNowButtonIsPressed(_ sender: Any) {
        guard let orderID: Int = self.orderID, orderID > 0 else {
            let apiError = APIError()
            apiError.message = "لا يوجد طلب"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let comment: String = self.rateTextView.text, comment.count >= 0 else {
            let apiError = APIError()
            apiError.message = "لا يوجد تعليق"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        guard let rate: Double = self.cosmosRateView.rating, rate > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء تقييم الطلب"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        let orderRate: Float = Float(rate)
        self.viewModel.rateOrder(orderID: orderID, rating: orderRate, comment: comment)
    }

    
    // MARK: - Navigation
    func goToHome() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeNavigationVC")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
}

extension RateNowViewController: backButtonViewDelegate {
    
    func didbackButtonPressed() {
        print("Delegate fired")
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
}

extension RateNowViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.white {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "complaints textview placeholder".localized
            textView.textColor = UIColor.white
        }
    }
    
}
