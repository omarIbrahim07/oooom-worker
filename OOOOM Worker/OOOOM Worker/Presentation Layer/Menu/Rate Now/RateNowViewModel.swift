//
//  RateNowViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/8/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

class RateNowViewModel {
    
    var error: APIError?

    var updateLoadingStatus: (()->())?
    var updateStatus: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
        
    // callback for interfaces
    var status: Bool? {
        didSet {
            self.updateStatus?()
        }
    }

    
    func rateOrder(orderID: Int, rating: Float, comment: String) {
    state = .loading
    
    let params: [String : AnyObject] = [
        "order_id" : orderID as AnyObject,
        "urating" : rating as AnyObject,
        "ucomment" : comment as AnyObject,
    ]
    
    OrderAPIManager().rateOrder(basicDictionary: params, onSuccess: { (status) in
        
        self.status = status
        if status {
            self.status = true
            self.state = .populated
        } else {
            self.makeError(message: "فشل الاتصال")
        }


        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func makeError(message: String) {
        let apiError = APIError()
        apiError.message = message
        self.error = apiError
        self.state = .error
    }

    func getError() -> APIError {
        return self.error!
    }

    func getStatus() -> Bool {
        return self.status!
    }
}
