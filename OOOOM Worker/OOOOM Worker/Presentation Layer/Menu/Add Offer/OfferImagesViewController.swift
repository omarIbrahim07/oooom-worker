//
//  OfferImagesViewController.swift
//  OOOOM Worker
//
//  Created by Omar Ibrahim on 3/8/20.
//  Copyright © 2020 Egy Designer. All rights reserved.
//

import UIKit

import UIKit
import AssetsPickerViewController
import Toast_Swift
import PKHUD
import Photos

class OfferImagesViewController: CommonExampleController {

    var postID: Int?
    var offerTitle: String?
    var offerPrice: Int?
    var offerDescription: String?
    var offerDays: Int?
    var mainImagee: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "add more images button".localized
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar()
    }
    
    //MARK:- Configurations
    func configureNavigationBar() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "add offer second stage bar button title".localized, style: .plain, target: self, action: #selector(saveAction))
    }
    
    func showDonePopUp() {
        let alertController = UIAlertController(title: "offer is done alert title".localized, message: "offer is done alert message".localized, preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "offer is done action message".localized, style: .default) { (action) in
            self.goToHomePage()
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK:- API Calls
    func saveAdPictures() {
        
        if mainImagee?.jpegData(compressionQuality: 0.7) != nil {
            
            let imgData = mainImagee!.jpegData(compressionQuality: 0.7)

//
//        guard let postIDValue = postID else {
//            return
//        }
//
        let parameters = [
            "title" : self.offerTitle as AnyObject,
            "price" : self.offerPrice as AnyObject,
            "description" : self.offerDescription as AnyObject,
            "days" : self.offerDays as AnyObject,
        ]

        var imageDataArr = [Data]()
        for asset in assets {
            let assetImage = getAssetThumbnail(asset: asset)
            let imgData = assetImage.jpegData(compressionQuality: 1.0)
            imageDataArr.append(imgData!)
        }

        startLoading()
        weak var weakSelf = self
        
        OffersAPIManager().addOfferWithExtraImages(imageData: imgData, imageDataArray: imageDataArr, basicDictionary: parameters, onSuccess: {

            weakSelf?.stopLoadingWithSuccess()
            self.showDonePopUp()
//            weakSelf?.dismiss(animated: true, completion: nil)
            

        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
        }
    }
    
    //MARK:- Actions
    @objc fileprivate func saveAction() {
        saveAdPictures()
    }

    override func pressedPick(_ sender: Any) {
        let picker = AssetsPickerViewController()
        picker.pickerDelegate = self
        present(picker, animated: true, completion: nil)
    }
    
    // MARK: - Navigation
    func goToHomePage() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeNavigationVC")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    //MARK:- Helpers
    func showError(error: APIError) {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
        var style: ToastStyle = ToastStyle.init()
        style.imageSize = CGSize(width: 44, height: 44)
        self.view.makeToast(error.message, duration: 3.0, position: .bottom, title: nil, image: nil, style: style, completion: nil)
    }
    
    func startLoading() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
        HUD.show(.progress)
    }
    
    func stopLoadingWithSuccess() {
        HUD.hide()
    }
    
    func stopLoadingWithError(error: APIError) {
        HUD.hide()
        showError(error: error)
    }
    
    func getAssetThumbnail(asset: PHAsset) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: CGSize(width: 100, height: 100), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            thumbnail = result!
        })
        return thumbnail
    }

}
