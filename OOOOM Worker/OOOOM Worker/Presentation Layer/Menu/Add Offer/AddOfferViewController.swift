//
//  AddOfferViewController.swift
//  OOOOM Worker
//
//  Created by Omar Ibrahim on 3/3/20.
//  Copyright © 2020 Egy Designer. All rights reserved.
//

import UIKit
import MOLH
import AssetsPickerViewController
import Toast_Swift
import PKHUD
import Photos

class AddOfferViewController: BaseViewController {

    var myImage: UIImage? = UIImage()
    var days: [Int] = [1,2,3,4,5,6,7]
    var choosedDays: String? = "1"
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var offerDetailsTextView: UITextView!
    @IBOutlet weak var mainOfferImageView: UIImageView!
    @IBOutlet weak var addMainImageButton: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var addOfferButton: UIButton!
    @IBOutlet weak var addMoreImagesForOfferButton: UIButton!
    @IBOutlet weak var addMainImageLabel: UILabel!
    @IBOutlet weak var daysLabel: UILabel!
    @IBOutlet weak var numberOfDaysLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureLanguage()
        configureView()
        navigationItem.title = "add offer title".localized
    }
    
    func closeKeyPad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        addressTextField.endEditing(true)
        priceTextField.endEditing(true)
        offerDetailsTextView.endEditing(true)
    }
    
    // MARK:- Configuration
    func configureView() {
//        self.navView.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
//        let reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
//        reusableView.frame = self.navView.bounds
//        reusableView.titleLabel.text = "add offer title".localized
//        reusableView.titleImage.image = UIImage(named: "offer")
//        reusableView.backButtonDelegate = self
//        self.navView.addSubview(reusableView)
        bottomView.roundCorners([.topLeft, .topRight], radius: 30.0)
        addOfferButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        addMoreImagesForOfferButton.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        offerDetailsTextView.addCornerRadius(raduis: 5.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        priceTextField.delegate = self
        offerDetailsTextView.delegate = self
        closeKeyPad()
    }
    
    func configureLanguage() {
        addressTextField.placeholder = "offer address placeholder".localized
        priceTextField.placeholder = "offer price placeholder".localized
        offerDetailsTextView.text = "offer details placeholder".localized
        offerDetailsTextView.textColor = UIColor.lightGray
        addOfferButton.setTitle("add offer button title".localized, for: .normal)
        addMoreImagesForOfferButton.setTitle("add more images for offer button title".localized, for: .normal)
        addMainImageLabel.text = "add main offer image title".localized
        daysLabel.text = "days title".localized
    }
                
    func imagePressed() {
        
        let image = UIImagePickerController()
        
        image.delegate = self
        
        image.sourceType = .photoLibrary
        
        self.present(image, animated: true) {
            
        }
    }
        
    func showMainOfferImageAlert() {
        let alertController = UIAlertController(title: "main offer image alert title".localized, message: "main offer image alert message".localized, preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let cancelAction = UIAlertAction(title: "cancel main offer image alert".localized, style: .cancel)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK:- Show Day Options Picker
    func setupDaysSheet() {
        let sheet = UIAlertController(title: "days alert title".localized, message: "day alert message".localized, preferredStyle: .actionSheet)
        for NoOfdays in days {
            if "Lang".localized == "ar" {
                sheet.addAction(UIAlertAction(title: String(NoOfdays), style: .default, handler: {_ in
                    self.choosedDays = String(NoOfdays)
                    self.numberOfDaysLabel.text = String(NoOfdays)
                }))
            } else if "Lang".localized == "en" {
                sheet.addAction(UIAlertAction(title: String(NoOfdays), style: .default, handler: {_ in
                    self.choosedDays = String(NoOfdays)
                    self.numberOfDaysLabel.text = String(NoOfdays)
                }))
            }
        }
        sheet.addAction(UIAlertAction(title: "cancel day".localized, style: .cancel, handler: nil))
        
//        sheet.popoverPresentationController?.sourceView = sortChoiceLabel
        
        self.present(sheet, animated: true, completion: nil)
    }
    
    func showDonePopUp() {
        let alertController = UIAlertController(title: "offer is done alert title".localized, message: "offer is done alert message".localized, preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "offer is done action message".localized, style: .default) { (action) in
            self.goToHomePage()
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }

    // MARK:- Networking
    func addOffer() {
        
        if myImage?.jpegData(compressionQuality: 0.7) != nil {
            
            let imgData = myImage!.jpegData(compressionQuality: 0.7)
            
            guard let address = addressTextField.text, address.count > 0 else {
                let apiError = APIError()
                apiError.message = "برجاء إدخال العنوان"
                showError(error: apiError)
                return
            }
            
            guard let price = priceTextField.text, price.count > 0 else {
                let apiError = APIError()
                apiError.message = "برجاء إدخال السعر بشكل صحيح"
                showError(error: apiError)
                return
            }
            
            guard let details = offerDetailsTextView.text, details.count > 0 else {
                let apiError = APIError()
                apiError.message = "برجاء إدخال السعر بشكل صحيح"
                showError(error: apiError)
                return
            }
            
            guard let days = self.choosedDays, days.count > 0 else {
                let apiError = APIError()
                apiError.message = "برجاء إدخال عدد الأيام"
                showError(error: apiError)
                return
            }
            
            let noOfdays = Int(days)
            
            let parameters = [
                "title" : address as AnyObject,
                "price" : price as AnyObject,
                "description" : details as AnyObject,
                "days" : noOfdays as AnyObject,
            ]

            weak var weakSelf = self
            weakSelf?.startLoading()
            OffersAPIManager().addOfferWithMainImage(imageData: imgData, basicDictionary: parameters, onSuccess: { (success) in
                
                weakSelf?.stopLoadingWithSuccess()
                
                if success == true {
                    self.showDonePopUp()
                }
    //            weakSelf?.presentSecondLevel(withPostID: postID)
                
            }) { (error) in
                weakSelf?.stopLoadingWithError(error: error)
            }
        }
    }
    
    // MARK:- Actions
    @IBAction func addOfferButtonIsPressed(_ sender: Any) {
        print("Add Offer is pressed")
        if myImage?.jpegData(compressionQuality: 0.7) == nil {
            self.showMainOfferImageAlert()
        }
        addOffer()
    }
    
    @IBAction func addMainImageForOfferIsPressed(_ sender: Any) {
        imagePressed()
    }
    
    @IBAction func daysButtonIsPressed(_ sender: Any) {
        print("Service Button Pressed")
        setupDaysSheet()
    }
    
    @IBAction func goToAddExtraOfferImages(_ sender: Any) {
        self.goToOfferDetails()
    }

    
    // MARK: - Navigation
    func goToHomePage() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeNavigationVC")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    func goToOfferDetails() {
                    
            if myImage?.jpegData(compressionQuality: 0.7) == nil {
                self.showMainOfferImageAlert()
                return
            }

            guard let address = addressTextField.text, address.count > 0 else {
                let apiError = APIError()
                apiError.message = "برجاء إدخال العنوان"
                showError(error: apiError)
                return
            }
            
            guard let price = priceTextField.text, price.count > 0 else {
                let apiError = APIError()
                apiError.message = "برجاء إدخال السعر بشكل صحيح"
                showError(error: apiError)
                return
            }
            
            guard let details = offerDetailsTextView.text, details.count > 0 else {
                let apiError = APIError()
                apiError.message = "برجاء إدخال السعر بشكل صحيح"
                showError(error: apiError)
                return
            }
            
            guard let days = self.choosedDays, days.count > 0 else {
                let apiError = APIError()
                apiError.message = "برجاء إدخال عدد الأيام"
                showError(error: apiError)
                return
            }
            
            let noOfdays = Int(days)
            let pricee = Int(price)

        
        let storyboard = UIStoryboard.init(name: "Menu", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "OfferImagesViewController") as! OfferImagesViewController
        //        viewController.postID = postID
        viewController.offerTitle = address
        viewController.offerPrice = pricee
        viewController.offerDescription = description
        viewController.offerDays = noOfdays
        viewController.mainImagee = myImage

        navigationController?.pushViewController(viewController, animated: true)
            
        }

        
//        if let offerImagesVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "OfferImagesViewController") as? OfferImagesViewController {
//            
//            offerImagesVC.offerTitle = title
//            offerImagesVC.offerPrice = price
//            offerImagesVC.offerDescription = description
//            offerImagesVC.offerDays = days
//            offerImagesVC.mainImagee = image
//            //                rootViewContoller.test = "test String"
//            self.present(offerImagesVC, animated: true, completion: nil)
//            print("Offer Images")
//        }
//    }
    
}

extension AddOfferViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            mainOfferImageView.image = image
            mainOfferImageView.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
            myImage = image
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension AddOfferViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
    
//        let allowedCharacters = CharacterSet.decimalDigits
//        let allowedCharacters = CharacterSet(charactersIn:".0123456789١٢٣٤٥٦٧٨٩٠,")
        let allowedCharacters = CharacterSet(charactersIn:".0123456789")
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
}

extension AddOfferViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "offer details placeholder".localized
            textView.textColor = UIColor.lightGray
        }
    }
    
}

// MARK:- Back Button Delegate
extension AddOfferViewController: backButtonViewDelegate {
    
    func didbackButtonPressed() {
        print("Delegate fired")
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
}
