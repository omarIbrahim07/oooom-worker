//
//  PromoCodeViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/28/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation

class PromoCodeViewModel {
    
    public enum State {
        case loading
        case error
        case populated
        case empty
    }
    
    var updateLoadingStatus: (()->())?
    var updatePromocode: (()->())?
    
    var error: APIError?
    
    var code: Int? {
        didSet {
            updatePromocode?()
        }
    }
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    func initFetch() {
        let params: [String : AnyObject] = [:]

        self.state = .loading
        OffersAPIManager().getPromoCode(basicDictionary: params, onSuccess: { (promocodeObj) in
            
            if promocodeObj.found == true {
                self.code = promocodeObj.promoCode?.value
            }
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func sendPromoCode(code: String) {
        let params: [String : AnyObject] = [
            "code" : code as AnyObject
        ]

        self.state = .loading
        OffersAPIManager().sendPromoCode(basicDictionary: params, onSuccess: { (promocodeResponse) in
            
            if promocodeResponse.saved == true {
                if let promocode: Promocode = promocodeResponse.promocode {
                    self.code = self.processpromoCode(promocode: promocode)
                }
                self.state = .populated
            } else if promocodeResponse.saved == false {
                self.error = APIError()
                self.state = .error
            }
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }

    
    private func processpromoCode(promocode: Promocode) -> Int {
        return promocode.value ?? 0
    }
    
    func getCode() -> Int {
        return self.code!
    }
    
    func getPromoCode() {
        
        let params: [String : AnyObject] = [:
        ]
        
        self.state = .loading
        
        OffersAPIManager().getPromoCode(basicDictionary: params, onSuccess: { (promocodeObj) in
                        
            if promocodeObj.found == true {
                self.code = promocodeObj.promoCode?.value
            } else {
                self.code = 0
            }
            
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func getError() -> APIError {
        return self.error!
    }
    
}
