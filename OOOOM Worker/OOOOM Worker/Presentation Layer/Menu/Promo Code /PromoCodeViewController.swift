//
//  PromoCodeViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/21/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class PromoCodeViewController: BaseViewController {
    
    lazy var viewModel: PromoCodeViewModel = {
        return PromoCodeViewModel()
    }()
    
    var code: Int?
    var error: APIError?
    
    var reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View

    @IBOutlet weak var youhaveLabel: UILabel!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var sendCodeButton: UIButton!
    @IBOutlet weak var promoView: UIView!
    @IBOutlet weak var promoCodeTextField: UITextField!
    @IBOutlet weak var promocodeValueLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configure()
        
        initVM()
        
        viewModel.initFetch()
    }
    
    // MARK:- Init View Model
    func initVM() {
        
        viewModel.updatePromocode = { [weak self] () in
            
            DispatchQueue.main.async { [weak self] in
                self?.code = self?.viewModel.getCode()
                if let code = self?.code {
                    self?.bindPromoCode(code: code)
                }
            }
        }
                
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.showError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
                
    }
    
    func bindPromoCode(code: Int) {
        self.promocodeValueLabel.text = String(code) + " %"
    }
    
    func configure() {
        promoCodeTextField.placeholder = "promo code".localized
        youhaveLabel.text = "you have".localized
        sendCodeButton.setTitle("send code button".localized, for: .normal)
        closeKeypad()
    }

    func configureView() {
        self.navView.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        bottomView.roundCorners([.topLeft, .topRight], radius: 30.0)
        sendCodeButton.addCornerRadius(raduis: 3.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        let reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
        reusableView.frame = self.navView.bounds
        reusableView.titleLabel.text = "promo code".localized
        reusableView.titleImage.image = UIImage(named: "percentage")
        reusableView.backButtonDelegate = self
        self.navView.addSubview(reusableView)
        promoView.addCornerRadius(raduis: promoView.frame.height / 2, borderColor: #colorLiteral(red: 0.2901960784, green: 0.6745098039, blue: 0.9137254902, alpha: 1), borderWidth: 5.0)
    }
    
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        promoCodeTextField.endEditing(true)
    }

    
    func changeTextFieldPlaceHolderColor(textField: UITextField, placeHolderString: String,fontSize: CGFloat) {
        textField.attributedPlaceholder = NSAttributedString(string: placeHolderString, attributes: [.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), .font: UIFont.boldSystemFont(ofSize: fontSize)])
    }

    
    @IBAction func sendCodeButtonPressed(_ sender: Any) {
        print("Send Promo Code")
        
        guard let code: String = promoCodeTextField.text, code.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال الكود الترويجي"
            self.viewModel.error = apiError
            self.viewModel.state = .error
            return
        }
        
        self.viewModel.sendPromoCode(code: code)
    }
    
}

extension PromoCodeViewController: backButtonViewDelegate {

    func didbackButtonPressed() {
        print("Delegate fired")
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
}
