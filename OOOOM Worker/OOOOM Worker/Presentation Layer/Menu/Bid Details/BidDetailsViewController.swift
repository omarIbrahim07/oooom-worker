//
//  BidDetailsViewController.swift
//  OOOOM Worker
//
//  Created by Omar Ibrahim on 1/21/20.
//  Copyright © 2020 Egy Designer. All rights reserved.
//

import UIKit
import MapKit
import SwiftAudio
import AVFoundation
import MediaPlayer

class BidDetailsViewController: BaseViewController {
    
    lazy var viewModel: BidDetailsViewModel = {
        return BidDetailsViewModel()
    }()
    
    var reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
    
    var bidID: Int?
    var error: APIError?
    var bidData: BidDetailsDataViewModel?
    var longitude: String?
    var latitude: String?
    
    var sound: String?

    private var isScrubbing: Bool = false
    let player = AudioPlayer()
    private let controller = AudioController.shared
    private var lastLoadFailed: Bool = false

    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var firstImage: UIImageView!
    @IBOutlet weak var secondImage: UIImageView!
    @IBOutlet weak var thirdImage: UIImageView!
    @IBOutlet weak var requestButton: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var imagesStackView: UIStackView!
    @IBOutlet weak var recordView: UIView!
    @IBOutlet weak var bidButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var soundSlider: UISlider!
    @IBOutlet weak var timeElapsedLabel: UILabel!
    @IBOutlet weak var reportButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configure()
        configureView()
        print(bidID)
        
        initVM()
        configureAudioPlayerEventHandlers()
    }
    
    func configureAudioPlayerEventHandlers() {
        controller.player.event.stateChange.addListener(self, handleAudioPlayerStateChange)
        controller.player.event.secondElapse.addListener(self, handleAudioPlayerSecondElapsed)
        controller.player.event.seek.addListener(self, handleAudioPlayerDidSeek)
        controller.player.event.updateDuration.addListener(self, handleAudioPlayerUpdateDuration)
        controller.player.event.didRecreateAVPlayer.addListener(self, handleAVPlayerRecreated)
        updateMetaData()
        handleAudioPlayerStateChange(data: controller.player.playerState)
    }
    
    func updateTimeValues() {
        self.soundSlider.maximumValue = Float(self.controller.player.duration)
        self.soundSlider.setValue(Float(self.controller.player.currentTime), animated: true)
        self.timeElapsedLabel.text = self.controller.player.currentTime.secondsToString()
//        self.remainingTimeLabel.text = (self.controller.player.duration - self.controller.player.currentTime).secondsToString()
    }
    
    func updateMetaData() {
        if let item = controller.player.currentItem {
            item.getArtwork({ (image) in
//                self.imageView.image = image
            })
        }
    }

    // MARK: - AudioPlayer Event Handlers
    
    func handleAudioPlayerStateChange(data: AudioPlayer.StateChangeEventData) {
        
        print(data)
        DispatchQueue.main.async {
            self.setPlayButtonState(forAudioPlayerState: data)
            switch data {
            case .loading:
//                self.loadIndicator.startAnimating()
                self.updateMetaData()
                self.updateTimeValues()
            case .buffering:
//                self.loadIndicator.startAnimating()
                print("")
            case .ready:
//                self.loadIndicator.stopAnimating()
                self.updateMetaData()
                self.updateTimeValues()
            case .playing, .paused, .idle:
//                self.loadIndicator.stopAnimating()
                self.updateTimeValues()
            }
        }
    }
    
    func setPlayButtonState(forAudioPlayerState state: AudioPlayerState) {
//        playButton.setTitle(state == .playing ? "Pause" : "Play", for: .normal)
        playButton.setImage(state == .playing ? #imageLiteral(resourceName: "pause (1)") : #imageLiteral(resourceName: "play-button-1"), for: .normal)
    }
    
    func handleAudioPlayerSecondElapsed(data: AudioPlayer.SecondElapseEventData) {

        if !isScrubbing {
            DispatchQueue.main.async {
                self.updateTimeValues()
            }
        }
    }
    
    func handleAudioPlayerDidSeek(data: AudioPlayer.SeekEventData) {

        isScrubbing = false
    }
    
    func handleAudioPlayerUpdateDuration(data: AudioPlayer.UpdateDurationEventData) {

        DispatchQueue.main.async {
            self.updateTimeValues()
        }
    }
    
    func handleAVPlayerRecreated() {

        try? controller.audioSessionController.set(category: .playback)
    }
    
    func setupPlayer(sound: String) {

        // To reset the current sound
//        controller.player.reset()
                    
        // load to remove any previous first book track
        controller.sources.append(DefaultAudioItem(audioUrl: sound, sourceType: .stream))
        controller.add()
        
        controller.sources = []
        controller.player.reset()

    }
    

    
    @IBAction func togglePlay(_ sender: Any) {
        print("Feeees")
        
        guard let sound = sound else {
            return
        }
        setupPlayer(sound: sound)
        
        if !controller.audioSessionController.audioSessionIsActive {
            try? controller.audioSessionController.activateSession()
        }
        if lastLoadFailed, let item = controller.player.currentItem {
            lastLoadFailed = false
//            errorLabel.isHidden = true
            try? controller.player.load(item: item, playWhenReady: true)
        }
        else {
            controller.player.togglePlaying()
        }
    }


    
    @IBAction func startScrubbing(_ sender: UISlider) {

        isScrubbing = true
    }
    
    @IBAction func scrubbing(_ sender: UISlider) {

        controller.player.seek(to: Double(soundSlider.value))
    }
    
    @IBAction func scrubbingValueChanged(_ sender: UISlider) {

        let value = Double(soundSlider.value)
        timeElapsedLabel.text = (controller.player.duration - value).secondsToString()
//        timeElapsedLabel.text = value.secondsToString()
//        remainingTimeLabel.text = (controller.player.duration - value).secondsToString()
    }

    func initVM() {
        
        viewModel.updateBidData = { [weak self] () in
            DispatchQueue.main.async {
                self?.bidData = self!.viewModel.getDataStackViewModel()
                if let bidData = self?.bidData {
                    self!.bindDataStackView(stackView: bidData)
                }
            }
        }
        
        viewModel.reportBid = { [weak self] () in
            DispatchQueue.main.async {
                if self!.viewModel.isReport == true {
                    print("Report is succeeded")
                }
            }
        }
                
        viewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.viewModel.state {
                
                case .loading:
                    self.startLoading()
                case .populated:
                    self.stopLoadingWithSuccess()
                case .error:
                    self.error = self.viewModel.getError()
                    if let error = self.error {
                        self.showError(error: error)
                    }
                case .empty:
                    print("")
                }
            }
        }
        
        viewModel.removeBlock = { [weak self] () in
            DispatchQueue.main.async {
                if self!.viewModel.isRemovedBlock == true {
                    self?.goToHomePage()
                }
            }
        }
        
        if let bidID: Int = self.bidID {
            viewModel.initFetch(bidID: bidID)
        }
    }
    
    func animate() {
        let view = UIView()
        view.backgroundColor = UIColor.red
        view.frame = firstImage.frame
        view.addSubview(view)
    }
    
    func bindDataStackView(stackView: BidDetailsDataViewModel) {
        print(stackView)
        
        if let record: String = stackView.record {
            recordView.isHidden = false
            self.sound = ImageURLBids + stackView.record!
        } else {
            recordView.isHidden = true
        }
        
        if let image: String = stackView.image {
            imagesStackView.isHidden = false
            firstImage.isHidden = false
            firstImage.loadImageFromUrl(imageUrl: ImageURLBids + image)
        } else {
            firstImage.isHidden = true
        }
        
        if let image2: String = stackView.image2 {
            imagesStackView.isHidden = false
            secondImage.isHidden = false
            secondImage.loadImageFromUrl(imageUrl: ImageURLBids + image2)
        } else {
            secondImage.isHidden = true
        }

        if let image3: String = stackView.image3 {
            imagesStackView.isHidden = false
            thirdImage.isHidden = false
            thirdImage.loadImageFromUrl(imageUrl: ImageURLBids + image3)
        } else {
            thirdImage.isHidden = true
        }
        
        if firstImage.isHidden == true || secondImage.isHidden == true || thirdImage.isHidden == true {
            imagesStackView.isHidden = true
        }
        
        if let lat = stackView.latitude {
            print(lat)
            self.latitude = lat
        }
        
        if let long = stackView.longitude {
            print(long)
            self.longitude = long
        }

        if let description = stackView.message {
            self.descriptionLabel.text = description
        }
        
        if let price = stackView.bidOfferPrice {
            print(price)
            bottomView.isHidden = true
            showDonePopUp(bidPrice: price)
        } else {
            bottomView.isHidden = false
        }
    }
    
    func configure() {
        bidButton.setTitle("bid button title".localized, for: .normal)
        reportButton.setTitle("report".localized, for: .normal)
        imagesStackView.isHidden = true
        recordView.isHidden = true
        descriptionLabel.text = ""
        firstImage.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        secondImage.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        thirdImage.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func configureView() {
        self.navView.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        bottomView.roundCorners([.topLeft, .topRight], radius: 30.0)
        bidButton.addCornerRadius(raduis: 3.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        recordView.roundCorners([.topRight, .bottomRight, .bottomLeft ], radius: 20)
        let reusableView = Bundle.main.loadNibNamed("View", owner: self, options: nil)?.first as! View
        reusableView.frame = self.navView.bounds
        reusableView.titleLabel.text = "bid details title".localized
        reusableView.titleImage.image = UIImage(named: "offer")
        reusableView.backButtonDelegate = self
        self.navView.addSubview(reusableView)
    }
    
    func showDonePopUp(bidPrice: Int) {
        let alertController = UIAlertController(title: "تم تقديم عرض من قبل قيمته", message: "\(bidPrice) SAR                       ", preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let cancelAction = UIAlertAction(title: "تم", style: .cancel) { (action) in
        }
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK:- Pass long and lat
    func passLocation() {
        if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
//                        UIApplication.shared.openURL(NSURL(string:
//                            "comgooglemaps://?saddr=&daddr=\(place.latitude),\(place.longitude)&directionsmode=driving")! as URL)
            UIApplication.shared.openURL(NSURL(string:
                "comgooglemaps://?saddr=&daddr=\(37.421998333333335),\(-122.08400000000002)&directionsmode=driving")! as URL)
        } else {
            NSLog("Can't use comgooglemaps://");
        }
    }
        
    func openMapForPlace(longitude: String, latitude: String) {
        let lat1 : String = longitude
        let lng1 : String = latitude

        let latitude:CLLocationDegrees =  Double(lat1)!
        let longitude:CLLocationDegrees =  Double(lng1)!

        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
//        let options = [
//            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
//            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
//        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
//        mapItem.
        let options = [MKLaunchOptionsDirectionsModeKey:
            MKLaunchOptionsDirectionsModeDriving]

        mapItem.openInMaps(launchOptions: options)
    }
    
    func alertWithTextField() {
        var textField = UITextField()
        
        // Create Alert
        let alert = UIAlertController(title: "إدخال السعر", message: "ادخل السعر", preferredStyle: .alert)
        
        // The alert has an action
        let action = UIAlertAction(title: "ارسل السعر", style: .default) { (action) in
            // What will happen once the user clicks the add button item on alert
            
            if textField.text == "" {
                print("NIL")
            } else {
                if let bidID: Int = self.bidID, let price = textField.text {
                    let price: Int = Int(price)!
                    self.viewModel.sendPrice(bidID: bidID, price: price)
                }
            }
                
        }
        
        let cancelAction = UIAlertAction(title: "إغلاق", style: .cancel)
        
        // add textfield to alert
        alert.addTextField { (alertTextField) in
            alertTextField.placeholder = "ادخل السعر"
            alertTextField.keyboardType = .asciiCapableNumberPad
            
            // it doesn't type anything as the action of add item pressed is in ((( let action ))) so we need a textfield as the closure of add textfield is called once u add textfield to alert so in this time the textfield is empty so it doesn't print anything.
            print(alertTextField.text)
            
            textField = alertTextField
        }
        
        // add the action to the alert
        alert.addAction(action)
        alert.addAction(cancelAction)
        
        // present the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK:- Show Options Picker
    func setupOptionSheet() {
        let sheet = UIAlertController()
//        let sheet = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        sheet.addAction(UIAlertAction(title: "report option".localized, style: .default, handler: {_ in
            if let fromUserID = UserDefaultManager.shared.currentUser?.id, let toUserID = self.bidData?.clientID, let bidID = self.bidData?.id {
                self.showReportPopUp(fromUserID: fromUserID, toUserId: toUserID, bidID: bidID)
            }
        }))
        sheet.addAction(UIAlertAction(title: "block option".localized, style: .default, handler: {_ in
            if let toUserID = self.bidData?.clientID {
                self.showBlockPopUp(toId: toUserID)
            }
        }))
        sheet.addAction(UIAlertAction(title: "report cancel button".localized, style: .cancel, handler: nil))
//        sheet.popoverPresentationController?.sourceView = s1WeigthLabel
        self.present(sheet, animated: true, completion: nil)
    }
            
    func showBlockPopUp(toId: Int) {
        let alertController = UIAlertController(title: "block title popup".localized, message: "block message popup".localized, preferredStyle: .alert)
        let openAction = UIAlertAction(title: "block okay button".localized, style: .default) { (action) in
            self.viewModel.blockUser(toUserID: toId)
        }
        let cancelAction = UIAlertAction(title: "block cancel button".localized, style: .cancel)
        alertController.addAction(openAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showReportPopUp(fromUserID: Int, toUserId: Int, bidID: Int) {
        let alertController = UIAlertController(title: "report title popup".localized, message: "report message popup".localized, preferredStyle: .alert)
        let openAction = UIAlertAction(title: "report okay button".localized, style: .default) { (action) in
//            self.goToHomePage()
            self.viewModel.sendReport(fromUserID: fromUserID, toUserID: toUserId, bidID: bidID)
        }
        let cancelAction = UIAlertAction(title: "report cancel button".localized, style: .cancel)
        alertController.addAction(openAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    // MARK:- Actions
    @IBAction func reportButtonIsPressed(_ sender: Any) {
        print("report is pressed")
        setupOptionSheet()
    }
    
    @IBAction func locationButtonIsPressed(_ sender: Any) {
        if let lat = self.latitude, let long = self.longitude {
            openMapForPlace(longitude: long, latitude: lat)
        }
    }
    
    @IBAction func bidRequestButtonIsPressed(_ sender: Any) {
        print("Bid request button is pressed")
        alertWithTextField()
    }
    
    @IBAction func firstImageButtonIsPressed(_ sender: Any) {
        print("FirstButtonIsPressed")
        if let imageURL = self.bidData?.image {
            self.goToImageDetails(imageURL: ImageURLBids + imageURL)
        }
    }
    
    @IBAction func secondImageButtonIsPressed(_ sender: Any) {
        print("SecondButtonIsPressed")
        if let imageURL = self.bidData?.image2 {
            self.goToImageDetails(imageURL: ImageURLBids + imageURL)
        }
    }

    @IBAction func thirdImageButtonIsPressed(_ sender: Any) {
        print("ThirdButtonIsPressed")
        if let imageURL = self.bidData?.image3 {
            self.goToImageDetails(imageURL: ImageURLBids + imageURL)
        }
    }
        
    // MARK: - Navigation
//    func goToImageDetails(imageURL: String) {
//        let storyboard = UIStoryboard.init(name: "Menu", bundle: nil)
//        let viewController = storyboard.instantiateViewController(withIdentifier: "BidsViewControllernav") as! ImageZoomedViewController
//        viewController.imageURL = imageURL
//        navigationController?.pushViewController(viewController, animated: true)
//    }
    
    func goToImageDetails(imageURL: String) {
        if let imageDetailsVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "ImageZoomedViewController") as? ImageZoomedViewController {
            //                rootViewContoller.test = "test String"
            imageDetailsVC.imageURL = imageURL
            self.present(imageDetailsVC, animated: true, completion: nil)
            print("bidDetailsVC")
        }
    }
    
    func goToHomePage() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeNavigationVC")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }

}

extension BidDetailsViewController: backButtonViewDelegate {
    
    func didbackButtonPressed() {
        print("Delegate fired")
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
}

//extension BidDetailsViewController: UIViewControllerPreviewingDelegate {
//    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
////        if let selectedIndex = tableView.indexPathForRow(at: location) {
////            let item = viewModel.Resturants?[selectedIndex.row]
////            if let image = item? {
//                let view = self.storyboard?.instantiateViewController(withIdentifier: "PeekAndPopViewController") as! PeekAndPopViewController
//                view.data = item
//                return view
////            }
////        }
//        return ViewController()
//    }
//
//    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
//        if let view = viewControllerToCommit as? PeekAndPopViewController {
//            let restView = self.storyboard?.instantiateViewController(withIdentifier: "ResturantViewController") as! ResturantViewController
//            restView.data = view.data
//            self.navigationController?.pushViewController(restView, animated: true)
//        }
//    }
//}

