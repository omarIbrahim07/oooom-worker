//
//  BidDetailsViewModel.swift
//  OOOOM Worker
//
//  Created by Omar Ibrahim on 1/21/20.
//  Copyright © 2020 Egy Designer. All rights reserved.
//

import Foundation

class BidDetailsViewModel {
    
    private var bid: BidDetails?
    
    public enum State {
        case loading
        case error
        case populated
        case empty
    }
    
    var updateLoadingStatus: (()->())?
    var updateBidData: (()->())?
    var reportBid: (()->())?
    var removeBlock: (()->())?

    var error: APIError?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var isRemovedBlock: Bool = false {
        didSet {
            self.removeBlock?()
        }
    }
    
    var isReport: Bool = false {
        didSet {
            self.reportBid?()
        }
    }
    
    var bidDetailsDataViewModel: BidDetailsDataViewModel? {
        didSet {
            self.updateBidData?()
        }
    }
    
    func initFetch(bidID: Int) {
        let params: [String : AnyObject] = ["bid_id" : bidID as AnyObject]

        self.state = .loading
        OrderAPIManager().getBidDetails(basicDictionary: params, onSuccess: { (bid) in
            
            self.bid = bid
            if let bid = self.bid {
                self.bidDetailsDataViewModel = self.processTechnicianDetails(bid: bid)
            }
            
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func processTechnicianDetails(bid: BidDetails) -> BidDetailsDataViewModel {
        return BidDetailsDataViewModel(id: bid.id, message: bid.message, record: bid.record, image: bid.image, image2: bid.image2, image3: bid.image3, longitude: bid.longitude, latitude: bid.latitude, bidOfferPrice: bid.bidOfferPrice, name: bid.name, clientID: bid.clientID)
    }
    
    func sendPrice(bidID: Int, price: Int) {
        let params: [String : AnyObject] = [
            "bid_id" : bidID as AnyObject,
            "price" : price as AnyObject
        ]

        self.state = .loading
        OrderAPIManager().sendBidPrice(basicDictionary: params, onSuccess: { (saved) in
            
            if saved == true {
                if let bidID = self.bid?.id {
                    self.initFetch(bidID: bidID)
                }
            }
            
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func sendReport(fromUserID: Int, toUserID: Int, bidID: Int) {
        let params: [String : AnyObject] = [
            "from_userid" : fromUserID as AnyObject,
            "to_userid" : toUserID as AnyObject,
            "is_chat" : 0 as AnyObject,
            "bid_id" : bidID as AnyObject,
        ]

        self.state = .loading
        OrderAPIManager().sendReport(basicDictionary: params, onSuccess: { (saved) in
            
            if saved == true {
                self.isReport = true
            } else if saved == false {
                
            }
            
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }
    
    func blockUser(toUserID: Int) {
        let params: [String : AnyObject] = [
            "to_id" : toUserID as AnyObject,
        ]

        self.state = .loading
        BlockAPIManager().blockUser(basicDictionary: params, onSuccess: { (saved) in
            
            if saved == true {
                self.isRemovedBlock = true
            } else if saved == false {
                
            }
            
            self.state = .populated
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }


    func makeError(message: String) {
        let apiError = APIError()
        apiError.message = message
        self.error = apiError
        self.state = .error
    }

    func sendPromoCode(code: String) {
        let params: [String : AnyObject] = [
            "code" : code as AnyObject
        ]

        self.state = .loading
        OffersAPIManager().sendPromoCode(basicDictionary: params, onSuccess: { (promocodeResponse) in
            
            if promocodeResponse.saved == true {
                if let promocode: Promocode = promocodeResponse.promocode {
//                    self.code = self.processpromoCode(promocode: promocode)
                }
                self.state = .populated
            } else if promocodeResponse.saved == false {
                self.error = APIError()
                self.state = .error
            }
            
        }) { (error) in
            self.error = error
            self.state = .error
        }
    }

    
    private func processpromoCode(promocode: Promocode) -> Int {
        return promocode.value ?? 0
    }
            
    func getError() -> APIError {
        return self.error!
    }

    func getDataStackViewModel() -> BidDetailsDataViewModel {
        return self.bidDetailsDataViewModel!
    }
    
}
