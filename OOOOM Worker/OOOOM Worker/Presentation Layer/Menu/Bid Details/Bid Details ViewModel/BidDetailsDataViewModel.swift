//
//  BidDetailsDataViewModel.swift
//  OOOOM Worker
//
//  Created by Omar Ibrahim on 1/21/20.
//  Copyright © 2020 Egy Designer. All rights reserved.
//

import Foundation
struct BidDetailsDataViewModel {
    var id: Int?
    var message: String?
    var record: String?
    var image: String?
    var image2: String?
    var image3: String?
    var longitude: String?
    var latitude: String?
    var bidOfferPrice: Int?
    var name: String?
    var clientID: Int?
}
