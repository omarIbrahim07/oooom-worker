//
//  constants.swift
//  GameOn
//
//  Created by Hassan on 12/22/17.
//  Copyright © 2017 Hassan. All rights reserved.
//

import Foundation
import UIKit
import Firebase

let GoogleClientID = "245060057438-g8g7dhp3emmo5cjdvfijcjarovqmev76.apps.googleusercontent.com"

//MARK:- User default keys
let kAuthorization = "UserDefaults_Authorization"
let kUserData = "UserDefaults_UserData"

var FirebaseToken : String!

let uuid = NSUUID().uuidString
let kUserDefault = UserDefaults.standard

let appDelegate = UIApplication.shared.delegate as! AppDelegate


let screenWidth = UIScreen.main.bounds.width
let screenHeight = UIScreen.main.bounds.height


let iPhoneXNavigationBarHeight: CGFloat = 88
let normalNavigationBarHeight: CGFloat = 64

let mainBlueColor = UIColor().hexStringToUIColor(hex: "#4367B2")

let generalError = "An Error has occured"
let ALL_COLLECTIONVIEW_CELL = 1

let nckSelectHomeTab = "nckSelectHomeTab"

let nckUpdatePost = "nckUpdatePost"

let ncSelectHomeTab = Notification.Name(nckSelectHomeTab)

let ncUpdatePost = Notification.Name(nckUpdatePost)

let AppStoreURL = "https://itunes.apple.com/us/app/"
